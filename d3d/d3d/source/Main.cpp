#include <network/RakWin.h>

#include <scene/RenderScene.h>

#include <sceneobjects/SceneObjectConfig.h>
#include <sceneobjects/SceneObjectEntryPoint.h>
#include <sceneobjects/input/SceneObjectBufferedInput.h>

#include <rendering/lighting/SceneObjectLighting.h>
#include <rendering/model/SceneObjectStaticModelBatcher.h>

#include <sceneobjects/network/SceneObjectGameServer.h>
#include <sceneobjects/network/SceneObjectClient.h>

#include <sceneobjects/gui/SceneObjectChatBox.h>

#include <util/Math.h>

#include <fstream>
#include <limits>

#define D3D_FIXED_TIMESTEP 0.017f

int main() {
	/*std::ofstream out("out.txt");
	std::cout.rdbuf(out.rdbuf());

	std::ofstream err("err.txt");
	std::cerr.rdbuf(err.rdbuf());*/

	std::shared_ptr<SceneObjectConfig> config(new SceneObjectConfig());

	config->load("config.txt");

	sf::RenderWindow window;
	sf::ContextSettings settings;

	settings.majorVersion = 4;
	settings.minorVersion = 3;
	settings.stencilBits = 0;
	settings.antialiasingLevel = 0;

	window.create(sf::VideoMode(config->_resolutionWidth, config->_resolutionHeight), "d3d", config->_windowed ? sf::Style::Default : sf::Style::Fullscreen, settings);

	window.setVerticalSyncEnabled(config->_vsync);
	window.setFramerateLimit(60);

	window.setActive();

	GLenum error = glewInit();

	assert(error == GLEW_NO_ERROR);

	// -------------------------------- Scene Setup --------------------------------

	std::unique_ptr<d3d::RenderScene> scene(new d3d::RenderScene());

	//scene->_randomGenerator.seed(time(nullptr));

	{
		std::shared_ptr<d3d::Shader> gBufferRender(new d3d::Shader());
		std::shared_ptr<d3d::Shader> gBufferRenderNormal(new d3d::Shader());
		std::shared_ptr<d3d::Shader> gBufferRenderHeightNormal(new d3d::Shader());
		std::shared_ptr<d3d::Texture2D> whiteTexture(new d3d::Texture2D());

		gBufferRender->createAsset("NONE resources/shaders/gbufferrender/gBufferRender.vert resources/shaders/gbufferrender/gBufferRender.frag");
		gBufferRenderNormal->createAsset("NONE resources/shaders/gbufferrender/gBufferRenderBump.vert resources/shaders/gbufferrender/gBufferRenderBump.frag");
		gBufferRenderHeightNormal->createAsset("NONE resources/shaders/gbufferrender/gBufferRenderParallax.vert resources/shaders/gbufferrender/gBufferRenderParallax.frag");
		whiteTexture->createAsset("resources/shaders/white.png");

		scene->createRenderScene(8, d3d::AABB3D(d3d::Vec3f(-1.0f, -1.0f, -1.0f), d3d::Vec3f(1.0f, 1.0f, 1.0f)), &window,
			gBufferRender, gBufferRenderNormal, gBufferRenderHeightNormal, whiteTexture, d3d::ComputeSystem::_none);

		scene->_logicCamera._projectionMatrix = d3d::Matrix4x4f::perspectiveMatrix(d3d::_piOver4, static_cast<float>(window.getSize().x) / static_cast<float>(window.getSize().y), 0.1f, 10000.0f);
		scene->_logicCamera._position = d3d::Vec3f(1.5f, 1.5f, 1.5f);
		scene->_logicCamera._rotation = d3d::Quaternion::getFromMatrix(d3d::Matrix4x4f::cameraDirectionMatrix(-scene->_logicCamera._position.normalized(), d3d::Vec3f(0.0f, 1.0f, 0.0f)));
	}

	// ------------------------------- Add Config -------------------------------

	scene->addNamed(config, "config", false);

	// -------------------------------- Lighting --------------------------------

	{
		std::shared_ptr<d3d::Shader> ambientLightShader(new d3d::Shader());
		std::shared_ptr<d3d::Shader> pointLightShader(new d3d::Shader());
		std::shared_ptr<d3d::Shader> pointLightShadowedShader(new d3d::Shader());
		std::shared_ptr<d3d::Shader> spotLightShader(new d3d::Shader());
		std::shared_ptr<d3d::Shader> directionalLightShader(new d3d::Shader());
		std::shared_ptr<d3d::Shader> directionalLightShadowedShader(new d3d::Shader());
		std::shared_ptr<d3d::Shader> emissiveRenderShader(new d3d::Shader());
		std::shared_ptr<d3d::Shader> depthRenderShader(new d3d::Shader());

		std::shared_ptr<d3d::StaticPositionModel> sphereModel(new d3d::StaticPositionModel());
		std::shared_ptr<d3d::StaticPositionModel> coneModel(new d3d::StaticPositionModel());

		ambientLightShader->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/noTransformFragment.frag");
		pointLightShader->createAsset("NONE resources/shaders/positionOnlyVertex.vert resources/shaders/light/unshadowed/point.frag");
		pointLightShadowedShader->createAsset("NONE resources/shaders/positionOnlyVertex.vert resources/shaders/light/shadowed/point.frag");
		spotLightShader->createAsset("NONE resources/shaders/positionOnlyVertex.vert resources/shaders/light/unshadowed/spot.frag");
		directionalLightShader->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/light/unshadowed/directional.frag");
		directionalLightShadowedShader->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/light/shadowed/directionalShadowed.frag");
		emissiveRenderShader->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/light/emissiveRender.frag");
		depthRenderShader->createAsset("NONE resources/shaders/defaultVertex.vert resources/shaders/depthRender.frag");

		d3d::AABB3D aabb;

		sphereModel->loadFromOBJ("resources/shaders/models/icosphere_lowRes.obj", aabb, true, true);
		coneModel->loadFromOBJ("resources/shaders/models/cone.obj", aabb, true, true);

		std::shared_ptr<d3d::Asset> assetNoise;

		scene->getAssetManager("tex2D", d3d::Texture2D::assetFactory)->getAsset("resources/textures/noise.bmp", assetNoise);

		std::shared_ptr<d3d::Texture2D> noiseMap = std::static_pointer_cast<d3d::Texture2D>(assetNoise);

		std::shared_ptr<d3d::SceneObjectLighting> lighting(new d3d::SceneObjectLighting());

		scene->addNamed(lighting, "lighting");

		lighting->create(ambientLightShader,
			pointLightShader, pointLightShadowedShader,
			spotLightShader,
			directionalLightShader, directionalLightShadowedShader,
			emissiveRenderShader, depthRenderShader,
			sphereModel, coneModel,
			noiseMap);
	}

	// --------------------------------- Input -----------------------------------

	d3d::SceneObjectRef bufferedInputRef;

	{
		std::shared_ptr<d3d::SceneObjectBufferedInput> bufferedInput(new d3d::SceneObjectBufferedInput());

		scene->addNamed(bufferedInput, "buffIn");

		bufferedInputRef = *bufferedInput;
	}

	// -------------------------------- Networking -------------------------------

	{
		char choice = 'A';
		std::cout << "(C)lient or (S)erver: ";
		std::cin >> choice;

		std::shared_ptr<d3d::SceneObjectClient> client(new d3d::SceneObjectClient());
		scene->addNamed(client, "client");

		if (choice == 'S' || choice == 's')
		{
			std::shared_ptr<d3d::SceneObjectGameServer> server(new d3d::SceneObjectGameServer());
			scene->addNamed(server, "server");
			server->start(4321, 100);
		}

		if (choice == 'C' || choice == 'c')
		{
			std::cout << "IP Address of GameServer: ";
			std::string ip;
			std::getline(std::cin, ip);
			std::getline(std::cin, ip);

			client->connect(ip, 4321);
		}
		else
			client->connect(std::string("127.0.0.1"), 4321);
	}

	// -------------------------------- Batching ---------------------------------

	{
		std::shared_ptr<d3d::SceneObjectStaticModelBatcher> staticModelBatcher(new d3d::SceneObjectStaticModelBatcher());

		scene->addNamed(staticModelBatcher, "smb", false);
	}

	// -------------------------------- Game Loop --------------------------------

	{
		std::shared_ptr<SceneObjectEntryPoint> entryPoint(new SceneObjectEntryPoint());

		scene->add(entryPoint);
	}

	bool quit = false;

	sf::Clock clock;

	float dt = 0.0f;

	while (!quit) {
		{
			sf::Event windowEvent;

			d3d::SceneObjectBufferedInput* pBufferedInput = static_cast<d3d::SceneObjectBufferedInput*>(bufferedInputRef.get());
			pBufferedInput->_events.clear();

			while (window.pollEvent(windowEvent)) {
				pBufferedInput->_events.push_back(windowEvent);

				switch (windowEvent.type) {
				case sf::Event::Closed:
					quit = true;

					break;
				}
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
				quit = true;
		}

#ifdef D3D_FIXED_TIMESTEP
		scene->frame(D3D_FIXED_TIMESTEP);
#else
		scene->frame(dt);
#endif

		glFlush();

		window.display();

		dt = clock.getElapsedTime().asSeconds();
		clock.restart();
	}

	scene.reset();

	window.close();

	return 0;
}