#pragma once

#include <scene/RenderScene.h>

namespace d3d {
	class SceneObjectEffectBuffer : public SceneObject {
	private:
		float _downsampleRatio;

	public:
		std::shared_ptr<d3d::TextureRT> _ping;
		std::shared_ptr<d3d::TextureRT> _pong;

		std::shared_ptr<d3d::TextureRT> _fullPing;
		std::shared_ptr<d3d::TextureRT> _fullPong;

		void create(float downsampleRatio);

		// Inherited from SceneObject
		void onResize();

		SceneObjectEffectBuffer* copyFactory() {
			return new SceneObjectEffectBuffer(*this);
		}
	};
}