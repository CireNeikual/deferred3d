#pragma once

#include <scene/RenderScene.h>
#include <rendering/imageeffects/SceneObjectEffectBuffer.h>

namespace d3d {
	class SceneObjectSSAO : public SceneObject {
	private:
		d3d::SceneObjectRef _effectBuffer;

		std::shared_ptr<Shader> _blurShaderHorizontalEdgeAware;
		std::shared_ptr<Shader> _blurShaderVerticalEdgeAware;

		std::shared_ptr<Shader> _ssaoShader;

		std::shared_ptr<Shader> _renderImageShader;

		std::shared_ptr<Texture2D> _noiseMap;

	public:
		size_t _numBlurPasses;
		float _blurRadius;

		float _ssaoRadius;
		float _ssaoStrength;

		SceneObjectSSAO()
			: _numBlurPasses(2), _blurRadius(0.00493f), _ssaoRadius(0.2f), _ssaoStrength(3.0f)
		{}

		void create(const std::shared_ptr<Shader> &blurShaderHorizontalEdgeAware,
			const std::shared_ptr<Shader> &blurShaderVerticalEdgeAware,
			const std::shared_ptr<Shader> &ssaoShader,
			const std::shared_ptr<Shader> &renderImageShader,
			const std::shared_ptr<Texture2D> &noiseMap);

		// Inherited from SceneObject
		void onAdd();
		void postRender();
		void onResize();

		SceneObjectSSAO* copyFactory() {
			return new SceneObjectSSAO(*this);
		}
	};
}