#pragma once

#include <vector>
#include <random>

namespace rl {
	class MultiLSTMRL {
	public:
		struct Synapse {
			float _weight;
			float _trace;
		};

		struct MemorySynapse {
			float _weight;
			float _trace;
			float _derivative;
		};

		struct Node {
			std::vector<Synapse> _synapses;
			Synapse _bias;

			float _prevOutput;
			float _output;
		};

		struct MemoryNode {
			std::vector<MemorySynapse> _synapses;
			MemorySynapse _bias;

			float _prevOutput;
			float _output;
		};

		struct MemoryCell {
			MemoryNode _inputGate;
			Node _outputGate;
			MemoryNode _forgetGate;

			std::vector<MemorySynapse> _synapses;
			MemorySynapse _bias;

			float _net;
			float _state;
			float _prevOutput;
			float _output;
		};

		struct MultiLSTMRLSettings {
			float _alpha;
			float _tauInv;
			float _gamma;
			float _lambda;
			float _epsilon;

			MultiLSTMRLSettings()
				: _alpha(0.01f),
				_tauInv(10.0f),
				_gamma(0.99f),
				_lambda(0.5f),
				_epsilon(0.15f)
			{}
		};

		static float sigmoid(float x) {
			return 1.0f / (1.0f + std::expf(-x));
		}

	private:
		std::vector<std::vector<Node>> _outputNodes;
		std::vector<std::vector<Node>> _hiddenGroups;
		std::vector<MemoryCell> _memoryCells;

		std::vector<float> _currentInputs;

		float _prevValue;
		float _prevAdvantage;

		std::vector<size_t> _outputs;

	public:
		MultiLSTMRL();

		void createRandom(size_t numInputs, size_t numOutputGroups, size_t numOutputsPerGroup, size_t hiddenSize, size_t numMemoryCells, float minWeight, float maxWeight, std::mt19937 &generator);

		void step(float reward, const MultiLSTMRLSettings &settings, std::mt19937 &generator);

		void setInput(size_t index, float value) {
			_currentInputs[index] = value;
		}

		size_t getOutput(size_t index) const {
			return _outputs[index];
		}

		// For evolution
		void mutate(float mutationRate, float maxPerturbation, std::mt19937 &generator);
		void crossover(const MultiLSTMRL &parent1, const MultiLSTMRL &parent2, float averageChance, size_t numInputs, size_t numOutputGroups, size_t numOutputsPerGroup, size_t hiddenSize, size_t numMemoryCells, float minWeight, float maxWeight, std::mt19937 &generator);
	
		void clearMemory();

		void saveToFile(const std::string &fileName);
		bool createFromFile(const std::string &fileName);
	};
}