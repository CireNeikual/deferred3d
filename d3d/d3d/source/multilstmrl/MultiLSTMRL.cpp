#include <multilstmrl/MultiLSTMRL.h>

#include <algorithm>
#include <fstream>
#include <assert.h>

using namespace rl;

MultiLSTMRL::MultiLSTMRL()
{}

void MultiLSTMRL::createRandom(size_t numInputs, size_t numOutputGroups, size_t numOutputsPerGroup, size_t hiddenSize, size_t numMemoryCells, float minWeight, float maxWeight, std::mt19937 &generator) {
	_currentInputs.assign(numInputs, 0.0f);

	_outputNodes.resize(numOutputGroups);
	_hiddenGroups.resize(numOutputGroups);
	_memoryCells.resize(numMemoryCells);

	std::uniform_real_distribution<float> weightDist(minWeight, maxWeight);

	const size_t numOutputInputs = hiddenSize + numMemoryCells;
	const size_t numHiddenInputs = numInputs + numMemoryCells;
	const size_t numGateInputs = numMemoryCells * 3 + numInputs + numMemoryCells + hiddenSize * numOutputGroups * numOutputsPerGroup;
	const size_t numMemoryCellInputs = numInputs + numMemoryCells + hiddenSize * numOutputGroups * numOutputsPerGroup;

	for (size_t i = 0; i < numOutputGroups; i++) {
		_outputNodes[i].resize(numOutputsPerGroup);

		for (size_t j = 0; j < numOutputsPerGroup; j++) {
			_outputNodes[i][j]._bias._weight = weightDist(generator);
			_outputNodes[i][j]._bias._trace = 0.0f;
			_outputNodes[i][j]._prevOutput = _outputNodes[i][j]._output = 0.0f;

			_outputNodes[i][j]._synapses.resize(numOutputInputs);

			for (size_t k = 0; k < _outputNodes[i][j]._synapses.size(); k++) {
				_outputNodes[i][j]._synapses[j]._weight = weightDist(generator);
				_outputNodes[i][j]._synapses[j]._trace = 0.0f;
			}
		}

		_hiddenGroups[i].resize(hiddenSize);

		for (size_t j = 0; j < hiddenSize; j++) {
			_hiddenGroups[i][j]._bias._weight = weightDist(generator);
			_hiddenGroups[i][j]._bias._trace = 0.0f;
			_hiddenGroups[i][j]._prevOutput = _hiddenGroups[i][j]._output = 0.0f;

			_hiddenGroups[i][j]._synapses.resize(numHiddenInputs);

			for (size_t k = 0; k < _hiddenGroups[i][j]._synapses.size(); k++) {
				_hiddenGroups[i][j]._synapses[k]._weight = weightDist(generator);
				_hiddenGroups[i][j]._synapses[k]._trace = 0.0f;
			}
		}
	}

	for (size_t i = 0; i < numMemoryCells; i++) {
		_memoryCells[i]._outputGate._bias._weight = weightDist(generator);
		_memoryCells[i]._outputGate._bias._trace = 0.0f;
		_memoryCells[i]._outputGate._prevOutput = _memoryCells[i]._outputGate._output = 0.0f;

		_memoryCells[i]._inputGate._bias._weight = weightDist(generator);
		_memoryCells[i]._inputGate._bias._trace = 0.0f;
		_memoryCells[i]._inputGate._bias._derivative = 0.0f;
		_memoryCells[i]._inputGate._prevOutput = _memoryCells[i]._inputGate._output = 0.0f;

		_memoryCells[i]._forgetGate._bias._weight = weightDist(generator);
		_memoryCells[i]._forgetGate._bias._trace = 0.0f;
		_memoryCells[i]._forgetGate._bias._derivative = 0.0f;
		_memoryCells[i]._forgetGate._prevOutput = _memoryCells[i]._forgetGate._output = 0.0f;

		_memoryCells[i]._net = 0.0f;
		_memoryCells[i]._bias._weight = weightDist(generator);
		_memoryCells[i]._bias._trace = 0.0f;
		_memoryCells[i]._bias._derivative = 0.0f;
		_memoryCells[i]._prevOutput = _memoryCells[i]._output = 0.0f;
		_memoryCells[i]._state = 0.0f;

		_memoryCells[i]._outputGate._synapses.resize(numGateInputs);
		_memoryCells[i]._inputGate._synapses.resize(numGateInputs);
		_memoryCells[i]._forgetGate._synapses.resize(numGateInputs);
		_memoryCells[i]._synapses.resize(numGateInputs);

		for (size_t j = 0; j < numGateInputs; j++) {
			_memoryCells[i]._outputGate._synapses[j]._weight = weightDist(generator);
			_memoryCells[i]._outputGate._synapses[j]._trace = 0.0f;

			_memoryCells[i]._inputGate._synapses[j]._weight = weightDist(generator);
			_memoryCells[i]._inputGate._synapses[j]._trace = 0.0f;
			_memoryCells[i]._inputGate._synapses[j]._derivative = 0.0f;

			_memoryCells[i]._forgetGate._synapses[j]._weight = weightDist(generator);
			_memoryCells[i]._forgetGate._synapses[j]._trace = 0.0f;
			_memoryCells[i]._forgetGate._synapses[j]._derivative = 0.0f;

			_memoryCells[i]._synapses[j]._weight = weightDist(generator);
			_memoryCells[i]._synapses[j]._trace = 0.0f;
			_memoryCells[i]._synapses[j]._derivative = 0.0f;
		}
	}

	_outputs.assign(numOutputGroups, 0);
}

void MultiLSTMRL::step(float reward, const MultiLSTMRLSettings &settings, std::mt19937 &generator) {
	// Update memory cells
	for (size_t i = 0; i < _memoryCells.size(); i++) {
		float sum;
		size_t si;

		// ----------------------------- Update Gates -----------------------------

		// Output
		sum = _memoryCells[i]._outputGate._bias._weight;

		si = 0;

		for (size_t j = 0; j < _memoryCells.size(); j++) {
			sum += _memoryCells[j]._outputGate._prevOutput * _memoryCells[i]._outputGate._synapses[si++]._weight;
			sum += _memoryCells[j]._inputGate._prevOutput * _memoryCells[i]._outputGate._synapses[si++]._weight;
			sum += _memoryCells[j]._forgetGate._prevOutput * _memoryCells[i]._outputGate._synapses[si++]._weight;

			sum += _memoryCells[j]._prevOutput * _memoryCells[i]._outputGate._synapses[si++]._weight;
		}

		for (size_t j = 0; j < _currentInputs.size(); j++)
			sum += _currentInputs[j] * _memoryCells[i]._outputGate._synapses[si++]._weight;

		for (size_t j = 0; j < _hiddenGroups.size(); j++)
		for (size_t k = 0; k < _hiddenGroups[j].size(); k++)
			sum += _hiddenGroups[j][k]._prevOutput * _memoryCells[i]._outputGate._synapses[si++]._weight;

		_memoryCells[i]._outputGate._output = sigmoid(sum);

		// Input
		sum = _memoryCells[i]._inputGate._bias._weight;

		si = 0;

		for (size_t j = 0; j < _memoryCells.size(); j++) {
			sum += _memoryCells[j]._outputGate._prevOutput * _memoryCells[i]._inputGate._synapses[si++]._weight;
			sum += _memoryCells[j]._inputGate._prevOutput * _memoryCells[i]._inputGate._synapses[si++]._weight;
			sum += _memoryCells[j]._forgetGate._prevOutput * _memoryCells[i]._inputGate._synapses[si++]._weight;

			sum += _memoryCells[j]._prevOutput * _memoryCells[i]._inputGate._synapses[si++]._weight;
		}

		for (size_t j = 0; j < _currentInputs.size(); j++)
			sum += _currentInputs[j] * _memoryCells[i]._inputGate._synapses[si++]._weight;

		for (size_t j = 0; j < _hiddenGroups.size(); j++)
		for (size_t k = 0; k < _hiddenGroups[j].size(); k++)
			sum += _hiddenGroups[j][k]._prevOutput * _memoryCells[i]._inputGate._synapses[si++]._weight;

		_memoryCells[i]._inputGate._output = sigmoid(sum);

		// Forget
		sum = _memoryCells[i]._forgetGate._bias._weight;

		si = 0;

		for (size_t j = 0; j < _memoryCells.size(); j++) {
			sum += _memoryCells[j]._outputGate._prevOutput * _memoryCells[i]._forgetGate._synapses[si++]._weight;
			sum += _memoryCells[j]._inputGate._prevOutput * _memoryCells[i]._forgetGate._synapses[si++]._weight;
			sum += _memoryCells[j]._forgetGate._prevOutput * _memoryCells[i]._forgetGate._synapses[si++]._weight;

			sum += _memoryCells[j]._prevOutput * _memoryCells[i]._forgetGate._synapses[si++]._weight;
		}

		for (size_t j = 0; j < _currentInputs.size(); j++)
			sum += _currentInputs[j] * _memoryCells[i]._forgetGate._synapses[si++]._weight;

		for (size_t j = 0; j < _hiddenGroups.size(); j++)
		for (size_t k = 0; k < _hiddenGroups[j].size(); k++)
			sum += _hiddenGroups[j][k]._prevOutput * _memoryCells[i]._forgetGate._synapses[si++]._weight;

		_memoryCells[i]._forgetGate._output = sigmoid(sum);

		// ----------------------------- Update CEC -----------------------------

		sum = _memoryCells[i]._bias._weight;

		si = 0;

		for (size_t j = 0; j < _memoryCells.size(); j++) {
			sum += _memoryCells[j]._outputGate._prevOutput * _memoryCells[i]._synapses[si++]._weight;
			sum += _memoryCells[j]._inputGate._prevOutput * _memoryCells[i]._synapses[si++]._weight;
			sum += _memoryCells[j]._forgetGate._prevOutput * _memoryCells[i]._synapses[si++]._weight;

			sum += _memoryCells[j]._prevOutput * _memoryCells[i]._synapses[si++]._weight;
		}

		for (size_t j = 0; j < _currentInputs.size(); j++)
			sum += _currentInputs[j] * _memoryCells[i]._synapses[si++]._weight;

		for (size_t j = 0; j < _hiddenGroups.size(); j++)
		for (size_t k = 0; k < _hiddenGroups[j].size(); k++)
			sum += _hiddenGroups[j][k]._prevOutput * _memoryCells[i]._synapses[si++]._weight;

		float memoryCellInput = sigmoid(sum) * 4.0f - 2.0f;

		_memoryCells[i]._net = memoryCellInput;

		_memoryCells[i]._state = _memoryCells[i]._forgetGate._output * _memoryCells[i]._state + _memoryCells[i]._inputGate._output * memoryCellInput;

		_memoryCells[i]._output = (sigmoid(_memoryCells[i]._state) * 2.0f - 1.0f) * _memoryCells[i]._outputGate._output;
	}

	// Activate hidden units
	for (size_t i = 0; i < _hiddenGroups.size(); i++)
	for (size_t j = 0; j < _hiddenGroups[i].size(); j++) {
		float sum = _hiddenGroups[i][j]._bias._weight;

		size_t si = 0;

		for (size_t k = 0; k < _currentInputs.size(); k++)
			sum += _currentInputs[k] * _hiddenGroups[i][j]._synapses[si++]._weight;

		for (size_t k = 0; k < _memoryCells.size(); k++)
			sum += _memoryCells[k]._output * _hiddenGroups[i][j]._synapses[si++]._weight;

		_hiddenGroups[i][j]._output = sigmoid(sum);
	}

	// Update output units
	for (size_t i = 0; i < _outputNodes.size(); i++)
	for (size_t j = 0; j < _outputNodes[i].size(); j++) {
		float sum = _outputNodes[i][j]._bias._weight;

		size_t si = 0;

		for (size_t k = 0; k < _hiddenGroups[i].size(); k++)
			sum += _hiddenGroups[i][k]._output * _outputNodes[i][j]._synapses[si++]._weight;

		for (size_t k = 0; k < _memoryCells.size(); k++)
			sum += _memoryCells[k]._output * _outputNodes[i][j]._synapses[si++]._weight;

		_outputNodes[i][j]._output = sum;
	}

	// ------------------------------------------ Select Actions -----------------------------------------

	std::vector<size_t> prevOutputs = _outputs;

	float value = 0.0f;
	float advantage = 0.0f;

	std::uniform_real_distribution<float> uniformDist(0.0f, 1.0f);

	for (size_t i = 0; i < _outputNodes.size(); i++) {
		size_t maxAction = 0;

		for (size_t j = 1; j < _outputNodes[i].size(); j++)
		if (_outputNodes[i][j]._output > _outputNodes[i][maxAction]._output)
			maxAction = j;

		// Epsilon-greedy selection
		if (uniformDist(generator) < settings._epsilon) {
			std::uniform_int_distribution<int> outputDist(0, static_cast<int>(_outputNodes[i].size()) - 1);

			_outputs[i] = static_cast<size_t>(outputDist(generator));

			// Boltzmann distribution selection
			/*std::vector<float> relativeProbabilities(_outputNodes[i].size());

			float sum = 0.0f;

			for (int j = 0; j < _outputNodes[i].size(); j++) {
				relativeProbabilities[j] = std::expf(_outputNodes[i][j]._output);

				sum += relativeProbabilities[j];
			}

			float randomCusp = uniformDist(generator) * sum;

			_outputs[i] = 0;

			float sumSoFar = 0.0f;

			for (int j = 0; j < _outputNodes[i].size(); j++) {
				sumSoFar += relativeProbabilities[j];

				if (sumSoFar >= randomCusp) {
					_outputs[i] = j;

					break;
				}
			}*/
		}
		else
			_outputs[i] = maxAction;

		value += _outputNodes[i][maxAction]._output;
		advantage += _outputNodes[i][_outputs[i]]._output;
	}

	//float tdError = reward + settings._gamma * value - _prevAdvantage;
	float tdError = _prevValue + (reward + settings._gamma * value - _prevValue) * settings._tauInv - _prevAdvantage;
	//float tdError = reward - value;

	_prevValue = value;
	_prevAdvantage = advantage;

	// ------------------------------------------ Update weights ------------------------------------------

	const float gammaLambda = settings._gamma * settings._lambda;
	const float alphaTDError = settings._alpha * tdError;

	size_t si;

	for (size_t i = 0; i < _outputNodes.size(); i++) {
		for (size_t j = 0; j < _outputNodes[i].size(); j++) {
			float mult = j == _outputs[i] ? 1.0f : 0.0f;

			// Update output for selected action output unit
			_outputNodes[i][j]._bias._weight += alphaTDError * _outputNodes[i][j]._bias._trace;
			_outputNodes[i][j]._bias._trace = gammaLambda * _outputNodes[i][j]._bias._trace + mult;

			si = 0;

			for (size_t k = 0; k < _hiddenGroups[i].size(); k++) {
				_outputNodes[i][j]._synapses[si]._weight += alphaTDError * _outputNodes[i][j]._synapses[si]._trace;
				_outputNodes[i][j]._synapses[si]._trace = gammaLambda * _outputNodes[i][j]._synapses[si]._trace + _hiddenGroups[i][k]._output * mult;

				si++;
			}

			for (size_t k = 0; k < _memoryCells.size(); k++) {
				_outputNodes[i][j]._synapses[si]._weight += alphaTDError * _outputNodes[i][j]._synapses[si]._trace;
				_outputNodes[i][j]._synapses[si]._trace = gammaLambda * _outputNodes[i][j]._synapses[si]._trace + _memoryCells[k]._output * mult;

				si++;
			}
		}
	}

	/*for (size_t i = 0; i < _outputNodes.size(); i++) {
		// Update output for selected action output unit
		_outputNodes[i][prevOutputs[i]]._bias._weight += alphaTDError * _outputNodes[i][_outputs[i]]._bias._trace;
		_outputNodes[i][prevOutputs[i]]._bias._trace = gammaLambda * _outputNodes[i][_outputs[i]]._bias._trace + 1.0f;

		si = 0;

		for (size_t j = 0; j < _hiddenGroups[i].size(); j++) {
			_outputNodes[i][prevOutputs[i]]._synapses[si]._weight += alphaTDError * _outputNodes[i][_outputs[i]]._synapses[si]._trace;
			_outputNodes[i][prevOutputs[i]]._synapses[si]._trace = gammaLambda * _outputNodes[i][_outputs[i]]._synapses[si]._trace + _hiddenGroups[i][j]._output;

			si++;
		}

		for (size_t j = 0; j < _memoryCells.size(); j++) {
			_outputNodes[i][prevOutputs[i]]._synapses[si]._weight += alphaTDError * _outputNodes[i][_outputs[i]]._synapses[si]._trace;
			_outputNodes[i][prevOutputs[i]]._synapses[si]._trace = gammaLambda * _outputNodes[i][_outputs[i]]._synapses[si]._trace + _memoryCells[j]._output;

			si++;
		}
	}*/

	// Update hidden units
	for (size_t i = 0; i < _hiddenGroups.size(); i++) {
		for (size_t j = 0; j < _hiddenGroups[i].size(); j++) {
			float cTerm = _outputNodes[i][_outputs[i]]._synapses[j]._weight * _hiddenGroups[i][j]._output * (1.0f - _hiddenGroups[i][j]._output);

			_hiddenGroups[i][j]._bias._weight += alphaTDError * _hiddenGroups[i][j]._bias._trace;
			_hiddenGroups[i][j]._bias._trace = gammaLambda * _hiddenGroups[i][j]._bias._trace + cTerm;

			si = 0;

			for (size_t k = 0; k < _currentInputs.size(); k++) {
				_hiddenGroups[i][j]._synapses[si]._weight += alphaTDError * _hiddenGroups[i][j]._synapses[si]._trace;
				_hiddenGroups[i][j]._synapses[si]._trace = gammaLambda * _hiddenGroups[i][j]._synapses[si]._trace + cTerm * _currentInputs[k];

				si++;
			}

			for (size_t k = 0; k < _memoryCells.size(); k++) {		
				_hiddenGroups[i][j]._synapses[si]._weight += alphaTDError * _hiddenGroups[i][j]._synapses[si]._trace;
				_hiddenGroups[i][j]._synapses[si]._trace = gammaLambda * _hiddenGroups[i][j]._synapses[si]._trace + cTerm * _memoryCells[k]._output;

				si++;
			}
		}
	}

	// Update memory cells
	for (size_t i = 0; i < _memoryCells.size(); i++) {
		// ----------------------------- Update Gates -----------------------------

		const float sigmoidNet = sigmoid(_memoryCells[i]._state);
		const float hNet = sigmoidNet * 2.0f - 1.0f;
		const float hPrimeNet = 2.0f * sigmoidNet * (1.0f - sigmoidNet);

		float wKc = 0.0f;
		
		for (size_t j = 0; j < _outputNodes.size(); j++)
			wKc += _outputNodes[j][_outputs[j]]._synapses[_hiddenGroups[j].size() + i]._weight;

		float sum = 0.0f;

		for (size_t j = 0; j < _outputNodes.size(); j++)
		for (size_t k = 0; k < _outputNodes[j][_outputs[j]]._synapses.size(); k++)
			sum += _outputNodes[j][_outputs[j]]._synapses[k]._weight * _hiddenGroups[j][_outputs[j]]._synapses[_currentInputs.size() + i]._weight * _hiddenGroups[j][_outputs[j]]._output * (1.0f - _hiddenGroups[j][_outputs[j]]._output);

		float coeff;

		// Output
		coeff = hNet * (wKc + sum) * _memoryCells[i]._outputGate._output * (1.0f - _memoryCells[i]._outputGate._output);

		_memoryCells[i]._outputGate._bias._weight += alphaTDError * _memoryCells[i]._outputGate._bias._trace;
		_memoryCells[i]._outputGate._bias._trace = gammaLambda * _memoryCells[i]._outputGate._bias._trace + coeff;

		si = 0;

		for (size_t j = 0; j < _memoryCells.size(); j++) {	
			_memoryCells[i]._outputGate._synapses[si]._weight += alphaTDError * _memoryCells[i]._outputGate._synapses[si]._trace;
			_memoryCells[i]._outputGate._synapses[si]._trace = gammaLambda * _memoryCells[i]._outputGate._synapses[si]._trace + coeff * _memoryCells[j]._output;

			si++;
		}

		for (size_t j = 0; j < _currentInputs.size(); j++) {
			_memoryCells[i]._outputGate._synapses[si]._weight += alphaTDError * _memoryCells[i]._outputGate._synapses[si]._trace;
			_memoryCells[i]._outputGate._synapses[si]._trace = gammaLambda * _memoryCells[i]._outputGate._synapses[si]._trace + coeff * _currentInputs[j];

			si++;
		}

		for (size_t j = 0; j < _hiddenGroups.size(); j++)
		for (size_t k = 0; k < _hiddenGroups[j].size(); k++) {
			_memoryCells[i]._outputGate._synapses[si]._weight += alphaTDError * _memoryCells[i]._outputGate._synapses[si]._trace;
			_memoryCells[i]._outputGate._synapses[si]._trace = gammaLambda * _memoryCells[i]._outputGate._synapses[si]._trace + coeff * _hiddenGroups[j][k]._output;

			si++;
		}

		// CEC
		coeff = (wKc + sum) * _memoryCells[i]._outputGate._output * hPrimeNet;

		const float sigmoidNetCEC = sigmoid(_memoryCells[i]._net);
		const float scaledSigmoidNetCEC = 4.0f * sigmoidNetCEC;
		const float gPrimeTimesInputGate = scaledSigmoidNetCEC * (1.0f - sigmoidNetCEC) * _memoryCells[i]._inputGate._output;

		_memoryCells[i]._bias._weight += alphaTDError * _memoryCells[i]._synapses[si]._trace;
		_memoryCells[i]._bias._trace = gammaLambda * _memoryCells[i]._synapses[si]._trace + coeff * _memoryCells[i]._synapses[si]._derivative;
		_memoryCells[i]._bias._derivative = _memoryCells[i]._synapses[si]._derivative * _memoryCells[i]._forgetGate._output + gPrimeTimesInputGate;

		si = 0;

		for (size_t j = 0; j < _memoryCells.size(); j++) {
			_memoryCells[i]._synapses[si]._weight += alphaTDError * _memoryCells[i]._synapses[si]._trace;
			_memoryCells[i]._synapses[si]._trace = gammaLambda * _memoryCells[i]._synapses[si]._trace + coeff * _memoryCells[i]._synapses[si]._derivative;
			_memoryCells[i]._synapses[si]._derivative = _memoryCells[i]._synapses[si]._derivative * _memoryCells[i]._forgetGate._output + gPrimeTimesInputGate * _memoryCells[j]._output;

			si++;
		}

		for (size_t j = 0; j < _currentInputs.size(); j++) {	
			_memoryCells[i]._synapses[si]._weight += alphaTDError * _memoryCells[i]._synapses[si]._trace;
			_memoryCells[i]._synapses[si]._trace = gammaLambda * _memoryCells[i]._synapses[si]._trace + coeff * _memoryCells[i]._synapses[si]._derivative;
			_memoryCells[i]._synapses[si]._derivative = _memoryCells[i]._synapses[si]._derivative * _memoryCells[i]._forgetGate._output + gPrimeTimesInputGate * _currentInputs[j];

			si++;
		}

		for (size_t j = 0; j < _hiddenGroups.size(); j++)
		for (size_t k = 0; k < _hiddenGroups[j].size(); k++) {
			_memoryCells[i]._synapses[si]._weight += alphaTDError * _memoryCells[i]._synapses[si]._trace;
			_memoryCells[i]._synapses[si]._trace = gammaLambda * _memoryCells[i]._synapses[si]._trace + coeff * _memoryCells[i]._synapses[si]._derivative;
			_memoryCells[i]._synapses[si]._derivative = _memoryCells[i]._synapses[si]._derivative * _memoryCells[i]._forgetGate._output + gPrimeTimesInputGate * _hiddenGroups[j][k]._output;

			si++;
		}

		// Input
		// coeff is unchanged from previous, since it is the same equation. Therefore the following line has been commented out
		//coeff = (wKc + sum) * _memoryCells[i]._outputGate._output * hPrimeNet;

		const float gTimesInputPrime = scaledSigmoidNetCEC * _memoryCells[i]._inputGate._output * (1.0f - _memoryCells[i]._inputGate._output);

		_memoryCells[i]._inputGate._bias._weight += alphaTDError * _memoryCells[i]._inputGate._synapses[si]._trace;
		_memoryCells[i]._inputGate._bias._trace = gammaLambda * _memoryCells[i]._inputGate._synapses[si]._trace + coeff * _memoryCells[i]._synapses[si]._derivative;
		_memoryCells[i]._inputGate._bias._derivative = _memoryCells[i]._synapses[si]._derivative * _memoryCells[i]._forgetGate._output + gTimesInputPrime;

		si = 0;

		for (size_t j = 0; j < _memoryCells.size(); j++) {
			_memoryCells[i]._inputGate._synapses[si]._weight += alphaTDError * _memoryCells[i]._inputGate._synapses[si]._trace;
			_memoryCells[i]._inputGate._synapses[si]._trace = gammaLambda * _memoryCells[i]._inputGate._synapses[si]._trace + coeff * _memoryCells[i]._synapses[si]._derivative;
			_memoryCells[i]._inputGate._synapses[si]._derivative = _memoryCells[i]._synapses[si]._derivative * _memoryCells[i]._forgetGate._output + gTimesInputPrime * _memoryCells[j]._output;

			si++;
		}

		for (size_t j = 0; j < _currentInputs.size(); j++) {
			_memoryCells[i]._inputGate._synapses[si]._weight += alphaTDError * _memoryCells[i]._inputGate._synapses[si]._trace;
			_memoryCells[i]._inputGate._synapses[si]._trace = gammaLambda * _memoryCells[i]._inputGate._synapses[si]._trace + coeff * _memoryCells[i]._synapses[si]._derivative;
			_memoryCells[i]._inputGate._synapses[si]._derivative = _memoryCells[i]._synapses[si]._derivative * _memoryCells[i]._forgetGate._output + gTimesInputPrime * _currentInputs[j];

			si++;
		}

		for (size_t j = 0; j < _hiddenGroups.size(); j++)
		for (size_t k = 0; k < _hiddenGroups[j].size(); k++) {			
			_memoryCells[i]._inputGate._synapses[si]._weight += alphaTDError * _memoryCells[i]._inputGate._synapses[si]._trace;			
			_memoryCells[i]._inputGate._synapses[si]._trace = gammaLambda * _memoryCells[i]._inputGate._synapses[si]._trace + coeff * _memoryCells[i]._synapses[si]._derivative;
			_memoryCells[i]._inputGate._synapses[si]._derivative = _memoryCells[i]._synapses[si]._derivative * _memoryCells[i]._forgetGate._output + gTimesInputPrime * _hiddenGroups[j][k]._output;

			si++;
		}

		// Forget
		// coeff is unchanged from previous, since it is the same equation. Therefore the following line has been commented out
		//coeff = (wKc + sum) * _memoryCells[i]._outputGate._output * hPrimeNet;

		const float stateTimesForgetPrime = _memoryCells[i]._state * _memoryCells[i]._forgetGate._output * (1.0f - _memoryCells[i]._forgetGate._output);

		_memoryCells[i]._forgetGate._bias._derivative = _memoryCells[i]._synapses[si]._derivative * _memoryCells[i]._forgetGate._output + gTimesInputPrime;
		_memoryCells[i]._forgetGate._bias._trace = gammaLambda * _memoryCells[i]._inputGate._synapses[si]._trace + coeff * _memoryCells[i]._synapses[si]._derivative;
		_memoryCells[i]._forgetGate._bias._weight += alphaTDError * _memoryCells[i]._inputGate._synapses[si]._trace;

		si = 0;

		for (size_t j = 0; j < _memoryCells.size(); j++) {
			_memoryCells[i]._forgetGate._synapses[si]._derivative = _memoryCells[i]._synapses[si]._derivative * _memoryCells[i]._forgetGate._output + stateTimesForgetPrime * _memoryCells[j]._prevOutput;
			_memoryCells[i]._forgetGate._synapses[si]._trace = gammaLambda * _memoryCells[i]._forgetGate._synapses[si]._trace + coeff * _memoryCells[i]._synapses[si]._derivative;
			_memoryCells[i]._forgetGate._synapses[si]._weight += alphaTDError * _memoryCells[i]._forgetGate._synapses[si]._trace;

			si++;
		}

		for (size_t j = 0; j < _currentInputs.size(); j++) {
			_memoryCells[i]._forgetGate._synapses[si]._weight += alphaTDError * _memoryCells[i]._forgetGate._synapses[si]._trace;
			_memoryCells[i]._forgetGate._synapses[si]._trace = gammaLambda * _memoryCells[i]._forgetGate._synapses[si]._trace + coeff * _memoryCells[i]._synapses[si]._derivative;
			_memoryCells[i]._forgetGate._synapses[si]._derivative = _memoryCells[i]._synapses[si]._derivative * _memoryCells[i]._forgetGate._output + stateTimesForgetPrime * _currentInputs[j];

			si++;
		}

		for (size_t j = 0; j < _hiddenGroups.size(); j++)
		for (size_t k = 0; k < _hiddenGroups[j].size(); k++) {
			_memoryCells[i]._forgetGate._synapses[si]._weight += alphaTDError * _memoryCells[i]._forgetGate._synapses[si]._trace;
			_memoryCells[i]._forgetGate._synapses[si]._trace = gammaLambda * _memoryCells[i]._forgetGate._synapses[si]._trace + coeff * _memoryCells[i]._synapses[si]._derivative;
			_memoryCells[i]._forgetGate._synapses[si]._derivative = _memoryCells[i]._synapses[si]._derivative * _memoryCells[i]._forgetGate._output + stateTimesForgetPrime * _hiddenGroups[j][k]._output;

			si++;
		}
	}

	// --------------------------------- Set Previous Outputs to Current ---------------------------------

	for (size_t i = 0; i < _hiddenGroups.size(); i++)
	for (size_t j = 0; j < _hiddenGroups[i].size(); j++)
		_hiddenGroups[i][j]._prevOutput = _hiddenGroups[i][j]._output;

	for (size_t i = 0; i < _memoryCells.size(); i++) {
		_memoryCells[i]._prevOutput = _memoryCells[i]._output;

		_memoryCells[i]._inputGate._prevOutput = _memoryCells[i]._inputGate._output;
		_memoryCells[i]._outputGate._prevOutput = _memoryCells[i]._outputGate._output;
		_memoryCells[i]._forgetGate._prevOutput = _memoryCells[i]._forgetGate._output;
	}

	for (size_t i = 0; i < _outputNodes.size(); i++)
	for (size_t j = 0; j < _outputNodes[i].size(); j++)
		_outputNodes[i][j]._prevOutput = _outputNodes[i][j]._output;
}

void MultiLSTMRL::mutate(float mutationRate, float maxPerturbation, std::mt19937 &generator) {
	std::uniform_real_distribution<float> uniformDist(0.0f, 1.0f);
	std::uniform_real_distribution<float> perturbationDist(-maxPerturbation, maxPerturbation);

	for (size_t i = 0; i < _hiddenGroups.size(); i++)
	for (size_t j = 0; j < _hiddenGroups[i].size(); j++) {
		_hiddenGroups[i][j]._bias._weight += uniformDist(generator) < mutationRate ? perturbationDist(generator) : 0.0f;

		for (size_t k = 0; k < _hiddenGroups[i][j]._synapses.size(); k++)
			_hiddenGroups[i][j]._synapses[k]._weight += uniformDist(generator) < mutationRate ? perturbationDist(generator) : 0.0f;
	}

	for (size_t i = 0; i < _memoryCells.size(); i++) {
		_memoryCells[i]._bias._weight += uniformDist(generator) < mutationRate ? perturbationDist(generator) : 0.0f;

		for (size_t j = 0; j < _memoryCells[i]._synapses.size(); j++)
			_memoryCells[i]._synapses[j]._weight += uniformDist(generator) < mutationRate ? perturbationDist(generator) : 0.0f;

		_memoryCells[i]._inputGate._bias._weight += uniformDist(generator) < mutationRate ? perturbationDist(generator) : 0.0f;

		for (size_t j = 0; j < _memoryCells[i]._inputGate._synapses.size(); j++)
			_memoryCells[i]._inputGate._synapses[j]._weight += uniformDist(generator) < mutationRate ? perturbationDist(generator) : 0.0f;

		_memoryCells[i]._outputGate._bias._weight += uniformDist(generator) < mutationRate ? perturbationDist(generator) : 0.0f;

		for (size_t j = 0; j < _memoryCells[i]._outputGate._synapses.size(); j++)
			_memoryCells[i]._outputGate._synapses[j]._weight += uniformDist(generator) < mutationRate ? perturbationDist(generator) : 0.0f;

		_memoryCells[i]._forgetGate._bias._weight += uniformDist(generator) < mutationRate ? perturbationDist(generator) : 0.0f;

		for (size_t j = 0; j < _memoryCells[i]._forgetGate._synapses.size(); j++)
			_memoryCells[i]._forgetGate._synapses[j]._weight += uniformDist(generator) < mutationRate ? perturbationDist(generator) : 0.0f;
	}

	for (size_t i = 0; i < _outputNodes.size(); i++)
	for (size_t j = 0; j < _outputNodes[i].size(); j++) {
		_outputNodes[i][j]._bias._weight += uniformDist(generator) < mutationRate ? perturbationDist(generator) : 0.0f;

		for (size_t k = 0; k < _outputNodes[i][j]._synapses.size(); k++)
			_outputNodes[i][j]._synapses[k]._weight += uniformDist(generator) < mutationRate ? perturbationDist(generator) : 0.0f;
	}
}

void MultiLSTMRL::crossover(const MultiLSTMRL &parent1, const MultiLSTMRL &parent2, float averageChance, size_t numInputs, size_t numOutputGroups, size_t numOutputsPerGroup, size_t hiddenSize, size_t numMemoryCells, float minWeight, float maxWeight, std::mt19937 &generator) {
	std::uniform_real_distribution<float> uniformDist(0.0f, 1.0f);
	
	_currentInputs.assign(numInputs, 0.0f);

	_outputNodes.resize(numOutputGroups);
	_hiddenGroups.resize(numOutputGroups);
	_memoryCells.resize(numMemoryCells);

	std::uniform_real_distribution<float> weightDist(minWeight, maxWeight);

	const size_t numOutputInputs = hiddenSize + numMemoryCells;
	const size_t numHiddenInputs = numInputs + numMemoryCells;
	const size_t numGateInputs = numMemoryCells * 3 + numInputs + numMemoryCells + hiddenSize * numOutputGroups * numOutputsPerGroup;
	const size_t numMemoryCellInputs = numInputs + numMemoryCells + hiddenSize * numOutputGroups * numOutputsPerGroup;

	bool fitsParent1, fitsParent2;

	for (size_t i = 0; i < numOutputGroups; i++) {
		_outputNodes[i].resize(numOutputsPerGroup);

		for (size_t j = 0; j < numOutputsPerGroup; j++) {
			fitsParent1 = i < parent1._outputNodes.size() && j < parent1._outputNodes[i].size();
			fitsParent2 = i < parent2._outputNodes.size() && j < parent2._outputNodes[i].size();

			if (fitsParent1 && fitsParent2)
				_outputNodes[i][j]._bias._weight = uniformDist(generator) < averageChance ? (parent1._outputNodes[i][j]._bias._weight + parent2._outputNodes[i][j]._bias._weight) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._outputNodes[i][j]._bias._weight : parent2._outputNodes[i][j]._bias._weight);
			else if (fitsParent1)
				_outputNodes[i][j]._bias._weight = parent1._outputNodes[i][j]._bias._weight;
			else if (fitsParent2)
				_outputNodes[i][j]._bias._weight = parent2._outputNodes[i][j]._bias._weight;
			else
				_outputNodes[i][j]._bias._weight = weightDist(generator);

			_outputNodes[i][j]._bias._trace = 0.0f;
			_outputNodes[i][j]._prevOutput = _outputNodes[i][j]._output = 0.0f;

			_outputNodes[i][j]._synapses.resize(numOutputInputs);

			for (size_t k = 0; k < _outputNodes[i][j]._synapses.size(); k++) {
				fitsParent1 = i < parent1._outputNodes.size() && j < parent1._outputNodes[i].size() && k < parent1._outputNodes[i][j]._synapses.size();
				fitsParent2 = i < parent2._outputNodes.size() && j < parent2._outputNodes[i].size() && k < parent2._outputNodes[i][j]._synapses.size();

				if (fitsParent1 && fitsParent2)
					_outputNodes[i][j]._synapses[k]._weight = uniformDist(generator) < averageChance ? (parent1._outputNodes[i][j]._synapses[k]._weight + parent2._outputNodes[i][j]._synapses[k]._weight) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._outputNodes[i][j]._synapses[k]._weight : parent2._outputNodes[i][j]._synapses[k]._weight);
				else if (fitsParent1)
					_outputNodes[i][j]._synapses[k]._weight = parent1._outputNodes[i][j]._synapses[k]._weight;
				else if (fitsParent2)
					_outputNodes[i][j]._synapses[k]._weight = parent2._outputNodes[i][j]._synapses[k]._weight;
				else
					_outputNodes[i][j]._synapses[k]._weight = weightDist(generator);

				_outputNodes[i][j]._synapses[j]._trace = 0.0f;
			}
		}

		_hiddenGroups[i].resize(hiddenSize);

		for (size_t j = 0; j < hiddenSize; j++) {
			fitsParent1 = i < parent1._hiddenGroups.size() && j < parent1._hiddenGroups[i].size();
			fitsParent2 = i < parent2._hiddenGroups.size() && j < parent2._hiddenGroups[i].size();

			if (fitsParent1 && fitsParent2)
				_hiddenGroups[i][j]._bias._weight = uniformDist(generator) < averageChance ? (parent1._hiddenGroups[i][j]._bias._weight + parent2._hiddenGroups[i][j]._bias._weight) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._hiddenGroups[i][j]._bias._weight : parent2._hiddenGroups[i][j]._bias._weight);
			else if (fitsParent1)
				_hiddenGroups[i][j]._bias._weight = parent1._hiddenGroups[i][j]._bias._weight;
			else if (fitsParent2)
				_hiddenGroups[i][j]._bias._weight = parent2._hiddenGroups[i][j]._bias._weight;
			else
				_hiddenGroups[i][j]._bias._weight = weightDist(generator);

			_hiddenGroups[i][j]._bias._trace = 0.0f;
			_hiddenGroups[i][j]._prevOutput = _hiddenGroups[i][j]._output = 0.0f;

			_hiddenGroups[i][j]._synapses.resize(numHiddenInputs);

			for (size_t k = 0; k < _hiddenGroups[i][j]._synapses.size(); k++) {
				fitsParent1 = i < parent1._hiddenGroups.size() && j < parent1._hiddenGroups[i].size() && k < parent1._hiddenGroups[i][j]._synapses.size();
				fitsParent2 = i < parent2._hiddenGroups.size() && j < parent2._hiddenGroups[i].size() && k < parent2._hiddenGroups[i][j]._synapses.size();

				if (fitsParent1 && fitsParent2)
					_hiddenGroups[i][j]._synapses[k]._weight = uniformDist(generator) < averageChance ? (parent1._hiddenGroups[i][j]._synapses[k]._weight + parent2._hiddenGroups[i][j]._synapses[k]._weight) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._hiddenGroups[i][j]._synapses[k]._weight : parent2._hiddenGroups[i][j]._synapses[k]._weight);
				else if (fitsParent1)
					_hiddenGroups[i][j]._synapses[k]._weight = parent1._hiddenGroups[i][j]._synapses[k]._weight;
				else if (fitsParent2)
					_hiddenGroups[i][j]._synapses[k]._weight = parent2._hiddenGroups[i][j]._synapses[k]._weight;
				else
					_hiddenGroups[i][j]._synapses[k]._weight = weightDist(generator);

				_hiddenGroups[i][j]._synapses[k]._trace = 0.0f;
			}
		}
	}

	for (size_t i = 0; i < numMemoryCells; i++) {
		fitsParent1 = i < parent1._memoryCells.size();
		fitsParent2 = i < parent2._memoryCells.size();

		if (fitsParent1 && fitsParent2) {
			_memoryCells[i]._outputGate._bias._weight = uniformDist(generator) < averageChance ? (parent1._memoryCells[i]._outputGate._bias._weight + parent2._memoryCells[i]._outputGate._bias._weight) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._memoryCells[i]._outputGate._bias._weight : parent2._memoryCells[i]._outputGate._bias._weight);
			_memoryCells[i]._inputGate._bias._weight = uniformDist(generator) < averageChance ? (parent1._memoryCells[i]._inputGate._bias._weight + parent2._memoryCells[i]._inputGate._bias._weight) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._memoryCells[i]._inputGate._bias._weight : parent2._memoryCells[i]._inputGate._bias._weight);
			_memoryCells[i]._forgetGate._bias._weight = uniformDist(generator) < averageChance ? (parent1._memoryCells[i]._forgetGate._bias._weight + parent2._memoryCells[i]._forgetGate._bias._weight) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._memoryCells[i]._forgetGate._bias._weight : parent2._memoryCells[i]._forgetGate._bias._weight);
			_memoryCells[i]._bias._weight = uniformDist(generator) < averageChance ? (parent1._memoryCells[i]._bias._weight + parent2._memoryCells[i]._bias._weight) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._memoryCells[i]._bias._weight : parent2._memoryCells[i]._bias._weight);
		}
		else if (fitsParent1) {
			_memoryCells[i]._outputGate._bias._weight = parent1._memoryCells[i]._outputGate._bias._weight;
			_memoryCells[i]._inputGate._bias._weight = parent1._memoryCells[i]._inputGate._bias._weight;
			_memoryCells[i]._forgetGate._bias._weight = parent1._memoryCells[i]._forgetGate._bias._weight;
			_memoryCells[i]._bias._weight = parent1._memoryCells[i]._bias._weight;
		}
		else if (fitsParent2) {
			_memoryCells[i]._outputGate._bias._weight = parent2._memoryCells[i]._outputGate._bias._weight;
			_memoryCells[i]._inputGate._bias._weight = parent2._memoryCells[i]._inputGate._bias._weight;
			_memoryCells[i]._forgetGate._bias._weight = parent2._memoryCells[i]._forgetGate._bias._weight;
			_memoryCells[i]._bias._weight = parent2._memoryCells[i]._bias._weight;
		}
		else {
			_memoryCells[i]._outputGate._bias._weight = weightDist(generator);
			_memoryCells[i]._inputGate._bias._weight = weightDist(generator);
			_memoryCells[i]._forgetGate._bias._weight = weightDist(generator);
			_memoryCells[i]._bias._weight = weightDist(generator);
		}
	
		_memoryCells[i]._outputGate._bias._trace = 0.0f;
		_memoryCells[i]._outputGate._prevOutput = _memoryCells[i]._outputGate._output = 0.0f;

		_memoryCells[i]._inputGate._bias._weight = weightDist(generator);
		_memoryCells[i]._inputGate._bias._trace = 0.0f;
		_memoryCells[i]._inputGate._bias._derivative = 0.0f;
		_memoryCells[i]._inputGate._prevOutput = _memoryCells[i]._inputGate._output = 0.0f;

		_memoryCells[i]._forgetGate._bias._weight = weightDist(generator);
		_memoryCells[i]._forgetGate._bias._trace = 0.0f;
		_memoryCells[i]._forgetGate._bias._derivative = 0.0f;
		_memoryCells[i]._forgetGate._prevOutput = _memoryCells[i]._forgetGate._output = 0.0f;

		_memoryCells[i]._net = 0.0f;
		_memoryCells[i]._bias._trace = 0.0f;
		_memoryCells[i]._bias._derivative = 0.0f;
		_memoryCells[i]._prevOutput = _memoryCells[i]._output = 0.0f;
		_memoryCells[i]._state = 0.0f;

		_memoryCells[i]._synapses.resize(numGateInputs);
		_memoryCells[i]._outputGate._synapses.resize(numGateInputs);
		_memoryCells[i]._inputGate._synapses.resize(numGateInputs);
		_memoryCells[i]._forgetGate._synapses.resize(numGateInputs);

		for (size_t j = 0; j < numGateInputs; j++) {
			fitsParent1 = i < parent1._memoryCells.size() && j < parent1._memoryCells[i]._outputGate._synapses.size();
			fitsParent2 = i < parent2._memoryCells.size() && j < parent2._memoryCells[i]._outputGate._synapses.size();

			if (fitsParent1 && fitsParent2) {
				_memoryCells[i]._outputGate._synapses[j]._weight = uniformDist(generator) < averageChance ? (parent1._memoryCells[i]._outputGate._synapses[j]._weight + parent2._memoryCells[i]._outputGate._synapses[j]._weight) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._memoryCells[i]._outputGate._synapses[j]._weight : parent2._memoryCells[i]._outputGate._synapses[j]._weight);
				_memoryCells[i]._inputGate._synapses[j]._weight = uniformDist(generator) < averageChance ? (parent1._memoryCells[i]._inputGate._synapses[j]._weight + parent2._memoryCells[i]._inputGate._synapses[j]._weight) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._memoryCells[i]._inputGate._synapses[j]._weight : parent2._memoryCells[i]._inputGate._synapses[j]._weight);
				_memoryCells[i]._forgetGate._synapses[j]._weight = uniformDist(generator) < averageChance ? (parent1._memoryCells[i]._forgetGate._synapses[j]._weight + parent2._memoryCells[i]._forgetGate._synapses[j]._weight) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._memoryCells[i]._forgetGate._synapses[j]._weight : parent2._memoryCells[i]._forgetGate._synapses[j]._weight);
				_memoryCells[i]._synapses[j]._weight = uniformDist(generator) < averageChance ? (parent1._memoryCells[i]._synapses[j]._weight + parent2._memoryCells[i]._synapses[j]._weight) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._memoryCells[i]._synapses[j]._weight : parent2._memoryCells[i]._synapses[j]._weight);
			}
			else if (fitsParent1) {
				_memoryCells[i]._outputGate._synapses[j]._weight = parent1._memoryCells[i]._outputGate._synapses[j]._weight;
				_memoryCells[i]._inputGate._synapses[j]._weight = parent1._memoryCells[i]._inputGate._synapses[j]._weight;
				_memoryCells[i]._forgetGate._synapses[j]._weight = parent1._memoryCells[i]._forgetGate._synapses[j]._weight;
				_memoryCells[i]._synapses[j]._weight = parent1._memoryCells[i]._synapses[j]._weight;
			}
			else if (fitsParent2) {
				_memoryCells[i]._outputGate._synapses[j]._weight = parent2._memoryCells[i]._outputGate._synapses[j]._weight;
				_memoryCells[i]._inputGate._synapses[j]._weight = parent2._memoryCells[i]._inputGate._synapses[j]._weight;
				_memoryCells[i]._forgetGate._synapses[j]._weight = parent2._memoryCells[i]._forgetGate._synapses[j]._weight;
				_memoryCells[i]._synapses[j]._weight = parent2._memoryCells[i]._synapses[j]._weight;
			}
			else {
				_memoryCells[i]._outputGate._synapses[j]._weight = weightDist(generator);
				_memoryCells[i]._inputGate._synapses[j]._weight = weightDist(generator);
				_memoryCells[i]._forgetGate._synapses[j]._weight = weightDist(generator);
				_memoryCells[i]._synapses[j]._weight = weightDist(generator);
			}

			_memoryCells[i]._outputGate._synapses[j]._trace = 0.0f;

			_memoryCells[i]._inputGate._synapses[j]._trace = 0.0f;
			_memoryCells[i]._inputGate._synapses[j]._derivative = 0.0f;

			_memoryCells[i]._forgetGate._synapses[j]._trace = 0.0f;
			_memoryCells[i]._forgetGate._synapses[j]._derivative = 0.0f;

			_memoryCells[i]._synapses[j]._trace = 0.0f;
			_memoryCells[i]._synapses[j]._derivative = 0.0f;
		}
	}

	_outputs.assign(numOutputGroups, 0);
}

void MultiLSTMRL::clearMemory() {
	for (size_t i = 0; i < _hiddenGroups.size(); i++)
	for (size_t j = 0; j < _hiddenGroups[i].size(); j++) {
		_hiddenGroups[i][j]._bias._trace = 0.0f;

		_hiddenGroups[i][j]._output = 0.0f;
		_hiddenGroups[i][j]._prevOutput = 0.0f;

		for (size_t k = 0; k < _hiddenGroups[i][j]._synapses.size(); k++)
			_hiddenGroups[i][j]._synapses[k]._trace = 0.0f;
	}

	for (size_t i = 0; i < _memoryCells.size(); i++) {
		_memoryCells[i]._net = 0.0f;
		_memoryCells[i]._state = 0.0f;

		_memoryCells[i]._bias._trace = 0.0f;

		_memoryCells[i]._output = 0.0f;
		_memoryCells[i]._prevOutput = 0.0f;

		for (size_t j = 0; j < _memoryCells[i]._synapses.size(); j++) {
			_memoryCells[i]._synapses[j]._trace = 0.0f;
			_memoryCells[i]._synapses[j]._derivative = 0.0f;
		}

		_memoryCells[i]._inputGate._bias._trace = 0.0f;

		_memoryCells[i]._inputGate._output = 0.0f;
		_memoryCells[i]._inputGate._prevOutput = 0.0f;

		for (size_t j = 0; j < _memoryCells[i]._inputGate._synapses.size(); j++) {
			_memoryCells[i]._inputGate._synapses[j]._trace = 0.0f;
			_memoryCells[i]._inputGate._synapses[j]._derivative = 0.0f;
		}

		_memoryCells[i]._outputGate._bias._trace = 0.0f;

		_memoryCells[i]._outputGate._output = 0.0f;
		_memoryCells[i]._outputGate._prevOutput = 0.0f;

		for (size_t j = 0; j < _memoryCells[i]._outputGate._synapses.size(); j++)
			_memoryCells[i]._outputGate._synapses[j]._trace = 0.0f;

		_memoryCells[i]._forgetGate._bias._trace = 0.0f;

		_memoryCells[i]._forgetGate._output = 0.0f;
		_memoryCells[i]._forgetGate._prevOutput = 0.0f;

		for (size_t j = 0; j < _memoryCells[i]._forgetGate._synapses.size(); j++) {
			_memoryCells[i]._forgetGate._synapses[j]._trace = 0.0f;
			_memoryCells[i]._forgetGate._synapses[j]._derivative = 0.0f;
		}
	}

	for (size_t i = 0; i < _outputNodes.size(); i++)
	for (size_t j = 0; j < _outputNodes[i].size(); j++) {
		_outputNodes[i][j]._bias._trace = 0.0f;

		for (size_t k = 0; k < _outputNodes[i][j]._synapses.size(); k++)
			_outputNodes[i][j]._synapses[k]._trace = 0.0f;
	}
}

void MultiLSTMRL::saveToFile(const std::string &fileName) {
	std::ofstream toFile(fileName);

	assert(toFile.is_open());

	const size_t numInputs = _currentInputs.size();
	const size_t numOutputGroups = _outputNodes.size();
	const size_t numOutputsPerGroup = _outputNodes[0].size();
	const size_t hiddenSize = _hiddenGroups[0].size();
	const size_t numMemoryCells = _memoryCells.size();

	toFile << numInputs << " " << numOutputGroups << " " << numOutputsPerGroup << " " << hiddenSize << " " << numMemoryCells << std::endl;

	const size_t numOutputInputs = hiddenSize + numMemoryCells;
	const size_t numHiddenInputs = numInputs + numMemoryCells;
	const size_t numGateInputs = numMemoryCells * 3 + numInputs + numMemoryCells + hiddenSize * numOutputGroups * numOutputsPerGroup;
	const size_t numMemoryCellInputs = numInputs + numMemoryCells + hiddenSize * numOutputGroups * numOutputsPerGroup;
	
	for (size_t i = 0; i < numOutputGroups; i++) {
		for (size_t j = 0; j < numOutputsPerGroup; j++) {
			toFile << _outputNodes[i][j]._bias._weight << " ";

			for (size_t k = 0; k < _outputNodes[i][j]._synapses.size(); k++)
				toFile << _outputNodes[i][j]._synapses[j]._weight << " ";

			toFile << std::endl;
		}

		_hiddenGroups[i].resize(hiddenSize);

		for (size_t j = 0; j < hiddenSize; j++) {
			toFile << _hiddenGroups[i][j]._bias._weight << " ";

			for (size_t k = 0; k < _hiddenGroups[i][j]._synapses.size(); k++)
				toFile << _hiddenGroups[i][j]._synapses[k]._weight << " ";

			toFile << std::endl;
		}
	}

	for (size_t i = 0; i < numMemoryCells; i++) {
		toFile << _memoryCells[i]._outputGate._bias._weight << " ";

		toFile << _memoryCells[i]._inputGate._bias._weight << " ";

		toFile << _memoryCells[i]._forgetGate._bias._weight << " ";

		toFile << _memoryCells[i]._bias._weight << " ";

		for (size_t j = 0; j < numGateInputs; j++) {
			toFile << _memoryCells[i]._outputGate._synapses[j]._weight << " ";

			toFile << _memoryCells[i]._inputGate._synapses[j]._weight << " ";

			toFile << _memoryCells[i]._forgetGate._synapses[j]._weight << " ";

			toFile << _memoryCells[i]._synapses[j]._weight << " ";
		}
	}

	toFile.close();
}

bool MultiLSTMRL::createFromFile(const std::string &fileName) {
	std::ifstream fromFile(fileName);

	if (!fromFile.is_open())
		return false;

	size_t numInputs;
	size_t numOutputGroups;
	size_t numOutputsPerGroup;
	size_t hiddenSize;
	size_t numMemoryCells;

	fromFile >> numInputs >> numOutputGroups >> numOutputsPerGroup >> hiddenSize >> numMemoryCells;

	_currentInputs.assign(numInputs, 0.0f);

	_outputNodes.resize(numOutputGroups);
	_hiddenGroups.resize(numOutputGroups);
	_memoryCells.resize(numMemoryCells);

	const size_t numOutputInputs = hiddenSize + numMemoryCells;
	const size_t numHiddenInputs = numInputs + numMemoryCells;
	const size_t numGateInputs = numMemoryCells * 3 + numInputs + numMemoryCells + hiddenSize * numOutputGroups * numOutputsPerGroup;
	const size_t numMemoryCellInputs = numInputs + numMemoryCells + hiddenSize * numOutputGroups * numOutputsPerGroup;

	for (size_t i = 0; i < numOutputGroups; i++) {
		_outputNodes[i].resize(numOutputsPerGroup);

		for (size_t j = 0; j < numOutputsPerGroup; j++) {
			fromFile >> _outputNodes[i][j]._bias._weight;
			_outputNodes[i][j]._bias._trace = 0.0f;
			_outputNodes[i][j]._prevOutput = _outputNodes[i][j]._output = 0.0f;

			_outputNodes[i][j]._synapses.resize(numOutputInputs);

			for (size_t k = 0; k < _outputNodes[i][j]._synapses.size(); k++) {
				fromFile >> _outputNodes[i][j]._synapses[j]._weight;
				_outputNodes[i][j]._synapses[j]._trace = 0.0f;
			}
		}

		_hiddenGroups[i].resize(hiddenSize);

		for (size_t j = 0; j < hiddenSize; j++) {
			fromFile >> _hiddenGroups[i][j]._bias._weight;
			_hiddenGroups[i][j]._bias._trace = 0.0f;
			_hiddenGroups[i][j]._prevOutput = _hiddenGroups[i][j]._output = 0.0f;

			_hiddenGroups[i][j]._synapses.resize(numHiddenInputs);

			for (size_t k = 0; k < _hiddenGroups[i][j]._synapses.size(); k++) {
				fromFile >> _hiddenGroups[i][j]._synapses[k]._weight;
				_hiddenGroups[i][j]._synapses[k]._trace = 0.0f;
			}
		}
	}

	for (size_t i = 0; i < numMemoryCells; i++) {
		fromFile >> _memoryCells[i]._outputGate._bias._weight;
		_memoryCells[i]._outputGate._bias._trace = 0.0f;
		_memoryCells[i]._outputGate._prevOutput = _memoryCells[i]._outputGate._output = 0.0f;

		fromFile >> _memoryCells[i]._inputGate._bias._weight;
		_memoryCells[i]._inputGate._bias._trace = 0.0f;
		_memoryCells[i]._inputGate._bias._derivative = 0.0f;
		_memoryCells[i]._inputGate._prevOutput = _memoryCells[i]._inputGate._output = 0.0f;

		fromFile >> _memoryCells[i]._forgetGate._bias._weight;
		_memoryCells[i]._forgetGate._bias._trace = 0.0f;
		_memoryCells[i]._forgetGate._bias._derivative = 0.0f;
		_memoryCells[i]._forgetGate._prevOutput = _memoryCells[i]._forgetGate._output = 0.0f;

		_memoryCells[i]._net = 0.0f;
		fromFile >> _memoryCells[i]._bias._weight;
		_memoryCells[i]._bias._trace = 0.0f;
		_memoryCells[i]._bias._derivative = 0.0f;
		_memoryCells[i]._prevOutput = _memoryCells[i]._output = 0.0f;
		_memoryCells[i]._state = 0.0f;

		_memoryCells[i]._outputGate._synapses.resize(numGateInputs);
		_memoryCells[i]._inputGate._synapses.resize(numGateInputs);
		_memoryCells[i]._forgetGate._synapses.resize(numGateInputs);
		_memoryCells[i]._synapses.resize(numGateInputs);

		for (size_t j = 0; j < numGateInputs; j++) {
			fromFile >> _memoryCells[i]._outputGate._synapses[j]._weight;
			_memoryCells[i]._outputGate._synapses[j]._trace = 0.0f;

			fromFile >> _memoryCells[i]._inputGate._synapses[j]._weight;
			_memoryCells[i]._inputGate._synapses[j]._trace = 0.0f;
			_memoryCells[i]._inputGate._synapses[j]._derivative = 0.0f;

			fromFile >> _memoryCells[i]._forgetGate._synapses[j]._weight;
			_memoryCells[i]._forgetGate._synapses[j]._trace = 0.0f;
			_memoryCells[i]._forgetGate._synapses[j]._derivative = 0.0f;

			fromFile >> _memoryCells[i]._synapses[j]._weight;
			_memoryCells[i]._synapses[j]._trace = 0.0f;
			_memoryCells[i]._synapses[j]._derivative = 0.0f;
		}
	}

	_outputs.assign(numOutputGroups, 0);

	fromFile.close();

	return true;
}