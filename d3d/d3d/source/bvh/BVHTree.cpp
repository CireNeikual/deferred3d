#include <bvh/BVHTree.h>

#include <assert.h>

using namespace d3d;

BVHTree::BVHTree()
: _pRootNode(nullptr), _maxSplitsAfterNoTriangleReduction(16)
{}

void BVHTree::create(const AABB3D &rootRegion)
{
	_pRootNode.reset(new BVHNode(this, NULL));
	_pRootNode->_aabb = rootRegion;
}

void BVHTree::add(FormTriangle &triangle)
{
	assert(_pRootNode != NULL);

	_pRootNode->add(triangle, triangle.getAABB());
}

bool BVHTree::rayTrace(const Vec3f &origin, const Vec3f &direction,
	FormTriangle &triangle, Vec3f &point, Vec3f &normal)
{
	// Implement me!

	return false;
}
