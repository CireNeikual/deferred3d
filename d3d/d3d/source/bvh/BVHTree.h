#pragma once

#include <bvh/BVHNode.h>
#include <system/Uncopyable.h>

#include <constructs/AABB3D.h>

#include <geometry/FormTriangle.h>

#include <array>

namespace d3d {
	class BVHTree : public Uncopyable {
	private:
		std::unique_ptr<BVHNode> _pRootNode;

	public:
		unsigned int _maxSplitsAfterNoTriangleReduction;

		BVHTree();

		void create(const AABB3D &rootRegion);
		void add(FormTriangle &triangle);

		void compile() {
			// TODO: Remove useless (empty) nodes
			_pRootNode->split(0);
		}

		bool rayTrace(const Vec3f &origin, const Vec3f &direction,
			FormTriangle &triangle, Vec3f &point, Vec3f &normal);

		const BVHNode* getRootNode() const {
			return _pRootNode.get();
		}

		friend BVHNode;
	};
}