#include "Packet.h"

#include <sceneobjects/network/SceneObjectClient.h>
#include <sceneobjects/network/SceneObjectGameServer.h>

void d3d::Packet::ProcessClient()
{

}

void d3d::Packet::ProcessServer()
{

}

void d3d::Packet::WriteToMessage(RakNet::BitStream &message)
{
	message.Write(_id);
}

const unsigned char d3d::Packet::GetID() const
{
	return _id;
}

void d3d::Packet::SetId(unsigned char id)
{
	_id = id;
}
