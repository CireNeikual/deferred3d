#include "PacketList.h"

//pliststatic
pliststatic::pliststatic()
{
	idmap.insert(std::pair<unsigned char, std::function<std::shared_ptr<d3d::Packet>()>>(PacketSendMessage::ID, PacketSendMessage::Create));
}

pliststatic::~pliststatic()
{

}

//packetlist
pliststatic PacketList::_stdata;

std::shared_ptr<d3d::Packet> PacketList::ConstructPacket(unsigned char id)
{
	if (_stdata.idmap.count(id) == 1)
		return (_stdata.idmap[id])();
	return nullptr;
}