#pragma once

#include <network/RakWin.h>

#include <unordered_map>
#include <functional>

#include <MessageIdentifiers.h>

#include <network/Packet.h>
#include <network/packets/PacketSendMessage.h>

/*workaround class to simulate a static constructor.
In pliststatic's constructor, hardcode idmap to contain all of the packet class's ids and a pointer to a static function
to create them. */
class pliststatic
{
	friend class PacketList;
public:
	pliststatic();
	~pliststatic();
private:
	std::unordered_map<unsigned char, std::function<std::shared_ptr<d3d::Packet>()>> idmap;
};


/*singleton factory used to construct a packet of a given id type*/
class PacketList
{
public:
	static std::shared_ptr<d3d::Packet> ConstructPacket(unsigned char id);
private:
	static pliststatic _stdata;
};