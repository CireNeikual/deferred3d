#pragma once

#include <network/RakWin.h>

#include <memory>
#include <vector>

#include <RakPeerInterface.h>
#include <BitStream.h>
#include <MessageIdentifiers.h>

#include <scene/Scene.h>
#include <scene/SceneObjectRef.h>

namespace d3d
{
	class Packet
	{
		friend class SceneObjectClient;
		friend class SceneObjectGameServer;
	public:
		typedef std::shared_ptr<Packet> Ptr;

		virtual bool ReadFromMessage(RakNet::BitStream &message) = 0; //Returns true if the received packet contained the appropriate number of bytes to parse.
		virtual void WriteToMessage(RakNet::BitStream &message);

		virtual void ProcessClient();
		virtual void ProcessServer();

		void SetId(unsigned char id);
		const unsigned char GetID() const;

	protected:
		unsigned char _id;

		d3d::Scene* _scene;

		d3d::SceneObjectRef _client;
		d3d::SceneObjectRef _server;

		RakNet::RakNetGUID _sender;

		PacketReliability _reliability = PacketReliability::RELIABLE_ORDERED;
		PacketPriority _priority = PacketPriority::MEDIUM_PRIORITY;
		char _channel = 0;
	};
}
