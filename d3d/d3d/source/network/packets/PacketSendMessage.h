#pragma once

#include <network/Packet.h>
#include <sceneobjects/gui/SceneObjectChatBox.h>
#include <sceneobjects/network/SceneObjectGameServer.h>

class PacketSendMessage :
	public d3d::Packet
{
public:
	static const int ID = ID_USER_PACKET_ENUM + 1;

	PacketSendMessage();
	PacketSendMessage(const std::string &message);

	static std::shared_ptr<PacketSendMessage> Create();

	void WriteToMessage(RakNet::BitStream &message);
	bool ReadFromMessage(RakNet::BitStream &message);

	void ProcessClient();
	void ProcessServer();

private:
	RakNet::RakString _message;
	static d3d::SceneObjectRef _chatbox;
};

