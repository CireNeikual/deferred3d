#include "PacketSendMessage.h"


d3d::SceneObjectRef PacketSendMessage::_chatbox = nullptr;

PacketSendMessage::PacketSendMessage()
{
	_id = ID_USER_PACKET_ENUM + 1;
}

PacketSendMessage::PacketSendMessage(const std::string &message) : _message(message.c_str())
{
	_id = ID_USER_PACKET_ENUM + 1;
}

std::shared_ptr<PacketSendMessage> PacketSendMessage::Create()
{
	return std::shared_ptr<PacketSendMessage>(new PacketSendMessage());
}

void PacketSendMessage::WriteToMessage(RakNet::BitStream &message)
{
	Packet::WriteToMessage(message);
	message.Write(_message);
}

bool PacketSendMessage::ReadFromMessage(RakNet::BitStream &message)
{
	return message.Read(_message);
}

void PacketSendMessage::ProcessClient()
{
	if (_chatbox == nullptr)
	{
		_chatbox = _scene->getNamed("chatbox");
		std::cout << "Having to search the tree" << std::endl;
	}

	if (_chatbox != nullptr)
	{
		SceneObjectChatBox* chatbox = static_cast<SceneObjectChatBox*>(_chatbox.get());
		chatbox->addMessage("???", std::string(_message.C_String()));
	}
}

void PacketSendMessage::ProcessServer()
{
	d3d::SceneObjectGameServer* server = static_cast<d3d::SceneObjectGameServer*>(_server.get());
	
	PacketSendMessage message(*this);
	server->send(message);
}