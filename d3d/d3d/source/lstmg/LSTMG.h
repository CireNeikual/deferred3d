#pragma once

#include <unordered_map>
#include <lstmg/TupleHash.h>
#include <random>

namespace lstmg {
	class LSTMG {
	public:
		struct Connection {
			float _weight;
			float _trace;
			int _gaterIndex;

			float _eligibility;
		};

		struct Unit {
			float _state;
			float _prevState;
			float _activation;

			float _bias;
			float _biasEligibility;
	
			std::vector<int> _ingoingConnectionIndices;
			std::vector<int> _outgoingConnectionIndices;
		};

		static float sigmoid(float x) {
			return 1.0f / (1.0f + std::expf(-x));
		}

		static float sigmoidDerivative(float x) {
			float s = sigmoid(x);

			return s * (1.0f - s);
		}

	private:
		std::vector<int> _inputIndices;
		std::vector<int> _outputIndices;

		std::vector<Unit> _units;

		std::unordered_map<std::tuple<int, int>, Connection> _connections;
		std::unordered_map<std::tuple<int, int>, int> _gaterIndices;
		std::unordered_map<int, std::vector<std::tuple<int, int>>> _reverseGaterIndices;
		std::vector<int> _orderedGaterIndices;
		std::unordered_map<std::tuple<int, int, int>, float> _extendedTraces;

		std::unordered_map<std::tuple<int, int>, float> _prevGains;
		std::unordered_map<std::tuple<int, int>, float> _prevActivations;

		float gain(int j, int i);
		float theTerm(int j, int k);

	public:
		void createRandomLayered(int numInputs, int numOutputs,
			int numMemoryLayers, int memoryLayerSize, int numHiddenLayers, int hiddenLayerSize,
			float minWeight, float maxWeight, std::mt19937 &randomGenerator);

		void step(bool linearOutput);

		void getDeltas(const std::vector<float> &targets, float eligibilityDecay, bool linearOutput);

		void moveAlongDeltas(float error);

		void setInput(int index, float value) {
			_units[_inputIndices[index]]._activation = value;
		}

		float getInput(int index) const {
			return _units[_inputIndices[index]]._activation;
		}

		float getOutput(int index) const {
			return _units[_outputIndices[index]]._activation;
		}

		void clear();

		void mutate(float mutationRate, float mutationStdDev, std::mt19937 &randomGenerator);
		void crossover(const LSTMG &parent1, const LSTMG &parent2, float averageChance, int numInputs, int numOutputs,
			int numMemoryLayers, int memoryLayerSize, int numHiddenLayers, int hiddenLayerSize, float minWeight, float maxWeight, std::mt19937 &randomGenerator);
	};
}