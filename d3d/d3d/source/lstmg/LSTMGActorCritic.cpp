#include <lstmg/LSTMGActorCritic.h>

#include <algorithm>

#include <iostream>

using namespace lstmg;

void LSTMGActorCritic::createRandom(int numInputs, int numOutputs,
	int actorNumHiddenLayers, int actorHiddenLayerSize,
	int numActorMemoryCells, int numActorMemoryCellLayers,
	int numCriticHiddenLayers, int criticHiddenLayerSize,
	int numCriticMemoryCells, int numCriticMemoryCellLayers,
	float minWeight, float maxWeight, std::mt19937 &generator)
{
	_actor.createRandomLayered(numInputs, numOutputs, numActorMemoryCellLayers, numActorMemoryCells, actorNumHiddenLayers, actorHiddenLayerSize, minWeight, maxWeight, generator);
	_critic.createRandomLayered(numInputs + numOutputs, 1, numCriticMemoryCellLayers, numCriticMemoryCells, numCriticHiddenLayers, criticHiddenLayerSize, minWeight, maxWeight, generator);

	_currentInputs.assign(numInputs, 0.0f);
	_currentOutputs.assign(numOutputs, 0.0f);

	for (size_t i = 0; i < _currentInputs.size(); i++) {
		_actor.setInput(i, _currentInputs[i]);
		_critic.setInput(i, _currentInputs[i]);
	}

	_actor.step(true);
	_critic.step(true);
}

void LSTMGActorCritic::step(float reward, float actorAlpha, float offsetStdDev, float criticAlpha, float gamma, float eligibiltyDecay, std::mt19937 &generator) {
	std::vector<float> prevInputs(_currentInputs.size());

	for (size_t i = 0; i < _currentInputs.size(); i++) {
		prevInputs[i] = _actor.getInput(i);

		_actor.setInput(i, _currentInputs[i]);
	}

	// Get outputs
	for (size_t i = 0; i < _currentOutputs.size(); i++)
		_critic.setInput(i + _currentInputs.size(), _actor.getOutput(i));

	LSTMG nextCritic = _critic;

	nextCritic.step(true);

	float q = reward + gamma * nextCritic.getOutput(0);

	_error = q - _prevValue;

	_critic.getDeltas(std::vector<float>(1, _critic.getOutput(0) + 1.0f), eligibiltyDecay, true);

	_critic.moveAlongDeltas(criticAlpha * _error);

	for (size_t i = 0; i < _currentInputs.size(); i++)
		_critic.setInput(i, _currentInputs[i]);

	_critic.step(true);

	//if (_error > 0.0f)
	_actor.moveAlongDeltas(actorAlpha * _error);

	_prevValue = _critic.getOutput(0);

	_actor.step(true);

	std::normal_distribution<float> distOffset(0.0f, offsetStdDev);

	for (size_t i = 0; i < _currentOutputs.size(); i++)
		_currentOutputs[i] = std::min(1.0f, std::max(-1.0f, _actor.getOutput(i))) + distOffset(generator);

	_actor.getDeltas(_currentOutputs, eligibiltyDecay, true);

	std::cout << _prevValue << " " << _error << std::endl;
}