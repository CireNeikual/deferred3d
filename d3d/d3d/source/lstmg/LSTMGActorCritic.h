#pragma once

#include <lstmg/LSTMG.h>

namespace lstmg {
	class LSTMGActorCritic {
	private:
		LSTMG _actor;
		LSTMG _critic;

		std::vector<float> _currentInputs;
		std::vector<float> _currentOutputs;

		float _prevValue;
		float _error;

	public:
		LSTMGActorCritic()
			: _prevValue(0.0f), _error(0.0f)
		{}

		void createRandom(int numInputs, int numOutputs,
			int actorNumHiddenLayers, int actorHiddenLayerSize,
			int numActorMemoryCells, int numActorMemoryCellLayers,
			int numCriticHiddenLayers, int criticHiddenLayerSize,
			int numCriticMemoryCells, int numCriticMemoryCellLayers,
			float minWeight, float maxWeight, std::mt19937 &generator);

		void step(float reward, float actorAlpha, float offsetStdDev, float criticAlpha, float gamma, float eligibiltyDecay, std::mt19937 &generator);

		void setInput(size_t index, float value) {
			_currentInputs[index] = value;
		}

		float getInput(size_t index) const {
			return _currentInputs[index];
		}

		float getOutput(size_t index) const {
			return _currentOutputs[index];
		}
	};
}