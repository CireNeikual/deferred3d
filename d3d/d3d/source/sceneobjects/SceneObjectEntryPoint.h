#pragma once

#include <scene/Scene.h>

class SceneObjectEntryPoint : public d3d::SceneObject {
public:
	// Inherited from SceneObject
	void onAdd();

	SceneObject* copyFactory() {
		return new SceneObjectEntryPoint(*this);
	}
};