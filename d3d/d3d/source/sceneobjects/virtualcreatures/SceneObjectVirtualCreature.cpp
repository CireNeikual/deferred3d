#include <sceneobjects/virtualcreatures/SceneObjectVirtualCreature.h>

void SceneObjectVirtualCreature::onDestroy() {
	if (_physicsWorld.isAlive()) {
		d3d::SceneObjectPhysicsWorld* pPhysicsWorld = static_cast<d3d::SceneObjectPhysicsWorld*>(_physicsWorld.get());

		for (size_t i = 0; i < _limbs.size(); i++) {
			if (_limbs[i]._jointMotor != nullptr)
				pPhysicsWorld->_pDynamicsWorld->removeMultiBodyConstraint(_limbs[i]._jointMotor.get());

			pPhysicsWorld->_pDynamicsWorld->removeMultiBodyConstraint(_limbs[i]._constraint.get());

			pPhysicsWorld->_pDynamicsWorld->removeCollisionObject(_limbs[i]._pLinkCollider.get());
		}

		pPhysicsWorld->_pDynamicsWorld->removeMultiBody(_multiBody.get());
	}
}

btTransform SceneObjectVirtualCreature::getTransform() const {
	btTransform t = btTransform::getIdentity();

	t.setOrigin(_multiBody->getBasePos());
	t.setRotation(_multiBody->getWorldToBaseRot());

	return t;
}

void SceneObjectVirtualCreature::setTransform(const btTransform &transform) {
	_multiBody->setBasePos(transform.getOrigin());
	_multiBody->setWorldToBaseRot(transform.getRotation());
}

void SceneObjectVirtualCreature::updateAABB() {
	assert(_multiBody != nullptr);

	btVector3 lower, upper;

	_limbs[0]._pCollisionShape->getAabb(_limbs[0]._pLinkCollider->getWorldTransform(), lower, upper);

	_aabb._lowerBound = cons(lower);
	_aabb._upperBound = cons(upper);

	for (size_t i = 0; i < _limbs.size(); i++) {
		_limbs[i]._pCollisionShape->getAabb(_limbs[i]._pLinkCollider->getWorldTransform(), lower, upper);

		d3d::AABB3D subAABB(cons(lower), cons(upper));

		_aabb.expand(subAABB);
	}

	_aabb.calculateHalfDims();
	_aabb.calculateCenter();
}

int SceneObjectVirtualCreature::genLimbs(const VirtualCreatureGenes &vc, const VirtualCreatureGenes::LimbGenes &genes, int parentIndex, btMultiBody* pMultiBody, const btTransform &transform, btMultiBodyDynamicsWorld* pWorld, int &linkIndex, bool flipped) {
	_limbs.push_back(Limb());

	int currentIndex = _limbs.size() - 1;

	_limbs[currentIndex]._linkIndex = linkIndex;
	_limbs[currentIndex]._genes = genes;

	btTransform localTransform = btTransform::getIdentity();

	const float ccdMotionThreshold = 0.3f;
	const float ccdRadiusScalar = 0.9f;

	if (parentIndex == -1) {
		_limbs[currentIndex]._pCollisionShape.reset(new btBoxShape(bt(genes._halfDims)));

		btVector3 inertia;

		float mass = genes._density * 8.0f * genes._halfDims.magnitude();

		_limbs[currentIndex]._pCollisionShape->calculateLocalInertia(mass, inertia);

		_limbs[currentIndex]._pLinkCollider.reset(new btMultiBodyLinkCollider(pMultiBody, linkIndex));

		_limbs[currentIndex]._pLinkCollider->setCollisionShape(_limbs[currentIndex]._pCollisionShape.get());

		_limbs[currentIndex]._pLinkCollider->setWorldTransform(transform);

		pWorld->addCollisionObject(_limbs[currentIndex]._pLinkCollider.get(), 5, ~5);

		pMultiBody->setBaseCollider(_limbs[currentIndex]._pLinkCollider.get());

		_limbs[currentIndex]._pLinkCollider->setCcdMotionThreshold(ccdMotionThreshold);
		_limbs[currentIndex]._pLinkCollider->setCcdSweptSphereRadius(std::min(genes._halfDims.x, std::min(genes._halfDims.y, genes._halfDims.z)) * ccdRadiusScalar);

		localTransform = btTransform::getIdentity();
	}
	else {
		_limbs[currentIndex]._pCollisionShape.reset(new btBoxShape(bt(genes._halfDims)));

		btVector3 inertia;

		float mass = genes._density * 8.0f * genes._halfDims.magnitude();

		_limbs[currentIndex]._pCollisionShape->calculateLocalInertia(mass, inertia);

		btVector3 hingeAxis;

		switch (genes._hingeAxis) {
		case 0:
			hingeAxis = btVector3(1.0f, 0.0f, 0.0f);
			break;
		case 1:
			hingeAxis = btVector3(0.0f, 1.0f, 0.0f);
			break;
		case 2:
			hingeAxis = btVector3(0.0f, 0.0f, 1.0f);
			break;
		}

		d3d::Vec3f parentNormPos = genes._attachmentOnParent;
		d3d::Vec3f childNormPos = genes._attachmentOnThis - vc._jointBias;
		btQuaternion parentSurfaceRotation = btQuaternion::getIdentity();
		//btQuaternion childSurfaceRotation = btQuaternion::getIdentity();

		// Max out largest magnitude dimension
		{
			std::array<float, 3> normPosArray;

			normPosArray[0] = parentNormPos.x;
			normPosArray[1] = parentNormPos.y;
			normPosArray[2] = parentNormPos.z;

			int maxIndex = 0;

			for (int j = 1; j < 3; j++)
			if (std::fabsf(normPosArray[j]) > std::fabsf(normPosArray[maxIndex]))
				maxIndex = j;

			normPosArray[maxIndex] = d3d::sign(normPosArray[maxIndex]);

			switch (maxIndex) {
			case 0:
				parentSurfaceRotation = btQuaternion(btVector3(0.0f, 1.0f, 0.0f), normPosArray[maxIndex] > 0.0f ? d3d::_pi : 0.0f);
				break;
			case 1:
				parentSurfaceRotation = btQuaternion(btVector3(0.0f, 0.0f, 1.0f), normPosArray[maxIndex] > 0.0f ? d3d::_piOver2 : -d3d::_piOver2);
				break;
			case 2:
				parentSurfaceRotation = btQuaternion(btVector3(0.0f, 1.0f, 0.0f), normPosArray[maxIndex] > 0.0f ? -d3d::_piOver2 : d3d::_piOver2);
				break;
			}

			parentNormPos.x = normPosArray[0];
			parentNormPos.y = normPosArray[1];
			parentNormPos.z = normPosArray[2];
		}

		{
			std::array<float, 3> normPosArray;

			normPosArray[0] = childNormPos.x;
			normPosArray[1] = childNormPos.y;
			normPosArray[2] = childNormPos.z;

			int maxIndex = 0;

			for (int j = 1; j < 3; j++)
			if (std::fabsf(normPosArray[j]) > std::fabsf(normPosArray[maxIndex]))
				maxIndex = j;

			normPosArray[maxIndex] = d3d::sign(normPosArray[maxIndex]);

			childNormPos.x = normPosArray[0];
			childNormPos.y = normPosArray[1];
			childNormPos.z = normPosArray[2];
		}

		if (flipped) {
			parentNormPos.z *= -1.0f;
			childNormPos.z *= -1.0f;
		}

		btVector3 parentPos = bt(_limbs[parentIndex]._genes._halfDims * parentNormPos);
		btVector3 childPos = bt(genes._halfDims * childNormPos);

		btQuaternion rot = parentSurfaceRotation * btQuaternion(genes._relativeRotation.y, genes._relativeRotation.x, genes._relativeRotation.z);

		if (flipped) {
			rot = btQuaternion(rot.getX(), rot.getY(), -rot.getZ(), -rot.getW()).normalized();
		}

		pMultiBody->setupRevolute(linkIndex, mass, inertia, _limbs[parentIndex]._linkIndex, rot, hingeAxis, parentPos, childPos, true);
	
		//if (genes._isMotorized) {
		_limbs[currentIndex]._jointMotor.reset(new MyMultiBodyJointMotor(pMultiBody, linkIndex, 0.0f, genes._maxMotorImpulse));

		_limbs[currentIndex]._constraint.reset(new btMultiBodyJointLimitConstraint(pMultiBody, linkIndex, -genes._maxLimbAngle, genes._maxLimbAngle));

		pWorld->addMultiBodyConstraint(_limbs[currentIndex]._jointMotor.get());

		pWorld->addMultiBodyConstraint(_limbs[currentIndex]._constraint.get());
		//}
		//else {
		//	_limbs[currentIndex]._constraint.reset(new btMultiBodyJointLimitConstraint(pMultiBody, linkIndex, -0.000001f, 0.000001f));
		//
		//	pWorld->addMultiBodyConstraint(_limbs[currentIndex]._constraint.get());
		//}

		_limbs[currentIndex]._pLinkCollider.reset(new btMultiBodyLinkCollider(pMultiBody, linkIndex));

		_limbs[currentIndex]._pLinkCollider->setCollisionShape(_limbs[currentIndex]._pCollisionShape.get());

		_limbs[currentIndex]._pLinkCollider->setCcdMotionThreshold(ccdMotionThreshold);
		_limbs[currentIndex]._pLinkCollider->setCcdSweptSphereRadius(std::min(genes._halfDims.x, std::min(genes._halfDims.y, genes._halfDims.z)) * ccdRadiusScalar);

		btQuaternion localRotation = pMultiBody->getParentToLocalRot(linkIndex);
		btVector3 localPosition = quatRotate(localRotation.inverse(), pMultiBody->getRVector(linkIndex));

		localTransform.setRotation(localRotation);
		localTransform.setOrigin(localPosition);

		btTransform trans;
		trans.mult(transform, localTransform);

		_limbs[currentIndex]._pLinkCollider->setWorldTransform(trans);

		pWorld->addCollisionObject(_limbs[currentIndex]._pLinkCollider.get(), 5, ~5);

		pMultiBody->getLink(linkIndex).m_collider = _limbs[currentIndex]._pLinkCollider.get();

		pMultiBody->setJointPos(linkIndex, 0.0f);
	}
	
	int prevLinkIndex = linkIndex;
	linkIndex++;

	btTransform trans = btTransform::getIdentity();
	trans.mult(transform, localTransform);

	// Go through children
	if (genes._symmetrical) {
		int pI = currentIndex;
		
		for (int recCount = 0; recCount < genes._recursionCount + 1; recCount++) {
			int nPI = pI;

			for (size_t i = 0; i < genes._children.size(); i++)
				nPI = genLimbs(vc, *genes._children[i], pI, pMultiBody, trans, pWorld, linkIndex, flipped);

			pI = nPI;
		}

		pI = currentIndex;

		for (int recCount = 0; recCount < genes._recursionCount + 1; recCount++) {
			int nPI = pI;

			for (size_t i = 0; i < genes._children.size(); i++)
				nPI = genLimbs(vc, *genes._children[i], pI, pMultiBody, trans, pWorld, linkIndex, !flipped);

			pI = nPI;
		}
	}
	else {
		int pI = currentIndex;

		for (int recCount = 0; recCount < genes._recursionCount + 1; recCount++) {
			int nPI = pI;

			for (size_t i = 0; i < genes._children.size(); i++)
				nPI = genLimbs(vc, *genes._children[i], pI, pMultiBody, trans, pWorld, linkIndex, flipped);

			pI = nPI;
		}
	}

	return currentIndex;
}

bool SceneObjectVirtualCreature::create(const std::string &modelFileName, d3d::SceneObjectPhysicsWorld* pPhysicsWorld, const VirtualCreatureGenes &genes, const btTransform &transform) {
	_brain = genes._brain;
	
	_physicsWorld = *pPhysicsWorld;

	float mass = genes.getMass();
	btVector3 inertia = genes.getInertia();

	_multiBody.reset(new btMultiBody(genes.getNumLinks(), mass, inertia, false, true));
	
	_multiBody->setBasePos(transform.getOrigin());
	_multiBody->setWorldToBaseRot(transform.getRotation());
	_multiBody->setBaseVel(btVector3(0.0f, 0.0f, 0.0f));

	int linkIndex = -1;

	genLimbs(genes, genes._rootLimb, -1, _multiBody.get(), transform, pPhysicsWorld->_pDynamicsWorld.get(), linkIndex, false);

	pPhysicsWorld->_pDynamicsWorld->addMultiBody(_multiBody.get(), 5, ~5);

	// Rendering
	std::shared_ptr<d3d::Asset> asset;

	if (!getScene()->getAssetManager("MOBJ", d3d::StaticModelOBJ::assetFactory)->getAsset(modelFileName, asset))
		return false;

	_pModelOBJ = static_cast<d3d::StaticModelOBJ*>(asset.get());

	updateAABB();

	_timerRates = genes._timerRates;
	_timers = genes._initTimerTimes;

	_blendVelocityScalar = genes._blendVelocityScalar;

	return true;
}

void SceneObjectVirtualCreature::update(float dt) {
	assert(_multiBody != nullptr);

	size_t inputIndex = 0;

	for (size_t i = 0; i < _timers.size(); i++)
		_brain.setInput(inputIndex++, std::sinf(_timers[i]) * 4.0f);

	for (size_t i = 1; i < _limbs.size(); i++) {
		if (_limbs[i]._genes._isMotorized) {
			_brain.setInput(inputIndex, _multiBody->getJointPos(_limbs[i]._linkIndex));

			inputIndex++;
		}
	}

	_brain.step(_reward, _brainSettings, getScene()->_randomGenerator);

	size_t outputIndex = 0;

	for (size_t i = 1; i < _limbs.size(); i++) {
		if (_limbs[i]._genes._isMotorized) {
			float desiredVelocity = (static_cast<float>(_brain.getOutput(outputIndex)) - 1.0f) * _limbs[i]._genes._rotationalVelocity;

			if (desiredVelocity > 0.0f) {
				if (_multiBody->getJointPos(_limbs[i]._linkIndex) >= _limbs[i]._genes._maxLimbAngle)
					_limbs[i]._jointMotor->setDesiredVelocity(-0.1f);
				else
					_limbs[i]._jointMotor->setDesiredVelocity(_limbs[i]._jointMotor->getDesiredVelocity() + (desiredVelocity - _limbs[i]._jointMotor->getDesiredVelocity()) * _blendVelocityScalar * dt);
			}
			else {
				if (_multiBody->getJointPos(_limbs[i]._linkIndex) <= -_limbs[i]._genes._maxLimbAngle)
					_limbs[i]._jointMotor->setDesiredVelocity(0.1f);
				else
					_limbs[i]._jointMotor->setDesiredVelocity(_limbs[i]._jointMotor->getDesiredVelocity() + (desiredVelocity - _limbs[i]._jointMotor->getDesiredVelocity()) * _blendVelocityScalar * dt);
			}

			outputIndex++;
		}
	}

	updateAABB();

	for (size_t i = 0; i < _timers.size(); i++)
		_timers[i] += _timerRates[i] * dt;

	/*
	// --------------------------- Merge Meshes -------------------------------

	_combinedMesh.clearArrays();

	size_t indexBatchSize = _pModelOBJ->_model._meshes[0]._mesh->_indices.size();

	_combinedMesh._vertices.reserve(_pModelOBJ->_model._meshes[0]._mesh->_vertices.size() * _limbs.size());
	_combinedMesh._indices.reserve(indexBatchSize * _limbs.size());

	for (size_t i = 0; i < _limbs.size(); i++) {
		d3d::Matrix4x4f transform;

		_limbs[i]._pLinkCollider->getWorldTransform().getOpenGLMatrix(&transform._elements[0]);

		d3d::Matrix3x3f upperLeftSubmatrixInverse;

		transform.getUpperLeftMatrix3x3f().inverse(upperLeftSubmatrixInverse);

		d3d::Matrix3x3f normalMatrix(upperLeftSubmatrixInverse.transpose());

		transform *= d3d::Matrix4x4f::scaleMatrix(_limbs[i]._genes._halfDims);

		for (size_t j = 0; j < _pModelOBJ->_model._meshes[0]._mesh->_vertices.size(); j++) {
			d3d::StaticMesh::Vertex v = _pModelOBJ->_model._meshes[0]._mesh->_vertices[j];

			v._position = transform * v._position;
			v._normal = normalMatrix * v._normal;

			_combinedMesh._vertices.push_back(v);
		}

		for (size_t j = 0; j < indexBatchSize; j++)
			_combinedMesh._indices.push_back(i * _pModelOBJ->_model._meshes[0]._mesh->_vertices.size() + _pModelOBJ->_model._meshes[0]._mesh->_indices[j]);
	}

	_combinedMesh._numIndices = _combinedMesh._indices.size();*/
}

void SceneObjectVirtualCreature::deferredRender() {
	/*
	getRenderScene()->setTransform(d3d::Matrix4x4f::identityMatrix());

	getRenderScene()->useShader(_pModelOBJ->_model._materials[0]);

	_combinedMesh.renderFromArrays();
	*/

	for (size_t i = 0; i < _limbs.size(); i++) {
		d3d::Matrix4x4f transform;

		_limbs[i]._pLinkCollider->getWorldTransform().getOpenGLMatrix(&transform._elements[0]);

		transform *= d3d::Matrix4x4f::scaleMatrix(_limbs[i]._genes._halfDims);

		getRenderScene()->setTransform(transform);

		_pModelOBJ->render(getRenderScene());
	}
}