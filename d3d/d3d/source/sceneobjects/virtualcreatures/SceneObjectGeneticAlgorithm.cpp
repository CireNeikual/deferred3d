#include <sceneobjects/virtualcreatures/SceneObjectGeneticAlgorithm.h>

SceneObjectGeneticAlgorithm::SceneObjectGeneticAlgorithm()
: _generationTime(10.0f), _generationTimer(0.0f), _greedExponent(2.0f), _numElites(4), _show(false), _generations(0)
{}

size_t SceneObjectGeneticAlgorithm::rouletteWheel(float fitnessSum, const std::vector<float> &fitnesses) {
	std::uniform_real_distribution<float> fitnessDist(0.0f, fitnessSum);

	float randomCusp = fitnessDist(getScene()->_randomGenerator);

	float sumSoFar = 0.0f;

	for (size_t i = 0; i < fitnesses.size(); i++) {
		sumSoFar += fitnesses[i];

		if (sumSoFar >= randomCusp)
			return i;
	}

	return 0;
}

void SceneObjectGeneticAlgorithm::create(size_t populationSize) {
	assert(getScene() != nullptr);

	_genePool.resize(populationSize);

	for (size_t i = 0; i < _genePool.size(); i++)
		_genePool[i].createRandom(getScene()->_randomGenerator, _params);

	_physicsWorld = getScene()->getNamedCheckQueue("physWrld");

	assert(_physicsWorld.isAlive());

	spawnPopulation();
}

void SceneObjectGeneticAlgorithm::spawnPopulation() {
	std::uniform_real_distribution<float> positionDist(-40.0f, 40.0f);

	for (size_t i = 0; i < _genePool.size(); i++) {
		std::shared_ptr<SceneObjectVirtualCreature> creature(new SceneObjectVirtualCreature());

		getScene()->add(creature, true);

		d3d::SceneObjectPhysicsWorld* pPhysicsWorld = static_cast<d3d::SceneObjectPhysicsWorld*>(_physicsWorld.get());

		btTransform trans = btTransform::getIdentity();

		trans.setOrigin(btVector3(positionDist(getScene()->_randomGenerator), 20.0f, positionDist(getScene()->_randomGenerator)));

		creature->create("resources/models/box.obj", pPhysicsWorld, _genePool[i], trans);

		btTransform newTrans = creature->getTransform();

		newTrans.setOrigin(newTrans.getOrigin() + btVector3(0.0f, 0.0f - creature->getAABB()._lowerBound.y, 0.0f) + btVector3(0.0f, 0.01f, 0.0f));

		creature->setTransform(newTrans);

		creature->_renderMask = 0x0001;

		_creatures.push_back(creature.get());
		_startPositions.push_back(trans.getOrigin());
	}
}

void SceneObjectGeneticAlgorithm::destroyPopulation() {
	for (size_t i = 0; i < _creatures.size(); i++)
		_creatures[i]->destroy();

	_creatures.clear();
	_startPositions.clear();
}

void SceneObjectGeneticAlgorithm::onDestroy() {
	destroyPopulation();
}

void SceneObjectGeneticAlgorithm::onAdd() {
	_input = getScene()->getNamed("buffIn");

	assert(_input.isAlive());

	_gui = getScene()->getNamed("gui");

	assert(_gui.isAlive());

	SceneObjectGUI* pGUI = static_cast<SceneObjectGUI*>(_gui.get());
}

void SceneObjectGeneticAlgorithm::update(float dt) {
	d3d::SceneObjectBufferedInput* pBufferedInput = static_cast<d3d::SceneObjectBufferedInput*>(_input.get());

	if (pBufferedInput->isKeyPressed(sf::Keyboard::T))
		_show = !_show;

	if (_show)
		getRenderScene()->_renderMask = 0xffff;
	else
		getRenderScene()->_renderMask = 0xfffe;

	_generationTimer += dt;

	if (_generationTimer >= _generationTime) {
		_generationTimer = 0.0f;

		std::vector<float> fitnesses(_creatures.size());

		float minFitness = 9999999.0f;

		for (size_t i = 0; i < fitnesses.size(); i++) {
			SceneObjectVirtualCreature* pCreature = static_cast<SceneObjectVirtualCreature*>(_creatures[i].get());

			d3d::Vec2f startXZ(_startPositions[i].getX(), _startPositions[i].getZ());
			d3d::Vec2f creatureXZ(pCreature->getTransform().getOrigin().getX(), pCreature->getTransform().getOrigin().getZ());

			fitnesses[i] = (creatureXZ - startXZ).magnitude();

			if (fitnesses[i] < minFitness)
				minFitness = fitnesses[i];
		}

		// Normalize fitnesses and greedify them
		float fitnessSum = 0.0f;

		for (size_t i = 0; i < fitnesses.size(); i++) {
			fitnesses[i] = std::powf(fitnesses[i] - minFitness, _greedExponent);

			fitnessSum += fitnesses[i];
		}

		std::vector<float> fitnessesCopy = fitnesses;
		std::vector<VirtualCreatureGenes> genesCopy = _genePool;

		std::vector<VirtualCreatureGenes> newGenePool;

		for (size_t i = 0; i < _numElites; i++) {
			size_t maxIndex = 0;

			for (size_t j = 1; j < fitnessesCopy.size(); j++) {
				if (fitnessesCopy[j] > fitnessesCopy[maxIndex])
					maxIndex = j;
			}

			newGenePool.push_back(genesCopy[maxIndex]);

			fitnessesCopy.erase(fitnessesCopy.begin() + maxIndex);
			genesCopy.erase(genesCopy.begin() + maxIndex);
		}

		while (newGenePool.size() < _genePool.size()) {
			size_t parentIndex1 = rouletteWheel(fitnessSum, fitnesses);
			size_t parentIndex2 = rouletteWheel(fitnessSum, fitnesses);

			VirtualCreatureGenes child;

			_genePool[parentIndex1].crossover(_genePool[parentIndex2], child, getScene()->_randomGenerator, _params);

			child.mutate(getScene()->_randomGenerator, _params);

			newGenePool.push_back(child);
		}

		_genePool = newGenePool;

		destroyPopulation();
		spawnPopulation();

		_generations++;
	}
}

void SceneObjectGeneticAlgorithm::synchronousUpdate(float dt)
{}