#pragma once

#include <scene/RenderScene.h>

#include <rendering/model/StaticModelOBJ.h>

class SceneObjectVirtualCreatureBatcher : public d3d::SceneObject {
private:
	std::list<d3d::SceneObjectRef> _renderCreatures;

	// Rendering
	d3d::StaticModelOBJ* _pModelOBJ;

public:
	SceneObjectVirtualCreatureBatcher() {
		_renderMask = 0xffff;
	}

	bool create(const std::string &modelFileName);

	// Inherited from SceneObject
	void deferredRender(float dt);
};