#pragma once

#include <scene/SceneObject.h>

#include <sceneobjects/virtualcreatures/SceneObjectVirtualCreature.h>

#include <sceneobjects/input/SceneObjectBufferedInput.h>

#include <sceneobjects/gui/SceneObjectGUI.h>

class SceneObjectGeneticAlgorithm : public d3d::SceneObject {
private:
	d3d::SceneObjectRef _physicsWorld;
	d3d::SceneObjectRef _gui;

	std::vector<d3d::SceneObjectRef> _creatures;
	std::vector<btVector3> _startPositions;
	std::vector<VirtualCreatureGenes> _genePool;

	d3d::SceneObjectRef _input;

	float _generationTimer;

	int _generations;

	void spawnPopulation();
	void destroyPopulation();

	size_t rouletteWheel(float fitnessSum, const std::vector<float> &fitnesses);

public:
	VirtualCreatureGenes::GeneParams _params;

	float _generationTime;

	float _greedExponent;

	size_t _numElites;

	bool _show;

	SceneObjectGeneticAlgorithm();

	void create(size_t populationSize);
	
	// Inherited from SceneObject
	void onAdd();
	void update(float dt);
	void synchronousUpdate(float dt);
	void onDestroy();

	SceneObjectGeneticAlgorithm* copyFactory() {
		return new SceneObjectGeneticAlgorithm(*this);
	}
};

