#pragma once

#include <scene/Scene.h>

#include <constructs/Vec3f.h>

#include <util/Math.h>

#include <sceneobjects/physics/SceneObjectPhysicsWorld.h>

#include <multilstmrl/MultiLSTMRL.h>

class VirtualCreatureGenes {
public:
	struct GeneParams {
		// ------------ Init -------------

		d3d::Vec3f _minInitHalfDims;
		d3d::Vec3f _maxInitHalfDims;

		d3d::Vec3f _maxInitAttachmentOnThis;
		d3d::Vec3f _maxInitAttachmentOnParent;

		d3d::Vec3f _maxInitAngles;

		float _minInitDensity;
		float _maxInitDensity;
		float _minInitFriction;
		float _maxInitFriction;
		float _minInitRestitution;
		float _maxInitRestitution;

		float _minInitMaxMotorImpulse;
		float _maxInitMaxMotorImpulse;
		float _minInitLimbAngle;
		float _maxInitLimbAngle;

		float _initBranchPower;

		float _minInitRotationalVelocity;
		float _maxInitRotationalVelocity;

		float _minInitTimerRate;
		float _maxInitTimerRate;

		// ---------- Run Time -----------

		d3d::Vec3f _minHalfDims;
		d3d::Vec3f _maxHalfDims;

		d3d::Vec3f _maxAttachmentOnThis;
		d3d::Vec3f _maxAttachmentOnParent;

		d3d::Vec3f _maxAngles;

		int _maxBranches;

		float _minDensity;
		float _maxDensity;
		float _minFriction;
		float _maxFriction;
		float _minRestitution;
		float _maxRestitution;

		float _minMaxMotorImpulse;
		float _maxMaxMotorImpulse;
		float _minLimbAngle;
		float _maxLimbAngle;

		float _initMotorizedChance;
		float _initSymmetricalChance;
		float _initRecursiveChance;

		float _symmetricalDecreaseChance;

		float _minRotationalVelocity;
		float _maxRotationalVelocity;

		float _minTimerRate;
		float _maxTimerRate;

		// ----------- Common ------------

		float _branchCountDecreaseBase;
		float _noBranchChance;

		int _numTimers;

		// ---------- Mutation -----------

		float _halfDimMutationRate;
		float _halfDimMutationPerturbation;

		float _attachmentOnThisMutationRate;
		float _attachmentOnThisMutationPerturbation;

		float _attachmentOnParentMutationRate;
		float _attachmentOnParentMutationPerturbation;

		float _rotationMutationRate;
		float _rotationMutationPerturbation;

		float _isMotorizedMutationRate;

		float _branchesMutationRate;

		float _symmetricalMutationRate;
		float _recursiveMutationRate;

		float _densityMutationRate;
		float _densityMutationPerturbation;
		
		float _frictionMutationRate;
		float _frictionMutationPerturbation;

		float _restitutionMutationRate;
		float _restitutionMutationPerturbation;

		float _maxMotorImpulseMutationRate;
		float _maxMotorImpulseMutationPerturbation;

		float _limbAngleMutationRate;
		float _limbAngleMutationPerturbation;

		float _hingeAxisMutationRate;

		float _rotationalVelocityMutationRate;
		float _rotationalVelocityMutationPerturbation;

		float _timerInitTimeMutationRate;
		float _timerInitTimeMutationPerturbation;
		
		float _timerRateMutationRate;
		float _timerRateMutationPerturbation;

		// --------- Crossover ----------

		float _averageChance;

		GeneParams();
	};

	struct LimbGenes {
		d3d::Vec3f _halfDims;

		d3d::Vec3f _attachmentOnThis;
		d3d::Vec3f _attachmentOnParent;

		d3d::Vec3f _relativeRotation;

		float _maxMotorImpulse;
		float _maxLimbAngle;

		float _density;
		float _friction;
		float _restitution;

		float _rotationalVelocity;

		bool _symmetrical;
		int _recursionCount;

		bool _isMotorized;

		int _hingeAxis;

		std::vector<std::shared_ptr<LimbGenes>> _children;
	};
private:
	float rescale(float minimum, float maximum, float uniform) {
		return minimum + (maximum - minimum) * uniform;
	}

	void genLimbs(std::mt19937 &generator, LimbGenes &limbGenes, const GeneParams &params, int level);
	void crossLimbs(std::mt19937 &generator, const LimbGenes &parent1, const LimbGenes &parent2, LimbGenes &limbGenes, const GeneParams &params, int level);
	void mutateLimbs(std::mt19937 &generator, LimbGenes &limbGenes, const GeneParams &params, int level);

	int getNumInputsLimb(const LimbGenes &genes) const;
	int getNumOutputsLimb(const LimbGenes &genes) const;
	int getNumLinksLimb(const LimbGenes &genes) const;
	float getMassLimb(const LimbGenes &genes) const;
	btVector3 getInertiaLimb(const LimbGenes &genes) const;

public:
	LimbGenes _rootLimb;

	d3d::Vec3f _jointBias;
	float _blendVelocityScalar;

	rl::MultiLSTMRL _brain;

	std::vector<float> _initTimerTimes;
	std::vector<float> _timerRates;

	VirtualCreatureGenes();

	void createRandom(std::mt19937 &generator, const GeneParams &params);

	void crossover(const VirtualCreatureGenes &other, VirtualCreatureGenes &child, std::mt19937 &generator, const GeneParams &params);
	void mutate(std::mt19937 &generator, const GeneParams &params);

	int getNumInputs() const;
	int getNumOutputs() const;
	int getNumLinks() const;
	float getMass() const;
	btVector3 getInertia() const;
};