#include <sceneobjects/virtualcreatures/VirtualCreatureGenes.h>

VirtualCreatureGenes::GeneParams::GeneParams()
: _minInitHalfDims(0.1f, 0.05f, 0.05f),
_maxInitHalfDims(0.2f, 0.1f, 0.1f),
_maxInitAttachmentOnThis(1.0f, 1.0f, 1.0f),
_maxInitAttachmentOnParent(1.0f, 1.0f, 1.0f),
_maxInitAngles(d3d::_pi * 0.2f, d3d::_pi * 0.2f, d3d::_pi * 0.2f),
_minInitDensity(800.0f),
_maxInitDensity(1400.0f),
_minInitFriction(1.0f),
_maxInitFriction(2.0f),
_minInitRestitution(0.0f),
_maxInitRestitution(0.1f),
_minInitMaxMotorImpulse(600.0f),
_maxInitMaxMotorImpulse(1000.0f),
_minInitLimbAngle(d3d::_pi * 0.2f),
_maxInitLimbAngle(d3d::_pi * 0.5f),
_initBranchPower(16.0f),
_minInitRotationalVelocity(8.0f),
_maxInitRotationalVelocity(20.0f),
_minInitTimerRate(0.4f),
_maxInitTimerRate(1.0f),
_minHalfDims(0.06f, 0.03f, 0.03f),
_maxHalfDims(0.3f, 0.3f, 0.3f),
_maxAttachmentOnThis(1.0f, 1.0f, 1.0f),
_maxAttachmentOnParent(1.0f, 1.0f, 1.0f),
_maxAngles(d3d::_piOver2, d3d::_piOver2, d3d::_piOver2),
_maxBranches(4),
_minDensity(400.0f),
_maxDensity(2000.0f),
_minFriction(0.5f),
_maxFriction(6.0f),
_minRestitution(0.0f),
_maxRestitution(0.5f),
_minMaxMotorImpulse(400.0f),
_maxMaxMotorImpulse(3000.0f),
_minLimbAngle(d3d::_pi * 0.05f),
_maxLimbAngle(d3d::_pi * 0.95f),
_minRotationalVelocity(5.0f),
_maxRotationalVelocity(30.0f),
_minTimerRate(0.1f),
_maxTimerRate(4.0f),
_initMotorizedChance(0.6f),
_initSymmetricalChance(0.3f),
_initRecursiveChance(0.1f),
_symmetricalDecreaseChance(0.2f),
_branchCountDecreaseBase(0.01f),
_noBranchChance(0.0f),
_numTimers(4),
_halfDimMutationRate(0.05f),
_halfDimMutationPerturbation(0.07f),
_attachmentOnThisMutationRate(0.05f),
_attachmentOnThisMutationPerturbation(0.3f),
_attachmentOnParentMutationRate(0.05f),
_attachmentOnParentMutationPerturbation(0.3f),
_rotationMutationRate(0.05f),
_rotationMutationPerturbation(d3d::_pi * 0.1f),
_isMotorizedMutationRate(0.05f),
_branchesMutationRate(0.05f),
_symmetricalMutationRate(0.02f),
_recursiveMutationRate(0.02f),
_densityMutationRate(0.05f),
_densityMutationPerturbation(100.0f),
_frictionMutationRate(0.05f),
_frictionMutationPerturbation(0.1f),
_hingeAxisMutationRate(0.05f),
_restitutionMutationRate(0.05f),
_restitutionMutationPerturbation(0.1f),
_maxMotorImpulseMutationRate(0.05f),
_maxMotorImpulseMutationPerturbation(500.0f),
_limbAngleMutationRate(0.05f),
_limbAngleMutationPerturbation(d3d::_pi * 0.2f),
_rotationalVelocityMutationRate(0.05f),
_rotationalVelocityMutationPerturbation(0.1f),
_timerInitTimeMutationRate(0.05f),
_timerInitTimeMutationPerturbation(0.4f),
_timerRateMutationRate(0.05f),
_timerRateMutationPerturbation(0.3f),
_averageChance(0.3f)
{}

VirtualCreatureGenes::VirtualCreatureGenes()
: _jointBias(1.0f, 0.0f, 0.0f), _blendVelocityScalar(1.0f)
{}

void VirtualCreatureGenes::genLimbs(std::mt19937 &generator, LimbGenes &limbGenes, const GeneParams &params, int level) {
	std::uniform_real_distribution<float> uniformDist(0.0f, 1.0f);

	limbGenes._halfDims = d3d::Vec3f(rescale(params._minInitHalfDims.x, params._maxInitHalfDims.x, uniformDist(generator)),
		rescale(params._minInitHalfDims.y, params._maxInitHalfDims.y, uniformDist(generator)),
		rescale(params._minInitHalfDims.z, params._maxInitHalfDims.z, uniformDist(generator)));

	limbGenes._attachmentOnThis = d3d::Vec3f(rescale(-params._maxInitAttachmentOnThis.x, params._maxInitAttachmentOnThis.x, uniformDist(generator)),
		rescale(-params._maxInitAttachmentOnThis.y, params._maxInitAttachmentOnThis.y, uniformDist(generator)),
		rescale(-params._maxInitAttachmentOnThis.z, params._maxInitAttachmentOnThis.z, uniformDist(generator)));

	limbGenes._attachmentOnParent = d3d::Vec3f(rescale(-params._maxInitAttachmentOnParent.x, params._maxInitAttachmentOnParent.x, uniformDist(generator)),
		rescale(-params._maxInitAttachmentOnParent.y, params._maxInitAttachmentOnParent.y, uniformDist(generator)),
		rescale(-params._maxInitAttachmentOnParent.z, params._maxInitAttachmentOnParent.z, uniformDist(generator)));

	limbGenes._relativeRotation = d3d::Vec3f(rescale(-params._maxInitAngles.x, params._maxInitAngles.x, uniformDist(generator)),
		rescale(-params._maxInitAngles.y, params._maxInitAngles.y, uniformDist(generator)),
		rescale(-params._maxInitAngles.z, params._maxInitAngles.z, uniformDist(generator)));

	limbGenes._density = rescale(params._minInitDensity, params._maxInitDensity, uniformDist(generator));
	limbGenes._friction = rescale(params._minInitFriction, params._maxInitFriction, uniformDist(generator));
	limbGenes._restitution = rescale(params._minInitRestitution, params._maxInitRestitution, uniformDist(generator));

	limbGenes._rotationalVelocity = rescale(params._minInitRotationalVelocity, params._maxInitRotationalVelocity, uniformDist(generator));

	limbGenes._maxMotorImpulse = rescale(params._minInitMaxMotorImpulse, params._maxInitMaxMotorImpulse, uniformDist(generator));
	limbGenes._maxLimbAngle = rescale(params._minInitLimbAngle, params._maxInitLimbAngle, uniformDist(generator));

	limbGenes._isMotorized = uniformDist(generator) < params._initMotorizedChance;
	limbGenes._symmetrical = uniformDist(generator) < params._initSymmetricalChance * std::powf(params._symmetricalDecreaseChance, level);
	limbGenes._recursionCount = uniformDist(generator) < params._initRecursiveChance ? 0 : 1;

	std::uniform_int_distribution<int> distAxis(0, 2);

	limbGenes._hingeAxis = distAxis(generator);

	int numBranches = static_cast<int>(params._maxBranches * std::powf(uniformDist(generator), params._initBranchPower) * std::powf(params._branchCountDecreaseBase, static_cast<float>(level)));

	if (level == 0)
		numBranches = std::max(1, numBranches);

	limbGenes._children.resize(numBranches);

	for (int i = 0; i < numBranches; i++) {
		limbGenes._children[i].reset(new LimbGenes());
		genLimbs(generator, *limbGenes._children[i], params, level + 1);
	}
}

void VirtualCreatureGenes::crossLimbs(std::mt19937 &generator, const LimbGenes &parent1, const LimbGenes &parent2, LimbGenes &limbGenes, const GeneParams &params, int level) {
	std::uniform_real_distribution<float> uniformDist(0.0f, 1.0f);

	limbGenes._halfDims = uniformDist(generator) < params._averageChance ? (parent1._halfDims + parent2._halfDims) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._halfDims : parent2._halfDims);
	limbGenes._attachmentOnThis = uniformDist(generator) < params._averageChance ? (parent1._attachmentOnThis + parent2._attachmentOnThis) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._attachmentOnThis : parent2._attachmentOnThis);
	limbGenes._attachmentOnParent = uniformDist(generator) < params._averageChance ? (parent1._attachmentOnParent + parent2._attachmentOnParent) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._attachmentOnParent : parent2._attachmentOnParent);
	limbGenes._relativeRotation = uniformDist(generator) < params._averageChance ? (parent1._relativeRotation + parent2._relativeRotation) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._relativeRotation : parent2._relativeRotation);

	limbGenes._density = uniformDist(generator) < params._averageChance ? (parent1._density + parent2._density) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._density : parent2._density);
	limbGenes._friction = uniformDist(generator) < params._averageChance ? (parent1._friction + parent2._friction) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._friction : parent2._friction);
	limbGenes._restitution = uniformDist(generator) < params._averageChance ? (parent1._restitution + parent2._restitution) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._restitution : parent2._restitution);

	limbGenes._rotationalVelocity = uniformDist(generator) < params._averageChance ? (parent1._rotationalVelocity + parent2._rotationalVelocity) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._rotationalVelocity : parent2._rotationalVelocity);

	limbGenes._maxMotorImpulse = uniformDist(generator) < params._averageChance ? (parent1._maxMotorImpulse + parent2._maxMotorImpulse) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._maxMotorImpulse : parent2._maxMotorImpulse);
	limbGenes._maxLimbAngle = uniformDist(generator) < params._averageChance ? (parent1._maxLimbAngle + parent2._maxLimbAngle) * 0.5f : (uniformDist(generator) < 0.5f ? parent1._maxLimbAngle : parent2._maxLimbAngle);

	limbGenes._isMotorized = uniformDist(generator) < 0.5f ? parent1._isMotorized : parent2._isMotorized;
	limbGenes._symmetrical = uniformDist(generator) < 0.5f ? parent1._symmetrical : parent2._symmetrical;
	limbGenes._recursionCount = uniformDist(generator) < 0.5f ? parent1._recursionCount : parent2._recursionCount;

	limbGenes._hingeAxis = uniformDist(generator) < 0.5f ? parent1._hingeAxis : parent2._hingeAxis;

	// Cross limbs
	if (parent1._children.size() < parent2._children.size()) {
		std::uniform_int_distribution<int> limbDist(parent1._children.size(), parent2._children.size());

		int numLimbs = std::min(limbDist(generator), static_cast<int>(params._maxBranches * std::powf(params._branchCountDecreaseBase, static_cast<float>(level))));

		limbGenes._children.resize(numLimbs);

		for (int i = 0; i < numLimbs; i++) {
			limbGenes._children[i].reset(new LimbGenes());

			if (i < parent1._children.size())
				crossLimbs(generator, *parent1._children[i], *parent2._children[i], *limbGenes._children[i], params, level + 1);
			else
				(*limbGenes._children[i]) = *parent2._children[i];
		}
	}
	else {
		std::uniform_int_distribution<int> limbDist(parent2._children.size(), parent1._children.size());

		int numLimbs = std::min(limbDist(generator), static_cast<int>(params._maxBranches * std::powf(params._branchCountDecreaseBase, static_cast<float>(level))));

		limbGenes._children.resize(numLimbs);

		for (int i = 0; i < numLimbs; i++) {
			limbGenes._children[i].reset(new LimbGenes());

			if (i < parent2._children.size())
				crossLimbs(generator, *parent1._children[i], *parent2._children[i], *limbGenes._children[i], params, level + 1);
			else
				(*limbGenes._children[i]) = *parent1._children[i];
		}
	}
}

void VirtualCreatureGenes::mutateLimbs(std::mt19937 &generator, LimbGenes &limbGenes, const GeneParams &params, int level) {
	std::uniform_real_distribution<float> uniformDist(0.0f, 1.0f);

	if (uniformDist(generator) < params._halfDimMutationRate) {
		limbGenes._halfDims += d3d::Vec3f(rescale(-params._halfDimMutationPerturbation, params._halfDimMutationPerturbation, uniformDist(generator)),
			rescale(-params._halfDimMutationPerturbation, params._halfDimMutationPerturbation, uniformDist(generator)),
			rescale(-params._halfDimMutationPerturbation, params._halfDimMutationPerturbation, uniformDist(generator)));

		limbGenes._halfDims = d3d::Vec3f(d3d::clamp(limbGenes._halfDims.x, params._minHalfDims.x, params._maxHalfDims.x),
			d3d::clamp(limbGenes._halfDims.y, params._minHalfDims.y, params._maxHalfDims.y),
			d3d::clamp(limbGenes._halfDims.z, params._minHalfDims.z, params._maxHalfDims.z));
	}

	if (uniformDist(generator) < params._attachmentOnThisMutationRate) {
		limbGenes._attachmentOnThis += d3d::Vec3f(rescale(-params._attachmentOnThisMutationPerturbation, params._attachmentOnThisMutationPerturbation, uniformDist(generator)),
			rescale(-params._attachmentOnThisMutationPerturbation, params._attachmentOnThisMutationPerturbation, uniformDist(generator)),
			rescale(-params._attachmentOnThisMutationPerturbation, params._attachmentOnThisMutationPerturbation, uniformDist(generator)));

		limbGenes._attachmentOnThis = d3d::Vec3f(d3d::clamp(limbGenes._attachmentOnThis.x, -params._maxAttachmentOnThis.x, params._maxAttachmentOnThis.x),
			d3d::clamp(limbGenes._attachmentOnThis.y, -params._maxAttachmentOnThis.y, params._maxAttachmentOnThis.y),
			d3d::clamp(limbGenes._attachmentOnThis.z, -params._maxAttachmentOnThis.z, params._maxAttachmentOnThis.z));
	}

	if (uniformDist(generator) < params._attachmentOnParentMutationRate) {
		limbGenes._attachmentOnParent += d3d::Vec3f(rescale(-params._attachmentOnParentMutationPerturbation, params._attachmentOnParentMutationPerturbation, uniformDist(generator)),
			rescale(-params._attachmentOnParentMutationPerturbation, params._attachmentOnParentMutationPerturbation, uniformDist(generator)),
			rescale(-params._attachmentOnParentMutationPerturbation, params._attachmentOnParentMutationPerturbation, uniformDist(generator)));

		limbGenes._attachmentOnParent = d3d::Vec3f(d3d::clamp(limbGenes._attachmentOnParent.x, -params._maxAttachmentOnParent.x, params._maxAttachmentOnParent.x),
			d3d::clamp(limbGenes._attachmentOnParent.y, -params._maxAttachmentOnParent.y, params._maxAttachmentOnParent.y),
			d3d::clamp(limbGenes._attachmentOnParent.z, -params._maxAttachmentOnParent.z, params._maxAttachmentOnParent.z));
	}

	if (uniformDist(generator) < params._rotationMutationRate) {
		limbGenes._relativeRotation += d3d::Vec3f(rescale(-params._rotationMutationPerturbation, params._rotationMutationPerturbation, uniformDist(generator)),
			rescale(-params._rotationMutationPerturbation, params._rotationMutationPerturbation, uniformDist(generator)),
			rescale(-params._rotationMutationPerturbation, params._rotationMutationPerturbation, uniformDist(generator)));

		limbGenes._relativeRotation = d3d::Vec3f(d3d::clamp(limbGenes._relativeRotation.x, -params._maxAngles.x, params._maxAngles.x),
			d3d::clamp(limbGenes._relativeRotation.y, -params._maxAngles.y, params._maxAngles.y),
			d3d::clamp(limbGenes._relativeRotation.z, -params._maxAngles.z, params._maxAngles.z));
	}

	if (uniformDist(generator) < params._densityMutationRate) {
		limbGenes._density += rescale(-params._densityMutationPerturbation, params._densityMutationPerturbation, uniformDist(generator));

		limbGenes._density = d3d::clamp(limbGenes._density, params._minDensity, params._maxDensity);
	}

	if (uniformDist(generator) < params._frictionMutationRate) {
		limbGenes._friction += rescale(-params._frictionMutationPerturbation, params._frictionMutationPerturbation, uniformDist(generator));

		limbGenes._friction = d3d::clamp(limbGenes._friction, params._minFriction, params._maxFriction);
	}

	if (uniformDist(generator) < params._restitutionMutationRate) {
		limbGenes._restitution += rescale(-params._restitutionMutationPerturbation, params._restitutionMutationPerturbation, uniformDist(generator));

		limbGenes._restitution = d3d::clamp(limbGenes._restitution, params._minRestitution, params._maxRestitution);
	}

	if (uniformDist(generator) < params._rotationalVelocityMutationRate) {
		limbGenes._rotationalVelocity += rescale(-params._rotationalVelocityMutationPerturbation, params._rotationalVelocityMutationPerturbation, uniformDist(generator));

		limbGenes._rotationalVelocity = d3d::clamp(limbGenes._rotationalVelocity, params._minRotationalVelocity, params._maxRotationalVelocity);
	}

	if (uniformDist(generator) < params._maxMotorImpulseMutationRate) {
		limbGenes._maxMotorImpulse += rescale(-params._maxMotorImpulseMutationPerturbation, params._maxMotorImpulseMutationPerturbation, uniformDist(generator));

		limbGenes._maxMotorImpulse = d3d::clamp(limbGenes._maxMotorImpulse, params._minMaxMotorImpulse, params._maxMaxMotorImpulse);
	}

	if (uniformDist(generator) < params._limbAngleMutationRate) {
		limbGenes._maxLimbAngle += rescale(-params._limbAngleMutationPerturbation, params._limbAngleMutationPerturbation, uniformDist(generator));

		limbGenes._maxLimbAngle = d3d::clamp(limbGenes._maxLimbAngle, params._minLimbAngle, params._maxLimbAngle);
	}

	if (uniformDist(generator) < params._isMotorizedMutationRate)
		limbGenes._isMotorized = !limbGenes._isMotorized;

	if (uniformDist(generator) < params._symmetricalMutationRate)
		limbGenes._symmetrical = !limbGenes._symmetrical;

	if (uniformDist(generator) < params._recursiveMutationRate) {
		if (uniformDist(generator) < 0.5f) {
			limbGenes._recursionCount--;

			if (limbGenes._recursionCount < 0)
				limbGenes._recursionCount = 0;
		}
		else
			limbGenes._recursionCount++;
	}

	if (uniformDist(generator) < params._hingeAxisMutationRate) {
		std::uniform_int_distribution<int> distAxis(0, 2);

		limbGenes._hingeAxis = distAxis(generator);
	}

	if (uniformDist(generator) < params._branchesMutationRate) {
		// Add a limb
		if (uniformDist(generator) < 0.5f) {
			if (limbGenes._children.size() < static_cast<int>(params._maxBranches * std::powf(params._branchCountDecreaseBase, static_cast<float>(level)))) {
				for (size_t i = 0; i < limbGenes._children.size(); i++)
					mutateLimbs(generator, *limbGenes._children[i], params, level + 1);

				limbGenes._children.push_back(std::unique_ptr<LimbGenes>(new LimbGenes()));

				genLimbs(generator, *limbGenes._children.back(), params, level + 1);
			}
		}
		else if (!limbGenes._children.empty()) { // Remove a limb
			limbGenes._children.pop_back();

			for (size_t i = 0; i < limbGenes._children.size(); i++)
				mutateLimbs(generator, *limbGenes._children[i], params, level + 1);
		}
	}
	else // No count mutation, continue mutating branches
		for (size_t i = 0; i < limbGenes._children.size(); i++)
			mutateLimbs(generator, *limbGenes._children[i], params, level + 1);
}

void VirtualCreatureGenes::createRandom(std::mt19937 &generator, const GeneParams &params) {
	genLimbs(generator, _rootLimb, params, 0);

	// Create timers
	_initTimerTimes.resize(params._numTimers);
	_timerRates.resize(params._numTimers);

	std::uniform_real_distribution<float> initTimeDist(0.0f, d3d::_piTimes2);
	std::uniform_real_distribution<float> timerRateDist(params._minInitTimerRate, params._maxInitTimerRate);

	for (size_t i = 0; i < _initTimerTimes.size(); i++) {
		_initTimerTimes[i] = initTimeDist(generator);

		_timerRates[i] = timerRateDist(generator);
	}

	int numInputs = getNumInputs();
	int numOutputs = getNumOutputs();

	_brain.createRandom(numInputs, numOutputs, 3, 4, 3, -0.7f, 0.7f, generator);
}

void VirtualCreatureGenes::crossover(const VirtualCreatureGenes &other, VirtualCreatureGenes &child, std::mt19937 &generator, const GeneParams &params) {
	crossLimbs(generator, _rootLimb, other._rootLimb, child._rootLimb, params, 0);

	// Cross timers
	std::uniform_real_distribution<float> uniformDist(0.0f, 1.0f);

	child._initTimerTimes.resize(_initTimerTimes.size());
	child._timerRates.resize(_timerRates.size());

	for (size_t i = 0; i < _initTimerTimes.size(); i++) {
		child._initTimerTimes[i] = uniformDist(generator) < params._averageChance ? (_initTimerTimes[i] + other._initTimerTimes[i]) * 0.5f : (uniformDist(generator) < 0.5f ? _initTimerTimes[i] : other._initTimerTimes[i]);

		child._timerRates[i] = uniformDist(generator) < params._averageChance ? (_timerRates[i] + other._timerRates[i]) * 0.5f : (uniformDist(generator) < 0.5f ? _timerRates[i] : other._timerRates[i]);
	}

	int numInputs = child.getNumInputs();
	int numOutputs = child.getNumOutputs();

	child._brain.crossover(_brain, other._brain, 0.3f, numInputs, numOutputs, 3, 4, 3, -0.7f, 0.7f, generator);
}

void VirtualCreatureGenes::mutate(std::mt19937 &generator, const GeneParams &params) {
	mutateLimbs(generator, _rootLimb, params, 0);

	// Mutate timers
	std::uniform_real_distribution<float> uniformDist(0.0f, 1.0f);
	std::uniform_real_distribution<float> timerInitTimerPerturbationDist(-params._timerInitTimeMutationPerturbation, params._timerInitTimeMutationPerturbation);
	std::uniform_real_distribution<float> timerRatePerturbationDist(-params._timerRateMutationPerturbation, params._timerRateMutationPerturbation);

	for (size_t i = 0; i < _initTimerTimes.size(); i++) {
		if (uniformDist(generator) < params._timerInitTimeMutationRate)
			_initTimerTimes[i] = d3d::clamp(_initTimerTimes[i] + timerInitTimerPerturbationDist(generator), 0.0f, d3d::_piTimes2);

		if (uniformDist(generator) < params._timerRateMutationRate)
			_timerRates[i] = d3d::clamp(_timerRates[i] + timerRatePerturbationDist(generator), params._minTimerRate, params._maxTimerRate);
	}

	// Cross with self to get right number of inputs/outputs again (TODO: MAKE A FUNCTION TO CHANGE NUMBER OF INPUTS/OUTPUTS INSTEAD OF USING CROSSOVER (INEFFICIENT))
	_brain.crossover(_brain, _brain, 0.5f, getNumInputs(), getNumOutputs(), 3, 4, 3, -0.7f, 0.7f, generator);
	_brain.mutate(0.07f, 0.5f, generator);
}

int VirtualCreatureGenes::getNumInputsLimb(const LimbGenes &genes) const {
	int numInputs = 0;

	for (size_t i = 0; i < genes._children.size(); i++)
		numInputs += getNumInputsLimb(*genes._children[i]);

	if (genes._symmetrical)
		numInputs *= 2;

	numInputs *= (1 + genes._recursionCount);

	numInputs += genes._isMotorized ? 1 : 0;

	return numInputs;
}

int VirtualCreatureGenes::getNumOutputsLimb(const LimbGenes &genes) const {
	int numOutputs = 0;

	for (size_t i = 0; i < genes._children.size(); i++)
		numOutputs += getNumOutputsLimb(*genes._children[i]);

	if (genes._symmetrical)
		numOutputs *= 2;

	numOutputs *= (1 + genes._recursionCount);

	numOutputs += genes._isMotorized ? 1 : 0;

	return numOutputs;
}

int VirtualCreatureGenes::getNumLinksLimb(const LimbGenes &genes) const {
	int numLinks = genes._children.size();

	for (size_t i = 0; i < genes._children.size(); i++)
		numLinks += getNumLinksLimb(*genes._children[i]);

	if (genes._symmetrical)
		numLinks *= 2;

	numLinks *= (1 + genes._recursionCount);

	return numLinks;
}

float VirtualCreatureGenes::getMassLimb(const LimbGenes &genes) const {
	float mass = genes._density * 8.0f * genes._halfDims.magnitude();

	for (size_t i = 0; i < genes._children.size(); i++)
		mass += getMassLimb(*genes._children[i]);

	return mass;
}

btVector3 VirtualCreatureGenes::getInertiaLimb(const LimbGenes &genes) const {
	float mass = genes._density * 8.0f * genes._halfDims.magnitude();

	std::shared_ptr<btBoxShape> box(new btBoxShape(bt(genes._halfDims)));

	btVector3 inertia;

	box->calculateLocalInertia(mass, inertia);

	for (size_t i = 0; i < genes._children.size(); i++)
		inertia += getInertiaLimb(*genes._children[i]);

	return inertia;
}

int VirtualCreatureGenes::getNumInputs() const {
	return getNumInputsLimb(_rootLimb) + _initTimerTimes.size();
}

int VirtualCreatureGenes::getNumOutputs() const {
	return getNumOutputsLimb(_rootLimb);
}

int VirtualCreatureGenes::getNumLinks() const {
	return getNumLinksLimb(_rootLimb);
}

float VirtualCreatureGenes::getMass() const {
	return getMassLimb(_rootLimb);
}

btVector3 VirtualCreatureGenes::getInertia() const {
	return getInertiaLimb(_rootLimb);
}