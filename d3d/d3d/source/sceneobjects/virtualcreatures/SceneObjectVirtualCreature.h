#pragma once

#include <scene/Scene.h>

#include <sceneobjects/physics/SceneObjectPhysicsWorld.h>
#include <sceneobjects/virtualcreatures/VirtualCreatureGenes.h>

#include <rendering/model/StaticModelOBJ.h>

#include <BulletDynamics/Featherstone/btMultiBody.h>
#include <BulletDynamics/Featherstone/btMultiBodyJointMotor.h>
#include <BulletDynamics/Featherstone/btMultiBodyJointLimitConstraint.h>
#include <BulletDynamics/Featherstone/btMultiBodyLinkCollider.h>

class SceneObjectVirtualCreature : public d3d::SceneObject {
public:
	class MyMultiBodyJointMotor : public btMultiBodyJointMotor {
	public:
		MyMultiBodyJointMotor(btMultiBody* pMultiBody, int linkIndex, float velocity, float maxImpulse)
			: btMultiBodyJointMotor(pMultiBody, linkIndex, velocity, maxImpulse)
		{}

		void setDesiredVelocity(float velocity) {
			m_desiredVelocity = velocity;
		}

		float getDesiredVelocity() const {
			return m_desiredVelocity;
		}
	};
private:
	struct Limb {
		std::shared_ptr<btCollisionShape> _pCollisionShape;
		std::shared_ptr<btMultiBodyLinkCollider> _pLinkCollider;
		std::shared_ptr<MyMultiBodyJointMotor> _jointMotor;
		std::shared_ptr<btMultiBodyConstraint> _constraint;
		//std::shared_ptr<btRigidBody> _rigidBody;

		int _linkIndex;

		VirtualCreatureGenes::LimbGenes _genes;
	};

	d3d::SceneObjectRef _physicsWorld;

	std::vector<Limb> _limbs;

	std::shared_ptr<btMultiBody> _multiBody;

	rl::MultiLSTMRL _brain;

	std::vector<float> _timerRates;
	std::vector<float> _timers;

	// Rendering
	d3d::StaticModelOBJ* _pModelOBJ;

	d3d::StaticMesh _combinedMesh;

	int genLimbs(const VirtualCreatureGenes &vc, const VirtualCreatureGenes::LimbGenes &genes, int parentIndex, btMultiBody* pMultiBody, const btTransform &transform, btMultiBodyDynamicsWorld* pWorld, int &linkIndex, bool flipped);

public:
	float _reward;

	float _blendVelocityScalar;

	rl::MultiLSTMRL::MultiLSTMRLSettings _brainSettings;

	SceneObjectVirtualCreature()
		: _reward(0.0f)
	{}

	bool create(const std::string &modelFileName, d3d::SceneObjectPhysicsWorld* pPhysicsWorld, const VirtualCreatureGenes &genes, const btTransform &transform);

	btTransform getTransform() const;
	void setTransform(const btTransform &transform);

	void updateAABB();

	// Inherited from SceneObject
	void update(float dt);
	void onDestroy();
	void deferredRender();

	SceneObjectVirtualCreature* copyFactory() {
		return new SceneObjectVirtualCreature(*this);
	}
};