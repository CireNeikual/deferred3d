#pragma once

#include <scene/RenderScene.h>
#include <assetmanager/Asset.h>
#include <rendering/texture/TextureCube.h>
#include <sceneobjects/physics/SceneObjectPhysicsWorld.h>

class Trile : public d3d::Asset {
public:
	struct Quad {
		std::array<d3d::Vec3f, 4> positions;
	};

	struct Vertex {
		d3d::Vec3f _position;
		d3d::Vec3f _normal;
	};

	static const size_t _trileVoxelSize = 16;
	static const size_t _trileVoxelSizePlusOne = _trileVoxelSize + 1;
	static const float _trileSize; // 1 / 16

private:
	GLuint _cubeMapID;

	d3d::VBO _vertexBuffer;
	d3d::VBO _indexBuffer;

	unsigned short _numIndices;

	d3d::AABB3D _minBounds;

public:
	Trile()
		: _cubeMapID(0)
	{}

	~Trile();

	bool createAsset(const std::string &name);

	// Asset factory
	static Asset* assetFactory() {
		return new Trile();
	}

	static d3d::Point3i get3DChunkCoord(int linearCoord) {
		return d3d::Point3i(linearCoord % _trileVoxelSizePlusOne,
			(linearCoord / _trileVoxelSizePlusOne) % _trileVoxelSizePlusOne,
			(linearCoord / (_trileVoxelSizePlusOne * _trileVoxelSizePlusOne)) % _trileVoxelSizePlusOne);
	}

	const d3d::AABB3D &getMinBounds() const {
		return _minBounds;
	}

	friend class TrileMap;
	friend class TrileChunk;
	friend class TrileChunkBatcher;
	friend class TrileBatcher;
};

class TrileVoxelData : public d3d::Asset {
public:
	struct Voxel {
		unsigned char _id;
		unsigned char _r, _g, _b;
	};

	std::vector<Voxel> _colors;

	bool createAsset(const std::string &name);

	// Asset factory
	static Asset* assetFactory() {
		return new TrileVoxelData();
	}
};

class TrileMap : public d3d::Asset {
public:
	struct TrileData {
		unsigned char _id;
		unsigned char _rotation;
	};

	struct TrileBounds {
		d3d::Point3i _min, _max;
	};

private:
	d3d::Point3i _size;
	std::vector<TrileData> _map;
	std::vector<TrileBounds> _physicsBounds;

	TrileData &mapAt(const d3d::Point3i &point) {
		return _map[point.x + point.y * _size.x + point.z * _size.x * _size.y];
	}

public:
	bool createAsset(const std::string &name);

	static void generateChunks(const std::shared_ptr<TrileMap> &map, d3d::RenderScene* pRenderScene, int chunkSize, const std::shared_ptr<d3d::Shader> &trileRenderShader, const std::shared_ptr<d3d::Shader> &depthRenderShader, const std::shared_ptr<d3d::TextureCube> &trileNormalMap, const std::vector<std::shared_ptr<Trile>> &trileSet);

	const d3d::Point3i &getSize() const {
		return _size;
	}

	// Asset factory
	static Asset* assetFactory() {
		return new TrileMap();
	}

	friend class TrileChunk;
	friend class TrileChunkBatcher;
	friend class TrileMapPhysics;
};

class TrileChunk : public d3d::SceneObject {
private:
	d3d::Point3i _min, _max;

	std::shared_ptr<TrileMap> _trileMap;

	d3d::SceneObjectRef _batcher;

public:
	TrileChunk() {
		_renderMask = 0xffff;
	}

	void create(const d3d::Point3i &min, const d3d::Point3i &max, const std::shared_ptr<TrileMap> &trileMap, class TrileChunkBatcher* pBatcher);

	// Inherited from SceneObject
	void deferredRender();

	TrileChunk* copyFactory() {
		return new TrileChunk(*this);
	}

	friend class TrileChunkBatcher;
};

class TrileChunkBatcher : public d3d::SceneObject {
private:
	std::list<d3d::SceneObjectRef> _chunks;

	std::shared_ptr<d3d::Shader> _trileRenderShader;
	std::shared_ptr<d3d::Shader> _depthRenderShader;

	std::shared_ptr<d3d::TextureCube> _trileNormalMap;
	
	struct TrileRenderData {
		unsigned char _rot;
		d3d::Point3i _position;
	};

	std::shared_ptr<TrileMap> _trileMap;
	std::vector<std::shared_ptr<Trile>> _trileSet;

public:
	TrileChunkBatcher() {
		_renderMask = 0xffff;
	}

	void create(const std::shared_ptr<d3d::Shader> &trileRenderShader, const std::shared_ptr<d3d::Shader> &depthRenderShader, const std::shared_ptr<d3d::TextureCube> &trileNormalMap,
		const std::shared_ptr<TrileMap> &trileMap, const std::vector<std::shared_ptr<Trile>> &trileSet);

	// Inherited from SceneObject
	void batchRender();

	TrileChunkBatcher* copyFactory() {
		return new TrileChunkBatcher(*this);
	}

	friend class TrileChunk;
};

class TrileMapPhysics : public d3d::SceneObject {
private:
	// Physics
	d3d::SceneObjectRef _physicsWorld;

	struct Bounds {
		d3d::Point3i _min, _max;
	};

	struct Box {
		std::shared_ptr<btCollisionShape> _pCollisionShape;
		std::shared_ptr<btDefaultMotionState> _pMotionState;
		std::shared_ptr<btRigidBody> _pRigidBody;
	};

	std::vector<Box> _boxes;

public:
	void create(const std::shared_ptr<TrileMap> &trileMap, float friction, float restitution);

	// Inherited from SceneObject
	void onQueue();
	void onDestroy();

	TrileMapPhysics* copyFactory() {
		return new TrileMapPhysics(*this);
	}
};

class TrileBatcher : public d3d::SceneObject {
public:
	struct TrileAndTransform {
		Trile* _pTrile;
		d3d::Matrix4x4f _transform;
		float _emissivity;

		TrileAndTransform() {}
		TrileAndTransform(Trile* pTrile, const d3d::Matrix4x4f &transform, float emissivity)
			: _pTrile(pTrile), _transform(transform), _emissivity(emissivity)
		{}
	};

private:
	std::list<TrileAndTransform> _triles;

	std::shared_ptr<d3d::Shader> _trileRenderShader;
	std::shared_ptr<d3d::Shader> _depthRenderShader;

	std::shared_ptr<d3d::TextureCube> _trileNormalMap;

public:
	TrileBatcher() {
		_renderMask = 0xffff;
	}

	void create(const std::shared_ptr<d3d::Shader> &trileRenderShader, const std::shared_ptr<d3d::Shader> &depthRenderShader, const std::shared_ptr<d3d::TextureCube> &trileNormalMap);

	void add(Trile* pTrile, const d3d::Matrix4x4f &transform, float emissivity = 0.0f) {
		_triles.push_back(TrileAndTransform(pTrile, transform, emissivity));
	}

	// Inherited from SceneObject
	void batchRender();

	TrileBatcher* copyFactory() {
		return new TrileBatcher(*this);
	}

	friend class Trile;
};

std::shared_ptr<TrileMap> createTrileMapFromRootName(d3d::RenderScene* pScene, const std::string &name, size_t numTriles);