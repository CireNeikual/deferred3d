#pragma once

#include <sceneobjects/trixel/TrileMap.h>

struct Trixel {
	d3d::Vec3f _position;
	d3d::Quaternion _rotation;
	d3d::Vec3f _color;
};

class TrileExplosion : public d3d::SceneObject {
private:
	d3d::SceneObjectRef _batcher;

	std::vector<Trixel> _trixels;
	std::vector<d3d::Vec3f> _linearVelocities;
	std::vector<d3d::Quaternion> _angularVelocities;

	std::shared_ptr<d3d::VBO> _vertices;

public:
	float _despawnTimer;
	float _despawnTime;

	TrileExplosion()
		: _despawnTimer(0.0f), _despawnTime(2.0f)
	{
		_renderMask = 0xffff;
	}

	void create(TrileVoxelData* pTrileData, const d3d::Matrix4x4f &transform, const d3d::Vec3f &explosionCenter, float strength, float attenuationConstant, float linearVelocityMaxPerturbation, float maxSpinVel);

	// Inherited from SceneObject
	void onAdd();
	void update(float dt);
	void deferredRender();

	TrileExplosion* copyFactory() {
		return new TrileExplosion(*this);
	}

	friend class TrileExplosionBatcher;
};

class TrileExplosionBatcher : public d3d::SceneObject {
private:
	std::shared_ptr<d3d::Shader> _trixelsRenderShader;
	std::shared_ptr<d3d::Shader> _trixelsDepthRenderShader;

	std::list<d3d::SceneObjectRef> _explosions;

public:
	bool _castShadows;

	TrileExplosionBatcher()
		: _castShadows(true)
	{
		_renderMask = 0xffff;
	}

	void create(const std::shared_ptr<d3d::Shader> &trixelsRenderShader, const std::shared_ptr<d3d::Shader> &trixelsDepthRenderShader);

	// Inherited from SceneObject
	void batchRender();

	TrileExplosionBatcher* copyFactory() {
		return new TrileExplosionBatcher(*this);
	}

	friend class TrileExplosion;
};