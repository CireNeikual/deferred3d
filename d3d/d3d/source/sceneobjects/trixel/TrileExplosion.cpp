#include <sceneobjects/trixel/TrileExplosion.h>

void TrileExplosion::create(TrileVoxelData* pTrileData, const d3d::Matrix4x4f &transform, const d3d::Vec3f &explosionCenter, float strength, float attenuationConstant, float linearVelocityMaxPerturbation, float maxSpinVel) {
	assert(getScene() != nullptr);

	d3d::Quaternion rotation(d3d::Quaternion::getFromMatrix(transform));

	for (size_t i = 0; i < pTrileData->_colors.size(); i++) {
		if (pTrileData->_colors[i]._id != 0) {
			d3d::Point3i point(i % Trile::_trileVoxelSize,
				(i / Trile::_trileVoxelSize) % Trile::_trileVoxelSize,
				(i / (Trile::_trileVoxelSize * Trile::_trileVoxelSize)) % Trile::_trileVoxelSize);

			d3d::Vec3f position(transform * d3d::Vec3f(point.x * Trile::_trileSize - 0.5f, point.y * Trile::_trileSize - 0.5f, point.z * Trile::_trileSize - 0.5f));

			Trixel t;
			t._color = d3d::Vec3f(pTrileData->_colors[i]._r * 0.0031921f, pTrileData->_colors[i]._g * 0.0031921f, pTrileData->_colors[i]._b * 0.0031921f);
			t._position = position;
			t._rotation = rotation;
			
			_trixels.push_back(t);
		}
	}

	_linearVelocities.resize(_trixels.size());
	_angularVelocities.resize(_trixels.size());

	std::uniform_real_distribution<float> uniformDist(0.0f, 1.0f);
	std::uniform_real_distribution<float> normalDist(-1.0f, 1.0f);
	std::uniform_real_distribution<float> angleVelDist(-maxSpinVel, maxSpinVel);

	for (size_t i = 0; i < _trixels.size(); i++) {
		d3d::Vec3f dPos(_trixels[i]._position - explosionCenter);

		float dist = dPos.magnitude();

		dPos /= dist;

		d3d::Vec3f linearVelocityPerturbation = d3d::Vec3f(normalDist(getScene()->_randomGenerator), normalDist(getScene()->_randomGenerator), normalDist(getScene()->_randomGenerator)).normalized() * uniformDist(getScene()->_randomGenerator) * linearVelocityMaxPerturbation;

		_linearVelocities[i] = dPos / (attenuationConstant + dist) * strength * uniformDist(getScene()->_randomGenerator);

		_angularVelocities[i] = d3d::Quaternion(angleVelDist(getScene()->_randomGenerator), d3d::Vec3f(normalDist(getScene()->_randomGenerator), normalDist(getScene()->_randomGenerator), normalDist(getScene()->_randomGenerator)).normalized());
	}

	_vertices.reset(new d3d::VBO());

	_vertices->create();

	_vertices->bind(GL_ARRAY_BUFFER);

	glBufferData(GL_ARRAY_BUFFER, sizeof(Trixel) * _trixels.size(), &_trixels[0], GL_DYNAMIC_DRAW);
}

void TrileExplosion::onAdd() {
	_batcher = getScene()->getNamed("trileEB");

	assert(_batcher.isAlive());
}

void TrileExplosion::update(float dt) {
	for (size_t i = 0; i < _trixels.size(); i++) {
		_linearVelocities[i] += d3d::Vec3f(0.0f, -4.0f, 0.0f) * dt;
		_trixels[i]._position += _linearVelocities[i] * dt;
		_trixels[i]._rotation *= _angularVelocities[i] * dt;
		_trixels[i]._rotation.normalize();
	}

	_despawnTimer += dt;

	if (_despawnTimer > _despawnTime)
		destroy();
}

void TrileExplosion::deferredRender() {
	assert(_batcher.isAlive());

	static_cast<TrileExplosionBatcher*>(_batcher.get())->_explosions.push_back(this);
}

void TrileExplosionBatcher::create(const std::shared_ptr<d3d::Shader> &trixelsRenderShader, const std::shared_ptr<d3d::Shader> &trixelsDepthRenderShader) {
	_trixelsRenderShader = trixelsRenderShader;
	_trixelsDepthRenderShader = trixelsDepthRenderShader;
}

void TrileExplosionBatcher::batchRender() {
	if (getRenderScene()->_shaderSwitchesEnabled) {
		if (!_explosions.empty()) {
			_trixelsRenderShader->bind();
			_trixelsRenderShader->setUniformmat4("d3dViewModel", getRenderScene()->_renderCamera.getViewMatrix());
			_trixelsRenderShader->setUniformmat4("d3dProjectionViewModel", getRenderScene()->_renderCamera.getProjectionViewMatrix());
			_trixelsRenderShader->setUniformmat3("d3dNormal", getRenderScene()->_renderCamera.getNormalMatrix());

			for (std::list<d3d::SceneObjectRef>::iterator it = _explosions.begin(); it != _explosions.end(); it++) {
				TrileExplosion* pExplosion = static_cast<TrileExplosion*>(it->get());

				pExplosion->_vertices->bind(GL_ARRAY_BUFFER);

				glBufferData(GL_ARRAY_BUFFER, sizeof(Trixel) * pExplosion->_trixels.size(), &pExplosion->_trixels[0], GL_DYNAMIC_DRAW);

				// Set material
				glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Trixel), 0);
				glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Trixel), reinterpret_cast<const GLvoid*>(sizeof(d3d::Vec3f)));
				glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Trixel), reinterpret_cast<const GLvoid*>(sizeof(d3d::Vec4f) + sizeof(d3d::Vec3f)));

				glDrawArrays(GL_POINTS, 0, pExplosion->_trixels.size());
			}
		}
	}
	/*else if (getRenderScene()->_renderingShadows) {
		if (!_explosions.empty()) {
			d3d::Shader* pPrevShader = d3d::Shader::getCurrentShader();

			_depthRenderShader->bind();

			glDisableVertexAttribArray(D3D_ATTRIB_TEXCOORD);
			glDisableVertexAttribArray(D3D_ATTRIB_NORMAL);

			for (std::list<TrileAndTransform>::iterator it = _explosions.begin(); it != _triles.end(); it++) {
				// Set material
				_trileRenderShader->setShaderTexture("trileTexture", it->_pTrile->_cubeMapID, GL_TEXTURE_CUBE_MAP);
				_trileRenderShader->bindShaderTextures();

				it->_pTrile->_vertexBuffer.bind(GL_ARRAY_BUFFER);

				glVertexAttribPointer(D3D_ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(Trile::Vertex), 0);
				glVertexAttribPointer(D3D_ATTRIB_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(Trile::Vertex), reinterpret_cast<const GLvoid*>(sizeof(d3d::Vec3f)));

				it->_pTrile->_indexBuffer.bind(GL_ELEMENT_ARRAY_BUFFER);

				getRenderScene()->setTransform(it->_transform);

				glDrawElements(GL_TRIANGLES, it->_pTrile->_numIndices, GL_UNSIGNED_SHORT, nullptr);
			}

			glEnableVertexAttribArray(D3D_ATTRIB_NORMAL);
			glEnableVertexAttribArray(D3D_ATTRIB_TEXCOORD);

			pPrevShader->bind();
		}
	}*/

	_explosions.clear();
}