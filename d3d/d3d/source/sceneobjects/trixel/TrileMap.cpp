#include <sceneobjects/trixel/TrileMap.h>

#include <util/Math.h>

#include <fstream>
#include <sstream>

const float Trile::_trileSize = 0.0625f;

Trile::~Trile() {
	if (_cubeMapID != 0)
		glDeleteTextures(1, &_cubeMapID);
}

bool Trile::createAsset(const std::string &name) {
	std::istringstream iss(name);

	std::string chunkName, textureName;

	iss >> chunkName >> textureName;

	// ------------------------------------ Load Geometry ------------------------------------

	std::ifstream fromFile(chunkName);

	if (!fromFile.is_open()) {
		std::cerr << "Could not open file " << name << std::endl;

		return false;
	}

	std::array<std::vector<Quad>, 6> faceQuads;

	int currentFace = 0;

	while (!fromFile.eof() && fromFile.good()) {
		std::string line;
		std::getline(fromFile, line);

		std::istringstream fromLine(line);

		std::string heading;

		fromLine >> heading;

		if (heading == "f")
			fromLine >> currentFace;
		else if (heading == "q") {
			Quad quad;

			for (int i = 0; i < 4; i++) {
				int value;

				fromLine >> value;

				d3d::Point3i p = get3DChunkCoord(value);

				quad.positions[i] = d3d::Vec3f(p.x * _trileSize - 0.5f, p.y * _trileSize - 0.5f, p.z * _trileSize - 0.5f);
			}

			faceQuads[currentFace].push_back(quad);
		}
	}

	std::vector<Vertex> vertices;
	std::vector<unsigned short> indicies;

	size_t totalSize = 0;

	for (size_t i = 0; i < 6; i++)
		totalSize += faceQuads[i].size() * 4;

	vertices.resize(totalSize);

	_minBounds._lowerBound = d3d::Vec3f(99999.0f, 99999.0f, 99999.0f);
	_minBounds._upperBound = d3d::Vec3f(-99999.0f, -99999.0f, -99999.0f);

	size_t vertIndex = 0;

	for (size_t i = 0; i < 6; i++) {
		d3d::Vec3f normal(0.0f, 0.0f, 1.0f);

		switch (i) {
		case 0:
			normal = d3d::Vec3f(-1.0f, 0.0f, 0.0f);
			break;
		case 1:
			normal = d3d::Vec3f(1.0f, 0.0f, 0.0f);
			break;
		case 2:
			normal = d3d::Vec3f(0.0f, -1.0f, 0.0f);
			break;
		case 3:
			normal = d3d::Vec3f(0.0f, 1.0f, 0.0f);
			break;
		case 4:
			normal = d3d::Vec3f(0.0f, 0.0f, -1.0f);
			break;
		}

		for (size_t j = 0; j < faceQuads[i].size(); j++)
		for (size_t k = 0; k < 4; k++) {
			vertices[vertIndex]._position = faceQuads[i][j].positions[k];
			_minBounds.expand(vertices[vertIndex]._position);
			vertices[vertIndex]._normal = normal;

			vertIndex++;
		}
	}

	_minBounds.calculateHalfDims();
	_minBounds.calculateCenter();

	indicies.reserve(vertices.size() * 3 / 2);

	for (size_t i = 0; i < vertices.size(); i += 4) {
		indicies.push_back(i + 2);
		indicies.push_back(i + 1);
		indicies.push_back(i);
		indicies.push_back(i + 3);	
		indicies.push_back(i + 2);
		indicies.push_back(i);
	}

	// Generate buffers
	_vertexBuffer.create();

	_vertexBuffer.bind(GL_ARRAY_BUFFER);

	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vertices.size(), &vertices[0], GL_STATIC_DRAW);

	_numIndices = indicies.size();

	_indexBuffer.create();

	_indexBuffer.bind(GL_ELEMENT_ARRAY_BUFFER);

	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned short) * indicies.size(), &indicies[0], GL_STATIC_DRAW);

	// ------------------------------------ Load Cube Map ------------------------------------

	glGenTextures(1, &_cubeMapID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, _cubeMapID);

	sf::Image image;

	if (!image.loadFromFile(textureName))
		return false;

	for (size_t i = 0; i < 6; i++) {
		sf::Image subImage;

		subImage.create(_trileVoxelSize, _trileVoxelSize);

		for (size_t x = 0; x < _trileVoxelSize; x++)
		for (size_t y = 0; y < _trileVoxelSize; y++)
			subImage.setPixel(x, y, image.getPixel(i * _trileVoxelSize + x, y));

		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + static_cast<GLenum>(i), 0, GL_RGBA8, _trileVoxelSize, _trileVoxelSize, 0, GL_RGBA, GL_UNSIGNED_BYTE, subImage.getPixelsPtr());
	}

	// Default settings
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

	return true;
}

bool TrileVoxelData::createAsset(const std::string &name) {
	std::ifstream fromFile(name);

	if (!fromFile.is_open()) {
		std::cerr << "Could not open file " << name << std::endl;

		return false;
	}

	_colors.resize(Trile::_trileVoxelSize * Trile::_trileVoxelSize * Trile::_trileVoxelSize);

	for (size_t i = 0; i < _colors.size(); i++) {
		int id, r, g, b;

		fromFile >> id;
		fromFile >> r;
		fromFile >> g;
		fromFile >> b;

		_colors[i]._id = static_cast<unsigned char>(id);
		_colors[i]._r = static_cast<unsigned char>(r);
		_colors[i]._g = static_cast<unsigned char>(g);
		_colors[i]._b = static_cast<unsigned char>(b);
	}

	fromFile.close();

	return true;
}

bool TrileMap::createAsset(const std::string &name) {
	std::ifstream fromFile(name);

	if (!fromFile.is_open()) {
		std::cerr << "Could not open file " << name << std::endl;

		return false;
	}

	{
		std::string line;

		std::getline(fromFile, line);

		std::istringstream fromLine(line);

		// Read dimensions
		fromLine >> _size.x >> _size.y >> _size.z;

		_map.resize(_size.x * _size.y * _size.z);
	}

	for (int x = 0; x < _size.x; x++)
	for (int y = 0; y < _size.y; y++) {
		std::string line;

		std::getline(fromFile, line);

		std::istringstream fromLine(line);

		for (int z = 0; z < _size.z; z++) {
			unsigned int value;

			fromLine >> value;

			TrileData &t = mapAt(d3d::Point3i(x, y, z));

			t._id = static_cast<unsigned char>((value & 0xffff0000) >> 16);
			t._rotation = static_cast<unsigned char>(value & 0x0000ffff);
		}
	}

	int numBounds;

	fromFile >> numBounds;

	_physicsBounds.resize(numBounds);

	for (size_t i = 0; i < _physicsBounds.size(); i++)
		fromFile >> _physicsBounds[i]._min.x >> _physicsBounds[i]._min.y >> _physicsBounds[i]._min.z >> _physicsBounds[i]._max.x >> _physicsBounds[i]._max.y >> _physicsBounds[i]._max.z;

	fromFile.close();

	return true;
}

void TrileMap::generateChunks(const std::shared_ptr<TrileMap> &map, d3d::RenderScene* pRenderScene, int chunkSize, const std::shared_ptr<d3d::Shader> &trileRenderShader, const std::shared_ptr<d3d::Shader> &depthRenderShader, const std::shared_ptr<d3d::TextureCube> &trileNormalMap, const std::vector<std::shared_ptr<Trile>> &trileSet) {
	std::shared_ptr<TrileChunkBatcher> batcher(new TrileChunkBatcher());

	pRenderScene->add(batcher);

	batcher->create(trileRenderShader, depthRenderShader, trileNormalMap, map, trileSet);

	for (int cx = 0; cx < map->_size.x; cx += chunkSize)
	for (int cy = 0; cy < map->_size.y; cy += chunkSize)
	for (int cz = 0; cz < map->_size.z; cz += chunkSize) {
		std::shared_ptr<TrileChunk> chunk(new TrileChunk());

		pRenderScene->add(chunk, true);

		chunk->create(d3d::Point3i(cx, cy, cz), d3d::Point3i(std::min(map->_size.x, cx + chunkSize), std::min(map->_size.y, cy + chunkSize), std::min(map->_size.z, cz + chunkSize)), map, batcher.get());
	}
}

void TrileChunk::create(const d3d::Point3i &min, const d3d::Point3i &max, const std::shared_ptr<TrileMap> &trileMap, class TrileChunkBatcher* pBatcher) {
	_min = min;
	_max = max;
	_trileMap = trileMap;

	_batcher = pBatcher;

	d3d::Vec3f center(trileMap->_size.x * 0.5f, trileMap->_size.y * 0.5f, trileMap->_size.z * 0.5f);

	_aabb._lowerBound = d3d::Vec3f(_min.x - 0.5f, _min.y - 0.5f, _min.z - 0.5f) - center;
	_aabb._upperBound = d3d::Vec3f(_max.x + 0.5f, _max.y + 0.5f, _max.z + 0.5f) - center;

	_aabb.calculateHalfDims();
	_aabb.calculateCenter();

	updateAABB();
}

void TrileChunk::deferredRender() {
	assert(_trileMap != nullptr);

	assert(_batcher.isAlive());

	// Add self to batcher
	static_cast<TrileChunkBatcher*>(_batcher.get())->_chunks.push_back(this);
}

void TrileChunkBatcher::create(const std::shared_ptr<d3d::Shader> &trileRenderShader, const std::shared_ptr<d3d::Shader> &depthRenderShader, const std::shared_ptr<d3d::TextureCube> &trileNormalMap,
	const std::shared_ptr<TrileMap> &trileMap, const std::vector<std::shared_ptr<Trile>> &trileSet) {
	_trileRenderShader = trileRenderShader;
	_depthRenderShader = depthRenderShader;
	_trileNormalMap = trileNormalMap;
	_trileMap = trileMap;
	_trileSet = trileSet;

	_trileRenderShader->bind();
	_trileRenderShader->setShaderTexture("trileNormalMap", _trileNormalMap->getTextureID(), GL_TEXTURE_CUBE_MAP);
}

void TrileChunkBatcher::batchRender() {
	if (getRenderScene()->_shaderSwitchesEnabled) {
		if (!_chunks.empty()) {
			// Compile render data lists
			std::vector<std::list<TrileRenderData>> renderDataLists;

			renderDataLists.resize(_trileSet.size());

			for (std::list<d3d::SceneObjectRef>::iterator it = _chunks.begin(); it != _chunks.end(); it++) {
				assert((*it).isAlive());

				TrileChunk* pChunk = static_cast<TrileChunk*>((*it).get());

				for (int x = pChunk->_min.x; x < pChunk->_max.x; x++)
				for (int y = pChunk->_min.y; y < pChunk->_max.y; y++)
				for (int z = pChunk->_min.z; z < pChunk->_max.z; z++) {
					const TrileMap::TrileData &data = _trileMap->mapAt(d3d::Point3i(x, y, z));

					if (data._id == 0)
						continue;

					assert(data._id - 1 < renderDataLists.size()); // Make sure the trile ID is less than the trile set trile count!

					TrileRenderData renderData;
					renderData._position = d3d::Point3i(x, y, z);
					renderData._rot = data._rotation;

					renderDataLists[data._id - 1].push_back(renderData);
				}
			}

			d3d::Vec3f center(_trileMap->_size.x * 0.5f, _trileMap->_size.y * 0.5f, _trileMap->_size.z * 0.5f);

			_trileRenderShader->bind();

			glDisableVertexAttribArray(D3D_ATTRIB_TEXCOORD);

			for (size_t i = 0; i < renderDataLists.size(); i++) {
				if (renderDataLists[i].empty())
					continue;

				// Set material
				_trileRenderShader->setShaderTexture("trileTexture", _trileSet[i]->_cubeMapID, GL_TEXTURE_CUBE_MAP);
				_trileRenderShader->bindShaderTextures();

				_trileSet[i]->_vertexBuffer.bind(GL_ARRAY_BUFFER);

				glVertexAttribPointer(D3D_ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(Trile::Vertex), 0);
				glVertexAttribPointer(D3D_ATTRIB_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(Trile::Vertex), reinterpret_cast<const GLvoid*>(sizeof(d3d::Vec3f)));

				_trileSet[i]->_indexBuffer.bind(GL_ELEMENT_ARRAY_BUFFER);

				for (std::list<TrileRenderData>::iterator it = renderDataLists[i].begin(); it != renderDataLists[i].end(); it++) {
					getRenderScene()->setTransform(d3d::Matrix4x4f::translateMatrix(d3d::Vec3f(it->_position.x, it->_position.y, it->_position.z) - center + d3d::Vec3f(0.5f, 0.5f, 0.5f)) * d3d::Matrix4x4f::rotateMatrixY(-static_cast<float>(it->_rot) * d3d::_piOver2));

					glDrawElements(GL_TRIANGLES, _trileSet[i]->_numIndices, GL_UNSIGNED_SHORT, nullptr);
				}
			}

			glEnableVertexAttribArray(D3D_ATTRIB_TEXCOORD);
		}
	}
	else if (getRenderScene()->_renderingShadows) {
		if (!_chunks.empty()) {
			// Compile render data lists
			std::vector<std::list<TrileRenderData>> renderDataLists;

			renderDataLists.resize(_trileSet.size());

			for (std::list<d3d::SceneObjectRef>::iterator it = _chunks.begin(); it != _chunks.end(); it++) {
				assert((*it).isAlive());

				TrileChunk* pChunk = static_cast<TrileChunk*>((*it).get());

				for (int x = pChunk->_min.x; x < pChunk->_max.x; x++)
				for (int y = pChunk->_min.y; y < pChunk->_max.y; y++)
				for (int z = pChunk->_min.z; z < pChunk->_max.z; z++) {
					const TrileMap::TrileData &data = _trileMap->mapAt(d3d::Point3i(x, y, z));

					if (data._id == 0)
						continue;

					assert(data._id - 1 < renderDataLists.size()); // Make sure the trile ID is less than the trile set trile count!

					TrileRenderData renderData;
					renderData._position = d3d::Point3i(x, y, z);
					renderData._rot = data._rotation;

					renderDataLists[data._id - 1].push_back(renderData);
				}
			}

			d3d::Vec3f center(_trileMap->_size.x * 0.5f, _trileMap->_size.y * 0.5f, _trileMap->_size.z * 0.5f);

			d3d::Shader* pPrevShader = d3d::Shader::getCurrentShader();

			_depthRenderShader->bind();

			glDisableVertexAttribArray(D3D_ATTRIB_TEXCOORD);
			glDisableVertexAttribArray(D3D_ATTRIB_NORMAL);

			for (size_t i = 0; i < renderDataLists.size(); i++) {
				if (renderDataLists[i].empty())
					continue;

				_trileSet[i]->_vertexBuffer.bind(GL_ARRAY_BUFFER);

				glVertexAttribPointer(D3D_ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(Trile::Vertex), 0);

				_trileSet[i]->_indexBuffer.bind(GL_ELEMENT_ARRAY_BUFFER);

				for (std::list<TrileRenderData>::iterator it = renderDataLists[i].begin(); it != renderDataLists[i].end(); it++) {
					getRenderScene()->setTransform(d3d::Matrix4x4f::translateMatrix(d3d::Vec3f(it->_position.x, it->_position.y, it->_position.z) - center + d3d::Vec3f(0.5f, 0.5f, 0.5f)) * d3d::Matrix4x4f::rotateMatrixY(-static_cast<float>(it->_rot) * d3d::_piOver2));

					glDrawElements(GL_TRIANGLES, _trileSet[i]->_numIndices, GL_UNSIGNED_SHORT, nullptr);
				}
			}

			glEnableVertexAttribArray(D3D_ATTRIB_NORMAL);
			glEnableVertexAttribArray(D3D_ATTRIB_TEXCOORD);

			pPrevShader->bind();
		}
	}

	_chunks.clear();

	glFlush();
}

void TrileBatcher::create(const std::shared_ptr<d3d::Shader> &trileRenderShader, const std::shared_ptr<d3d::Shader> &depthRenderShader, const std::shared_ptr<d3d::TextureCube> &trileNormalMap) {
	_trileRenderShader = trileRenderShader;
	_depthRenderShader = depthRenderShader;
	_trileNormalMap = trileNormalMap;

	_trileRenderShader->bind();
	_trileRenderShader->setShaderTexture("trileNormalMap", _trileNormalMap->getTextureID(), GL_TEXTURE_CUBE_MAP);
}

void TrileBatcher::batchRender() {
	if (getRenderScene()->_shaderSwitchesEnabled) {
		if (!_triles.empty()) {
			_trileRenderShader->bind();

			glDisableVertexAttribArray(D3D_ATTRIB_TEXCOORD);

			for (std::list<TrileAndTransform>::iterator it = _triles.begin(); it != _triles.end(); it++) {
				// Set material
				_trileRenderShader->setShaderTexture("trileTexture", it->_pTrile->_cubeMapID, GL_TEXTURE_CUBE_MAP);
				_trileRenderShader->setUniformf("d3dEmissiveColor", it->_emissivity);
				_trileRenderShader->bindShaderTextures();

				it->_pTrile->_vertexBuffer.bind(GL_ARRAY_BUFFER);

				glVertexAttribPointer(D3D_ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(Trile::Vertex), 0);
				glVertexAttribPointer(D3D_ATTRIB_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(Trile::Vertex), reinterpret_cast<const GLvoid*>(sizeof(d3d::Vec3f)));

				it->_pTrile->_indexBuffer.bind(GL_ELEMENT_ARRAY_BUFFER);

				getRenderScene()->setTransform(it->_transform);

				glDrawElements(GL_TRIANGLES, it->_pTrile->_numIndices, GL_UNSIGNED_SHORT, nullptr);
			}

			glEnableVertexAttribArray(D3D_ATTRIB_TEXCOORD);
		}
	}
	else if (getRenderScene()->_renderingShadows) {
		if (!_triles.empty()) {
			d3d::Shader* pPrevShader = d3d::Shader::getCurrentShader();

			_depthRenderShader->bind();

			glDisableVertexAttribArray(D3D_ATTRIB_TEXCOORD);
			glDisableVertexAttribArray(D3D_ATTRIB_NORMAL);

			for (std::list<TrileAndTransform>::iterator it = _triles.begin(); it != _triles.end(); it++) {
				it->_pTrile->_vertexBuffer.bind(GL_ARRAY_BUFFER);

				glVertexAttribPointer(D3D_ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(Trile::Vertex), 0);
				glVertexAttribPointer(D3D_ATTRIB_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(Trile::Vertex), reinterpret_cast<const GLvoid*>(sizeof(d3d::Vec3f)));

				it->_pTrile->_indexBuffer.bind(GL_ELEMENT_ARRAY_BUFFER);

				getRenderScene()->setTransform(it->_transform);

				glDrawElements(GL_TRIANGLES, it->_pTrile->_numIndices, GL_UNSIGNED_SHORT, nullptr);
			}

			glEnableVertexAttribArray(D3D_ATTRIB_NORMAL);
			glEnableVertexAttribArray(D3D_ATTRIB_TEXCOORD);

			pPrevShader->bind();
		}
	}

	_triles.clear();

	glFlush();
}

std::shared_ptr<TrileMap> createTrileMapFromRootName(d3d::RenderScene* pScene, const std::string &name, size_t numTriles) {
	std::shared_ptr<TrileMap> map(new TrileMap());

	map->createAsset(name + ".tmap");

	std::vector<std::shared_ptr<Trile>> triles(numTriles);

	for (size_t i = 0; i < numTriles; i++) {
		triles[i].reset(new Trile());
		triles[i]->createAsset(name + std::to_string(i) + ".trile " + name + std::to_string(i) + ".png");
	}

	std::shared_ptr<d3d::Asset> asset;

	if (!pScene->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("NONE resources/shaders/custom/trileRender.vert resources/shaders/custom/trileRender.frag", asset))
		abort();

	std::shared_ptr<d3d::Shader> trileRenderShader = std::static_pointer_cast<d3d::Shader>(asset);

	if (!pScene->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("NONE resources/shaders/positionOnlyVertex.vert resources/shaders/positionOnlyFragment.frag", asset))
		abort();

	std::shared_ptr<d3d::Shader> depthRenderShader = std::static_pointer_cast<d3d::Shader>(asset);

	if (!pScene->getAssetManager("texCube", d3d::TextureCube::assetFactory)->getAsset(
		"resources/trixel/normalMap.png "
		"resources/trixel/normalMap.png "
		"resources/trixel/normalMap.png "
		"resources/trixel/normalMap.png "
		"resources/trixel/normalMap.png "
		"resources/trixel/normalMap.png ", asset))
		abort();

	std::shared_ptr<d3d::TextureCube> trileNormalMap = std::static_pointer_cast<d3d::TextureCube>(asset);

	map->generateChunks(map, pScene, 4, trileRenderShader, depthRenderShader, trileNormalMap, triles);

	std::shared_ptr<TrileMapPhysics> mapPhysics(new TrileMapPhysics());

	pScene->add(mapPhysics, false);

	mapPhysics->create(map, 1.0f, 0.0f);
	
	return map;
}

void TrileMapPhysics::create(const std::shared_ptr<TrileMap> &trileMap, float friction, float restitution) {
	assert(getScene() != nullptr);
	assert(_physicsWorld.isAlive());

	d3d::SceneObjectPhysicsWorld* pPhysicsWorld = static_cast<d3d::SceneObjectPhysicsWorld*>(_physicsWorld.get());

	_boxes.resize(trileMap->_physicsBounds.size());

	for (size_t i = 0; i < _boxes.size(); i++) {
		d3d::Vec3f minf(static_cast<float>(trileMap->_physicsBounds[i]._min.x), static_cast<float>(trileMap->_physicsBounds[i]._min.y), static_cast<float>(trileMap->_physicsBounds[i]._min.z));
		d3d::Vec3f maxf(static_cast<float>(trileMap->_physicsBounds[i]._max.x), static_cast<float>(trileMap->_physicsBounds[i]._max.y), static_cast<float>(trileMap->_physicsBounds[i]._max.z));

		d3d::Vec3f center(trileMap->_size.x * 0.5f, trileMap->_size.y * 0.5f, trileMap->_size.z * 0.5f);

		minf -= center;
		maxf -= center;

		_boxes[i]._pCollisionShape.reset(new btBoxShape(bt((maxf - minf) * 0.5f)));

		_boxes[i]._pMotionState.reset(new btDefaultMotionState(btTransform(btQuaternion::getIdentity(), bt((minf + maxf) * 0.5f))));

		btVector3 inertia;
		_boxes[i]._pCollisionShape->calculateLocalInertia(0.0f, inertia);

		btRigidBody::btRigidBodyConstructionInfo rigidBodyCI(0.0f, _boxes[i]._pMotionState.get(), _boxes[i]._pCollisionShape.get(), inertia);

		rigidBodyCI.m_restitution = restitution;
		rigidBodyCI.m_friction = friction;

		_boxes[i]._pRigidBody.reset(new btRigidBody(rigidBodyCI));

		pPhysicsWorld->_pDynamicsWorld->addRigidBody(_boxes[i]._pRigidBody.get());
	}
}

void TrileMapPhysics::onQueue() {
	_physicsWorld = getScene()->getNamedCheckQueue("physWrld");

	assert(_physicsWorld.isAlive());
}

void TrileMapPhysics::onDestroy() {
	if (_physicsWorld.isAlive()) {
		d3d::SceneObjectPhysicsWorld* pPhysicsWorld = static_cast<d3d::SceneObjectPhysicsWorld*>(_physicsWorld.get());

		for (size_t i = 0; i < _boxes.size(); i++)
		if (_boxes[i]._pRigidBody != nullptr)
			pPhysicsWorld->_pDynamicsWorld->removeRigidBody(_boxes[i]._pRigidBody.get());
	}
}