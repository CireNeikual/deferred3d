#include <network/RakWin.h>

#include <sceneobjects/SceneObjectEntryPoint.h>

#include <sceneobjects/SceneObjectConfig.h>
#include <sceneobjects/SceneObjectProp.h>
#include <sceneobjects/SceneObjectPlayer.h>
#include <sceneobjects/SceneObjectBox.h>
#include <sceneobjects/virtualcreatures/SceneObjectGeneticAlgorithm.h>
#include <sceneobjects/gui/SceneObjectGUI.h>
#include <sceneobjects/gui/SceneObjectMousePointer.h>
#include <sceneobjects/map/Map3DWS.h>
#include <sceneobjects/trixel/TrileMap.h>
#include <sceneobjects/trixel/TrileExplosion.h>
#include <sceneobjects/units/SceneObjectBattlefieldController.h>
#include <sceneobjects/units/SceneObjectArmyController.h>
#include <sceneobjects/units/SceneObjectUnit.h>
#include <sceneobjects/units/SceneObjectSpawner.h>
#include <sceneobjects/game/SceneObjectCommander.h>
#include <sceneobjects/game/SceneObjectAICommander.h>

#include <sceneobjects/gui/SceneObjectOptionsMenu.h>
#include <sceneobjects/gui/SceneObjectChatBox.h>

#include <sceneobjects/units/SceneObjectUnitMediumTank.h>

#include <rendering/voxel/VoxelTerrain.h>
#include <rendering/voxel/TerrainGenerator.h>
#include <rendering/lighting/SceneObjectDirectionalLightShadowed.h>
#include <rendering/lighting/SceneObjectPointLightShadowed.h>

#include <rendering/imageeffects/SceneObjectEffectBuffer.h>
#include <rendering/imageeffects/SceneObjectSSAO.h>
#include <rendering/imageeffects/SceneObjectLightScattering.h>
#include <rendering/imageeffects/SceneObjectDepthOfField.h>
#include <rendering/imageeffects/SceneObjectSSR.h>
#include <rendering/imageeffects/SceneObjectFXAA.h>
#include <rendering/imageeffects/SceneObjectFog.h>

#include <system/SoftwareImage2D.h>

void SceneObjectEntryPoint::onAdd() {
	// Physics

	std::shared_ptr<d3d::SceneObjectPhysicsWorld> physicsWorld(new d3d::SceneObjectPhysicsWorld());

	getRenderScene()->addNamed(physicsWorld, "physWrld");

	d3d::SceneObjectRef lighting = getScene()->getNamed("lighting");

	d3d::SceneObjectLighting* pLighting = static_cast<d3d::SceneObjectLighting*>(lighting.get());

	pLighting->_ambientLight = d3d::Vec3f(0.3f, 0.3f, 0.3f);

	std::shared_ptr<d3d::SceneObjectDirectionalLightShadowed> light(new d3d::SceneObjectDirectionalLightShadowed());

	getRenderScene()->add(light);

	light->create(pLighting, 2, 2048, 0.5f, 12.5f, 0.6f);

	light->setDirection(d3d::Vec3f(-0.4f, -1.0f, 0.6f).normalized());

	light->setColor(d3d::Vec3f(1.0f, 1.0f, 1.0f));

	// Trixels

	std::shared_ptr<TrileMap> map = createTrileMapFromRootName(getRenderScene(), "resources/trixel/maps/map1/trile", 12);

	std::shared_ptr<d3d::Asset> asset;

	if (!getScene()->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("NONE resources/shaders/custom/trileRender.vert resources/shaders/custom/trileRender.frag", asset))
		abort();

	std::shared_ptr<d3d::Shader> trileRenderShader = std::static_pointer_cast<d3d::Shader>(asset);

	if (!getScene()->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("NONE resources/shaders/positionOnlyVertex.vert resources/shaders/positionOnlyFragment.frag", asset))
		abort();

	std::shared_ptr<d3d::Shader> depthRenderShader = std::static_pointer_cast<d3d::Shader>(asset);

	if (!getScene()->getAssetManager("texCube", d3d::TextureCube::assetFactory)->getAsset(
		"resources/trixel/normalMap.png "
		"resources/trixel/normalMap.png "
		"resources/trixel/normalMap.png "
		"resources/trixel/normalMap.png "
		"resources/trixel/normalMap.png "
		"resources/trixel/normalMap.png", asset))
		abort();

	std::shared_ptr<d3d::TextureCube> trileNormalMap = std::static_pointer_cast<d3d::TextureCube>(asset);

	std::shared_ptr<TrileBatcher> trileBatcher(new TrileBatcher());

	getScene()->addNamed(trileBatcher, "trileBatcher", false);

	trileBatcher->create(trileRenderShader, depthRenderShader, trileNormalMap);

	// GUI

	std::shared_ptr<SceneObjectGUI> gui(new SceneObjectGUI());

	getRenderScene()->addNamed(gui, "gui", false);

	std::shared_ptr<d3d::Texture2D> pointerTexture(new d3d::Texture2D());

	pointerTexture->createAsset("resources/textures/mousePointer.png");

	pointerTexture->bind();

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	std::shared_ptr<SceneObjectMousePointer> mousePointer(new SceneObjectMousePointer());

	getRenderScene()->add(mousePointer, false);

	mousePointer->create(pointerTexture);

	mousePointer->_layer = 3.0f;

	gui->_layer = 2.0f;

	//std::shared_ptr<SceneObjectOptionsMenu> optionMenu(new SceneObjectOptionsMenu());

	//getRenderScene()->add(optionMenu, false);

	// Voxel explosion

	std::shared_ptr<d3d::Shader> trixelRenderShader(new d3d::Shader());

	trixelRenderShader->createAsset("resources/shaders/custom/trixelRender.geom resources/shaders/custom/trixelRender.vert resources/shaders/custom/trixelRender.frag");

	std::shared_ptr<TrileExplosionBatcher> explosionBatcher(new TrileExplosionBatcher());

	getRenderScene()->addNamed(explosionBatcher, "trileEB", false);

	explosionBatcher->create(trixelRenderShader, trixelRenderShader);

	// Battlefield

	std::shared_ptr<SceneObjectBattlefieldController> battlefield(new SceneObjectBattlefieldController());

	getRenderScene()->add(battlefield, false);

	battlefield->create(map, -0.5f, 5, 5334);

	// Commander

	std::shared_ptr<SceneObjectCommander> commander(new SceneObjectCommander());

	getRenderScene()->add(commander);

	commander->create(map, battlefield->getSide(0));

	// AI

	std::shared_ptr<SceneObjectAICommander> aiCommander(new SceneObjectAICommander());

	getRenderScene()->add(aiCommander);

	aiCommander->create(map, battlefield->getSide(1));

	// ------------------------------------------- Image Effects -------------------------------------------

	std::shared_ptr<d3d::SceneObjectEffectBuffer> effectBuffer(new d3d::SceneObjectEffectBuffer());

	getRenderScene()->addNamed(effectBuffer, "ebuf", false);

	effectBuffer->create(0.5f);

	std::shared_ptr<d3d::Asset> assetBlurHorizontal;

	getScene()->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/blurHorizontal.frag", assetBlurHorizontal);

	std::shared_ptr<d3d::Shader> blurShaderHorizontal = std::static_pointer_cast<d3d::Shader>(assetBlurHorizontal);

	std::shared_ptr<d3d::Asset> assetBlurVertical;

	getScene()->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/blurVertical.frag", assetBlurVertical);

	std::shared_ptr<d3d::Shader> blurShaderVertical = std::static_pointer_cast<d3d::Shader>(assetBlurVertical);

	std::shared_ptr<d3d::Asset> assetRenderImage;

	getScene()->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/renderImage.frag", assetRenderImage);

	std::shared_ptr<d3d::Shader> renderImageShader = std::static_pointer_cast<d3d::Shader>(assetRenderImage);

	std::shared_ptr<d3d::Asset> assetNoise;

	getScene()->getAssetManager("tex2D", d3d::Texture2D::assetFactory)->getAsset("resources/textures/noise.bmp", assetNoise);

	std::shared_ptr<d3d::Texture2D> noiseMap = std::static_pointer_cast<d3d::Texture2D>(assetNoise);

	std::shared_ptr<d3d::Asset> assetBlurHorizontalEdgeAware;

	getScene()->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/blurHorizontalEdgeAware.frag", assetBlurHorizontalEdgeAware);

	std::shared_ptr<d3d::Shader> blurShaderHorizontalEdgeAware = std::static_pointer_cast<d3d::Shader>(assetBlurHorizontalEdgeAware);

	std::shared_ptr<d3d::Asset> assetBlurVerticalEdgeAware;

	getScene()->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/blurVerticalEdgeAware.frag", assetBlurVerticalEdgeAware);

	std::shared_ptr<d3d::Shader> blurShaderVerticalEdgeAware = std::static_pointer_cast<d3d::Shader>(assetBlurVerticalEdgeAware);

	// Get config
	d3d::SceneObjectRef config = getScene()->getNamed("config");

	SceneObjectConfig* pConfig = static_cast<SceneObjectConfig*>(config.get());

	// SSAO
	if (pConfig->_ssao) {
		std::shared_ptr<d3d::Shader> ssaoShader(new d3d::Shader());

		ssaoShader->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/imageeffects/ssao.frag");

		std::shared_ptr<d3d::SceneObjectSSAO> ssao(new d3d::SceneObjectSSAO());

		getRenderScene()->addNamed(ssao, "ssao", false);

		ssao->create(blurShaderHorizontal, blurShaderVertical, ssaoShader, renderImageShader, noiseMap);

		ssao->_ssaoRadius = 0.33f;
		ssao->_ssaoStrength = 6.0f;
		ssao->_blurRadius = 0.00273f;
		ssao->_numBlurPasses = 1;
	}

	// Fog
	std::shared_ptr<d3d::Shader> fogShader(new d3d::Shader());

	fogShader->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/imageeffects/fog.frag");

	std::shared_ptr<d3d::SceneObjectFog> fog(new d3d::SceneObjectFog());

	getScene()->addNamed(fog, "fog", false);

	fog->create(fogShader);

	fog->_fogColor = d3d::Vec3f(1.0f, 0.8f, 0.6f) * 0.115f;
	fog->_fogStartDistance = 6.0f;

	fog->_layer = 1.2f;

	// SSR

	/*std::shared_ptr<d3d::TextureCube> cubeMap(new d3d::TextureCube());

	cubeMap->createAsset(
		"resources/environmentmaps/envMap0.png "
		"resources/environmentmaps/envMap1.png "
		"resources/environmentmaps/envMap2.png "
		"resources/environmentmaps/envMap3.png "
		"resources/environmentmaps/envMap4.png "
		"resources/environmentmaps/envMap5.png");

	std::shared_ptr<d3d::Shader> ssrShader(new d3d::Shader());

	ssrShader->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/imageeffects/ssr.frag");

	std::shared_ptr<d3d::SceneObjectSSR> ssr(new d3d::SceneObjectSSR());

	getRenderScene()->add(ssr, false);

	ssr->create(blurShaderHorizontalEdgeAware, blurShaderVerticalEdgeAware, ssrShader, renderImageShader, cubeMap, noiseMap);

	ssr->_layer = 1.0f;*/

	// Light Scattering

	/*std::shared_ptr<d3d::Shader> lightScatteringShader(new d3d::Shader());

	lightScatteringShader->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/imageeffects/lightScattering.frag");

	std::shared_ptr<d3d::SceneObjectLightScattering> lightScattering(new d3d::SceneObjectLightScattering());

	getRenderScene()->add(lightScattering, false);

	lightScattering->create(blurShaderHorizontal, blurShaderVertical, lightScatteringShader, renderImageShader);

	lightScattering->_layer = 1.5f;

	lightScattering->_lightSourcePosition = -light->getDirection() * 200.0f;
	lightScattering->_lightSourceColor = d3d::Vec3f(1.0f, 0.9f, 0.8f) * 0.5f;*/

	// Depth of field
	if (pConfig->_dof) {
		std::shared_ptr<d3d::Shader> depthOfFieldBlurShaderHorizontal(new d3d::Shader());

		depthOfFieldBlurShaderHorizontal->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/imageeffects/depthOfFieldBlurHorizontal.frag");

		std::shared_ptr<d3d::Shader> depthOfFieldBlurShaderVertical(new d3d::Shader());

		depthOfFieldBlurShaderVertical->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/imageeffects/depthOfFieldBlurVertical.frag");

		std::shared_ptr<d3d::SceneObjectDepthOfField> depthOfField(new d3d::SceneObjectDepthOfField());

		getRenderScene()->addNamed(depthOfField, "dof", false);

		depthOfField->create(depthOfFieldBlurShaderHorizontal, depthOfFieldBlurShaderVertical, renderImageShader);

		depthOfField->_layer = 1.5f;

		depthOfField->_focalDistance = 9.0f;
		depthOfField->_focalRange = 0.4f;
		depthOfField->_blurRadius = 0.0017123f;
		depthOfField->_numBlurPasses = 2;
	}

	// FXAA
	if (pConfig->_fxaa) {
		std::shared_ptr<d3d::Shader> lumaShader(new d3d::Shader());

		lumaShader->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/imageeffects/lumaRender.frag");

		std::shared_ptr<d3d::Shader> fxaaShader(new d3d::Shader());

		fxaaShader->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/imageeffects/fxaa.frag");

		std::shared_ptr<d3d::SceneObjectFXAA> fxaa(new d3d::SceneObjectFXAA());

		getRenderScene()->addNamed(fxaa, "fxaa", false);

		fxaa->create(fxaaShader, lumaShader);
	}

	std::shared_ptr<SceneObjectChatBox> chatbox(new SceneObjectChatBox());
	getRenderScene()->addNamed(chatbox, "chatbox");

	destroy();
}