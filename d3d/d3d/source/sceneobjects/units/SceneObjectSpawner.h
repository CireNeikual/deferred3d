#pragma once

#include <sceneobjects/units/SceneObjectUnit.h>

class SceneObjectSpawner : public d3d::SceneObject {
private:
	d3d::SceneObjectRef _trileBatcher;
	d3d::SceneObjectRef _armyController;

	std::shared_ptr<Trile> _trile;

	int _side;
	int _dir;

	SceneObjectUnit* (*_pUnitFactory)();

public:
	d3d::Vec3f _position;

	bool _placed;

	SceneObjectSpawner()
		: _placed(false)
	{
		_renderMask = 0xffff;
	}

	void create(const std::shared_ptr<Trile> trile, class SceneObjectArmyController* pArmyController, SceneObjectUnit* (*pUnitFactory)());

	// Inherited from SceneObject
	void onAdd();
	void deferredRender();

	void calculateAABB();

	d3d::SceneObjectRef spawn(raahn::RAAHN* pParent1, raahn::RAAHN* pParent2, unsigned long seed);

	SceneObjectSpawner* copyFactory() {
		return new SceneObjectSpawner(*this);
	}
};