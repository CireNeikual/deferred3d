#pragma once

#include <sceneobjects/units/SceneObjectUnit.h>

#include <SFML/Audio.hpp>

class SceneObjectUnitMediumTank : public SceneObjectUnit {
private:
	std::vector<std::shared_ptr<Trile>> _muzzleFlashTrites;

	int _fireAnimationState;
	float _fireAnimationCycleTime;
	float _fireAnimationCycleTimer;

	std::shared_ptr<sf::Sound> _soundSource;

	d3d::SceneObjectRef _muzzleFlashLight;

public:
	SceneObjectUnitMediumTank();

	// Inherited from SceneObjectUnit
	void unitOnDestroy();
	void unitOnAdd();
	void unitUpdate(float dt);
	void unitSynchronousUpdate(float dt);
	void unitAttack(SceneObjectUnit* pEnemy);
	void unitRender();

	static SceneObjectUnit* unitFactory() {
		return new SceneObjectUnitMediumTank();
	}

	SceneObjectUnitMediumTank* copyFactory() {
		return new SceneObjectUnitMediumTank(*this);
	}
};