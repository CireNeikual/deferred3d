#include <sceneobjects/units/SceneObjectUnitMediumTank.h>

#include <rendering/lighting/SceneObjectPointLight.h>

#include <sceneobjects/sound/SoundBuffer.h>

SceneObjectUnitMediumTank::SceneObjectUnitMediumTank()
: _fireAnimationState(-1), _fireAnimationCycleTime(0.025f), _fireAnimationCycleTimer(0.0f)
{
	_trileFileName = "resources/trixel/units/tier2/mediumTank.trile resources/trixel/units/tier2/mediumTank.png";
	_chunkFileName = "resources/trixel/units/tier2/mediumTank.chunk";

	_unitName = "M Tank";

	_unitTypeInput[0] = 0.0f;
	_unitTypeInput[1] = 0.0f;
	_unitTypeInput[2] = 0.0f;

	_soundSource.reset(new sf::Sound());
}

void SceneObjectUnitMediumTank::unitOnAdd() {
	const size_t numMuzzleFlashTrites = 7;

	_muzzleFlashTrites.resize(numMuzzleFlashTrites);

	std::shared_ptr<d3d::Asset> asset;

	for (size_t i = 0; i < numMuzzleFlashTrites; i++) {
		getScene()->getAssetManager("trile", Trile::assetFactory)->getAsset("resources/trixel/animations/muzzleFlash1/muzzleFlash" + std::to_string(i + 1) + ".trile" +
			" resources/trixel/animations/muzzleFlash1/muzzleFlash" + std::to_string(i + 1) + ".png", asset);

		_muzzleFlashTrites[i] = std::static_pointer_cast<Trile>(asset);
	}

	getScene()->getAssetManager("sound", d3d::SoundBuffer::assetFactory)->getAsset("resources/sounds/units/blast.wav", asset);

	std::shared_ptr<d3d::SoundBuffer> blastSound = std::static_pointer_cast<d3d::SoundBuffer>(asset);

	blastSound->setBuffer(*_soundSource);

	_soundSource->setAttenuation(1.0f);
	_soundSource->setMinDistance(4.0f);

	d3d::SceneObjectLighting* pLighting = static_cast<d3d::SceneObjectLighting*>(getScene()->getNamedCheckQueue("lighting").get());

	std::shared_ptr<d3d::SceneObjectPointLight> pointLight(new d3d::SceneObjectPointLight());

	getScene()->add(pointLight, false);

	pointLight->create(pLighting);

	pointLight->setColor(d3d::Vec3f(1.0f, 0.8f, 0.6f));
	pointLight->setRange(0.75f);
	pointLight->setPosition(getPosition());
	pointLight->_enabled = false;

	_muzzleFlashLight = pointLight.get();
}

void SceneObjectUnitMediumTank::unitOnDestroy() {
	if (_muzzleFlashLight.isAlive())
		_muzzleFlashLight->destroy();
}

void SceneObjectUnitMediumTank::unitUpdate(float dt) {
	if (_fireAnimationState != -1) {
		if (_fireAnimationCycleTimer < _fireAnimationCycleTime)
			_fireAnimationCycleTimer += dt;
		else {
			_fireAnimationCycleTimer -= _fireAnimationCycleTime;

			_fireAnimationState++;

			if (_fireAnimationState >= _muzzleFlashTrites.size())
				_fireAnimationState = -1;
		}
	}
}

void SceneObjectUnitMediumTank::unitSynchronousUpdate(float dt) {
	_soundSource->setPosition(sf::Vector3f(getPosition().x, getPosition().y, getPosition().z));

	assert(_muzzleFlashLight.isAlive());

	d3d::SceneObjectPointLight* pPointLight = static_cast<d3d::SceneObjectPointLight*>(_muzzleFlashLight.get());

	if (_fireAnimationState != -1) {
		pPointLight->setPosition(getPosition() + getRotation() * d3d::Vec3f(-0.2f, 0.0f, 0.0f));

		pPointLight->_enabled = true;
	}
	else
		pPointLight->_enabled = false;
}

void SceneObjectUnitMediumTank::unitAttack(SceneObjectUnit* pEnemy) {
	d3d::Vec3f forward = getRotation() * d3d::Vec3f(-1.0f, 0.0f, 0.0f);

	_fireAnimationState = 0;

	_fireAnimationCycleTimer = 0.0f;

	_pRigidBody->applyCentralForce(bt(-forward * 4500.0f));

	pEnemy->_hp -= _damage;

	std::uniform_real_distribution<float> distPitch(0.5f, 1.5f);

	_soundSource->setPitch(distPitch(getScene()->_randomGenerator));
	_soundSource->play();
}

void SceneObjectUnitMediumTank::unitRender() {
	if (_fireAnimationState != -1) {
		static_cast<TrileBatcher*>(_trileBatcher.get())->add(_muzzleFlashTrites[_fireAnimationState].get(), _fullTransform * d3d::Matrix4x4f::translateMatrix(d3d::Vec3f(-0.47f, -0.3f, 0.33f)), 1.0f);
	}
}