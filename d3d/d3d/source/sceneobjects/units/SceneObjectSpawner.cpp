#include <sceneobjects/units/SceneObjectSpawner.h>

#include <sceneobjects/units/SceneObjectArmyController.h>

#include <util/Math.h>

void SceneObjectSpawner::create(const std::shared_ptr<Trile> trile, SceneObjectArmyController* pArmyController, SceneObjectUnit* (*pUnitFactory)()) {
	_trile = trile;
	_pUnitFactory = pUnitFactory;
	_armyController = pArmyController;
	_side = pArmyController->getSide();

	_position = d3d::Vec3f(999.0f, 999.0f, 999.0f);

	calculateAABB();

	_dir = _side * 2;

	_tag = "spawner";
}

void SceneObjectSpawner::onAdd() {
	_trileBatcher = getScene()->getNamed("trileBatcher");

	assert(_trileBatcher.isAlive());
}

void SceneObjectSpawner::deferredRender() {
	assert(_trileBatcher.isAlive());

	static_cast<TrileBatcher*>(_trileBatcher.get())->add(_trile.get(), d3d::Matrix4x4f::translateMatrix(_position) * d3d::Matrix4x4f::rotateMatrixY(_dir * d3d::_piOver2));
}

void SceneObjectSpawner::calculateAABB() {
	_aabb = _trile->getMinBounds().getTransformedAABB(d3d::Matrix4x4f::translateMatrix(_position) * d3d::Matrix4x4f::rotateMatrixY(_dir * d3d::_piOver2));

	updateAABB();
}

d3d::SceneObjectRef SceneObjectSpawner::spawn(raahn::RAAHN* pParent1, raahn::RAAHN* pParent2, unsigned long seed) {
	assert(_placed);

	assert(getScene() != nullptr);

	assert(_pUnitFactory != nullptr);

	std::shared_ptr<SceneObjectUnit> unit(_pUnitFactory());

	getScene()->add(unit, true);

	unit->createBaseUnit(static_cast<SceneObjectArmyController*>(_armyController.get()), pParent1, pParent2, _side, seed);

	unit->_spawnPosition = _position - d3d::Vec3f(0.0f, 0.5f, 0.0f);

	unit->_spawnRotation = _side == 0 ? d3d::Quaternion(d3d::_pi, d3d::Vec3f(0.0f, 1.0f, 0.0f)) : d3d::Quaternion(0.0f, d3d::Vec3f(0.0f, 1.0f, 0.0f));

	return unit.get();
}