#pragma once

#include <sceneobjects/units/SceneObjectUnit.h>

class SceneObjectArmyController : public d3d::SceneObject {
public:
	struct Wave {
		std::vector<std::shared_ptr<raahn::RAAHN>> _brains;
		std::vector<float> _rewards;
		std::vector<d3d::SceneObjectRef> _units;
	};

private:
	struct FitnessAndIndex {
		float _fitness;
		size_t _index;

		static bool compare(const FitnessAndIndex &fai1, const FitnessAndIndex &fai2) {
			return fai1._fitness < fai2._fitness;
		}
	};

	d3d::SceneObjectRef _battlefield;

	int _side;

	std::vector<d3d::SceneObjectRef> _spawners;

	unsigned long _seed;

	std::mt19937 _armyGenerator;

public:
	size_t _numElites;

	float _fitnessOffset;

	float _fitnessMultiplier;
	float _greedExponent;

	std::vector<Wave> _waves;

	SceneObjectArmyController()
		: _seed(0), _numElites(1), _fitnessOffset(0.5f), _fitnessMultiplier(0.5f), _greedExponent(1.5f)
	{}

	void create(class SceneObjectBattlefieldController* pBattlefieldController, int side, unsigned long seed);

	void addSpawner(class SceneObjectSpawner* pSpawner);

	int getSide() const {
		return _side;
	}

	class SceneObjectBattlefieldController* getBattlefieldController() const;

	// Inherited from SceneObject
	void onAdd();
	void update(float dt);

	void spawn();

	SceneObjectArmyController* copyFactory() {
		return new SceneObjectArmyController(*this);
	}

	friend class SceneObjectBattlefieldController;
};