#include <sceneobjects/units/SceneObjectArmyController.h>

#include <sceneobjects/units/SceneObjectBattlefieldController.h>

#include <sceneobjects/units/SceneObjectSpawner.h>

void SceneObjectArmyController::create(SceneObjectBattlefieldController* pBattlefieldController, int side, unsigned long seed) {
	_battlefield = pBattlefieldController;

	_side = side;

	_seed = seed;
}

void SceneObjectArmyController::addSpawner(SceneObjectSpawner* pSpawner) {
	_spawners.push_back(d3d::SceneObjectRef(pSpawner));
}

void SceneObjectArmyController::onAdd() {

}

void SceneObjectArmyController::update(float dt) {
	// Grab rewards
	for (size_t i = 0; i < _waves.size(); i++)
	for (size_t j = 0; j < _waves[i]._units.size(); j++)
	if (_waves[i]._units[j].isAlive())
		_waves[i]._rewards[j] = static_cast<SceneObjectUnit*>(_waves[i]._units[j].get())->_reward;
}

SceneObjectBattlefieldController* SceneObjectArmyController::getBattlefieldController() const {
	return static_cast<SceneObjectBattlefieldController*>(_battlefield.get());
}

void SceneObjectArmyController::spawn() {
	std::uniform_real_distribution<float> uniformDist(0.0f, 1.0f);

	_waves.insert(_waves.begin(), Wave());

	if (_waves.size() == 1 || _waves[1]._units.empty()) { // If no wave to reproduce from
		for (size_t i = 0; i < _spawners.size(); i++) {
			SceneObjectSpawner* pSpawner = static_cast<SceneObjectSpawner*>(_spawners[i].get());

			if (pSpawner->_placed) {
				_waves[0]._units.push_back(pSpawner->spawn(nullptr, nullptr, _seed));

				_waves[0]._brains.push_back(static_cast<SceneObjectUnit*>(_waves[0]._units.back().get())->_brain);

				_waves[0]._rewards.push_back(0.0f);

				_seed++;
			}
		}
	}
	else { // If there is a wave to reproduce from, take the nearest one
		size_t waveIndex = 1;
	
		float min = _waves[waveIndex]._rewards[0];

		for (size_t i = 1; i < _waves[waveIndex]._units.size(); i++) {
			float r = _waves[waveIndex]._rewards[i];

			if (r < min)
				min = r;
		}

		std::vector<FitnessAndIndex> fitnesses(_waves[waveIndex]._units.size());

		float sum = 0.0f;

		for (size_t i = 0; i < _waves[waveIndex]._units.size(); i++) {
			float r = _waves[waveIndex]._rewards[i];

			fitnesses[i]._fitness = std::powf((r - min + _fitnessOffset) * _fitnessMultiplier, _greedExponent);

			fitnesses[i]._index = i;

			sum += fitnesses[i]._fitness;
		}

		std::sort(fitnesses.begin(), fitnesses.end(), FitnessAndIndex::compare);

		size_t numElitesFetched = 0;

		for (size_t i = 0; i < _spawners.size(); i++) {
			if (numElitesFetched < _numElites) {
	
				size_t fetchIndex = fitnesses[static_cast<size_t>(std::max(0, static_cast<int>(fitnesses.size() - 1 - numElitesFetched)))]._index;

				_waves[0]._units.push_back(static_cast<SceneObjectSpawner*>(_spawners[i].get())->spawn(_waves[waveIndex]._brains[fetchIndex].get(), nullptr, _seed));

				_waves[0]._brains.push_back(static_cast<SceneObjectUnit*>(_waves[0]._units.back().get())->_brain);

				_waves[0]._rewards.push_back(0.0f);

				numElitesFetched++;

				//if (numElitesFetched == 1)
				//	_waves[0]._brains.back()->saveToFile("defaultBrain2.lstmrl");
			}
			else {
				// Roulette wheel
				float randomCusp1 = sum * uniformDist(_armyGenerator);

				float sumSoFar = 0.0f;

				size_t index1 = 0;

				for (size_t j = 0; j < fitnesses.size(); j++) {
					sumSoFar += fitnesses[j]._fitness;

					if (sumSoFar >= randomCusp1) {
						index1 = fitnesses[j]._index;

						break;
					}
				}

				float randomCusp2 = sum * uniformDist(_armyGenerator);

				sumSoFar = 0.0f;

				size_t index2 = 0;

				for (size_t j = 0; j < fitnesses.size(); j++) {
					sumSoFar += fitnesses[j]._fitness;

					if (sumSoFar >= randomCusp2) {
						index2 = fitnesses[j]._index;

						break;
					}
				}

				SceneObjectSpawner* pSpawner = static_cast<SceneObjectSpawner*>(_spawners[i].get());

				if (pSpawner->_placed) {
					_waves[0]._units.push_back(pSpawner->spawn(_waves[waveIndex]._brains[index1].get(), _waves[waveIndex]._brains[index2].get(), _seed));

					_waves[0]._brains.push_back(static_cast<SceneObjectUnit*>(_waves[0]._units.back().get())->_brain);

					_waves[0]._rewards.push_back(0.0f);
				}
			}

			_seed++;
		}
	}

	while (_waves.size() > 2)
		_waves.pop_back();
}