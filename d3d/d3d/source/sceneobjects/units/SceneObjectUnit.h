#pragma once

#include <scene/RenderScene.h>
#include <sceneobjects/trixel/TrileMap.h>

#include <rendering/model/StaticModelOBJ.h>

#include <raahn/RAAHN.h>

class SceneObjectUnit : public d3d::SceneObject {
protected:
	d3d::SceneObjectRef _trileBatcher;
	d3d::SceneObjectRef _armyController;

	std::shared_ptr<d3d::SceneObjectRef> _pRefThis;

	std::shared_ptr<Trile> _trile;
	std::shared_ptr<TrileVoxelData> _chunk;

	d3d::SceneObjectRef _batcherRef;

	// Physics
	d3d::SceneObjectRef _physicsWorld;

	std::shared_ptr<btBoxShape> _pCollisionShape;
	std::shared_ptr<btDefaultMotionState> _pMotionState;
	std::shared_ptr<btRigidBody> _pRigidBody;

	// AI command
	float _leftPower, _rightPower;
	bool _attack;

	float _fireTimer;

	// AI
	std::shared_ptr<raahn::RAAHN> _brain;

	int _side;

	unsigned long _frames;

	float _age;

	std::mt19937 _generator;

	std::string _trileFileName;
	std::string _chunkFileName;

	std::string _unitName;

	d3d::Matrix4x4f _offsetTransform;
	d3d::Matrix4x4f _fullTransform;

	d3d::Vec3f _feelers;

	float _prevReward;

	float _friction;
	float _mass;

	std::array<float, 3> _unitTypeInput;

	bool _kill;

	d3d::SceneObjectRef _attackEnemy;

	void explode(float strength);

	virtual void unitOnDestroy() {}
	virtual void unitOnAdd() {}
	virtual void unitUpdate(float dt) {}
	virtual void unitSynchronousUpdate(float dt) {}
	virtual void unitAttack(SceneObjectUnit* pEnemy) {}
	virtual void unitRender() {}

public:
	float _turnRate;
	float _driveRate;
	float _hp;
	float _armor;
	float _damage;
	float _rateOfFire;
	float _attackRange;
	float _visionRange;
	float _rotationTolerance;
	float _maxSpeed;
	float _lifeSpan;
	float _treadSideOffsetMultiplier;
	float _linearDamping;
	float _angularDamping;

	float _floorOffset;

	float _reward;
	float _lowPassReward;

	d3d::Vec3f _spawnPosition;
	d3d::Quaternion _spawnRotation;

	SceneObjectUnit();
	virtual ~SceneObjectUnit() {}

	void createBaseUnit(class SceneObjectArmyController* pArmyController, raahn::RAAHN* pParent1, raahn::RAAHN* pParent2, int side, unsigned long seed);

	void onAdd();
	void onDestroy();
	void update(float dt);
	void synchronousUpdate(float dt);
	void deferredRender();

	float getHP() const {
		return _hp;
	}

	float getArmor() const {
		return _armor;
	}

	float getDamage() const {
		return _damage;
	}

	float getRateOfFire() const {
		return _rateOfFire;
	}

	float getVisionRange() const {
		return _visionRange;
	}

	float getAttackRange() const {
		return _attackRange;
	}

	int getSide() const {
		return _side;
	}

	d3d::Vec3f getPosition() const {
		return cons(_pRigidBody->getWorldTransform().getOrigin());
	}

	d3d::Vec3f getLinearVelocity() const {
		return cons(_pRigidBody->getLinearVelocity());
	}

	d3d::Quaternion getRotation() const {
		return cons(_pRigidBody->getWorldTransform().getRotation());
	}

	d3d::Vec3f getHalfDims() const {
		return cons(_pCollisionShape->getHalfExtentsWithoutMargin());
	}

	const std::string &getUnitName() const {
		return _unitName;
	}

	bool created() const {
		return _pRigidBody != nullptr;
	}

	friend class SceneObjectArmyController;
	friend class SceneObjectSpawner;
};