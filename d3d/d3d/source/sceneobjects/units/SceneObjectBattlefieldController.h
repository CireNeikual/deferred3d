#pragma once

#include <sceneobjects/units/SceneObjectUnit.h>

class SceneObjectBattlefieldController : public d3d::SceneObject {
private:
	std::shared_ptr<TrileMap> _map;

	float _fieldLevel;
	float _laneWidth;

	d3d::SceneObjectRef _side1;
	d3d::SceneObjectRef _side2;

public:
	std::mt19937 _generator;
	
	float _waveTimer;
	float _waveTime;

	SceneObjectBattlefieldController()
		: _waveTimer(0.0f), _waveTime(20.0f)
	{}

	void create(const std::shared_ptr<TrileMap> &map, int fieldLevel, int laneWidth, unsigned long seed);

	d3d::Vec3f getTargetForSide(int side) const;

	float getFieldLevel() const {
		return _fieldLevel;
	}

	float getLaneWidth() const {
		return _laneWidth;
	}

	// Inherited from SceneObject
	void onAdd();
	void synchronousUpdate(float dt);

	class SceneObjectArmyController* getSide(int side) const;

	SceneObjectBattlefieldController* copyFactory() {
		return new SceneObjectBattlefieldController(*this);
	}

	friend class SceneObjectUnit;
};