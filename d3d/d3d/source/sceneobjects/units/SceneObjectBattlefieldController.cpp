#include <sceneobjects/units/SceneObjectBattlefieldController.h>

#include <sceneobjects/units/SceneObjectArmyController.h>

void SceneObjectBattlefieldController::create(const std::shared_ptr<TrileMap> &map, int fieldLevel, int laneWidth, unsigned long seed) {
	_map = map;

	_fieldLevel = fieldLevel;
	_laneWidth = laneWidth;

	// Add sides
	std::shared_ptr<SceneObjectArmyController> side1(new SceneObjectArmyController());

	getScene()->add(side1, false);

	_side1 = side1.get();

	std::shared_ptr<SceneObjectArmyController> side2(new SceneObjectArmyController());

	getScene()->add(side2, false);

	_side2 = side2.get();

	side1->create(this, 0, seed);
	side2->create(this, 1, seed);
}

void SceneObjectBattlefieldController::onAdd() {

}

void SceneObjectBattlefieldController::synchronousUpdate(float dt) {
	SceneObjectArmyController* pSide1 = static_cast<SceneObjectArmyController*>(_side1.get());
	SceneObjectArmyController* pSide2 = static_cast<SceneObjectArmyController*>(_side2.get());

	_waveTimer += dt;

	if (_waveTimer > _waveTime) {
		_waveTimer -= _waveTime;

		// Spawn new wave
		pSide1->spawn();
		pSide2->spawn();
	}
}

d3d::Vec3f SceneObjectBattlefieldController::getTargetForSide(int side) const {
	switch (side) {
	case 0:
		return d3d::Vec3f(30.0f, static_cast<float>(_fieldLevel), 0.0f);
	case 1:
		return d3d::Vec3f(-30.0f, static_cast<float>(_fieldLevel), 0.0f);
	}

	return d3d::Vec3f(0.0f, 0.0f, 0.0f);
}

SceneObjectArmyController* SceneObjectBattlefieldController::getSide(int side) const {
	switch (side) {
	case 0:
		return static_cast<SceneObjectArmyController*>(_side1.get());
	case 1:
		return static_cast<SceneObjectArmyController*>(_side2.get());
	}

	return nullptr;
}
