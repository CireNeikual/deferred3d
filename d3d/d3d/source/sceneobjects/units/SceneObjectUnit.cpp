#include <sceneobjects/units/SceneObjectUnit.h>

#include <sceneobjects/units/SceneObjectArmyController.h>
#include <sceneobjects/units/SceneObjectBattlefieldController.h>
#include <sceneobjects/trixel/TrileExplosion.h>

#include <rendering/model/SceneObjectStaticModelBatcher.h>

#include <util/Math.h>

class UnitDistanceComparer {
public:
	SceneObjectUnit* _pTarget;

	bool operator()(SceneObjectUnit* pLeft, SceneObjectUnit* pRight) {
		return (_pTarget->getPosition() - pLeft->getPosition()).magnitude() < (_pTarget->getPosition() - pRight->getPosition()).magnitude();
	}
};

class IgnoreBodyCast : public btCollisionWorld::ClosestRayResultCallback {
private:
	btRigidBody* _pBody;

public:
	IgnoreBodyCast(btRigidBody* pBody)
		: btCollisionWorld::ClosestRayResultCallback(btVector3(0.0, 0.0, 0.0), btVector3(0.0, 0.0, 0.0)),
		_pBody(pBody)
	{}

	btScalar addSingleResult(btCollisionWorld::LocalRayResult &rayResult, bool normalInWorldSpace) {
		if (rayResult.m_collisionObject == _pBody)
			return 1.0f;

		return ClosestRayResultCallback::addSingleResult(rayResult, normalInWorldSpace);
	}
};

SceneObjectUnit::SceneObjectUnit()
: _leftPower(0.0f), _rightPower(0.0f), _attack(false), _spawnPosition(0.0f, 0.0f, 0.0f), _spawnRotation(d3d::Quaternion::identityMult()),
_friction(3.0f), _mass(20.0f), _turnRate(400.0f), _driveRate(600.0f), _hp(100.0f), _armor(1.0f), _damage(40.0f), _rateOfFire(1.5f), _attackRange(3.0f), _visionRange(4.0f),
_side(0), _rotationTolerance(0.1f), _fireTimer(0.0f), _maxSpeed(1.2f), _lifeSpan(17.0f), _kill(false), _floorOffset(0.1f), _treadSideOffsetMultiplier(0.85f),
_linearDamping(1.8f), _angularDamping(1.8f), _offsetTransform(d3d::Matrix4x4f::identityMatrix()), _reward(0.0f), _prevReward(0.0f), _lowPassReward(0.0f), _feelers(4.0f, 4.0f, 4.0f)
{
	_renderMask = 0xffff;

	_tag = "unit";
}

void SceneObjectUnit::createBaseUnit(SceneObjectArmyController* pArmyController, raahn::RAAHN* pParent1, raahn::RAAHN* pParent2, int side, unsigned long seed) {
	assert(getScene() != nullptr);

	_armyController = pArmyController;
	_side = side;

	_generator.seed(seed);

	// Inputs:
	// Vector enemy base
	// Vector in facing direction
	// Vector local velocity (3 components, last is angular velocity around Y)
	// Feelers (x3)
	// Closest friendly vectors (x2)
	// Closest enemy vector (x2)
	// Enemies in attacking range (0, 1, 2)
	// Unit type indicator (3 values)
	// Total number of inputs: 23

	// Outputs:
	// Left power (1 unit)
	// Right power (1 unit)
	// Should attack
	// Attack index
	// Total number of outputs: 4

	_brain.reset(new raahn::RAAHN());

	if (pParent1 == nullptr && pParent2 == nullptr)
		_brain->createRandom(23, 8, 3, 2, 1, 12, -0.5f, 0.5f, _generator);
		//_brain->createFromFile("resources/brains/defaultBrain.lstmrl");
	else if (pParent1 != nullptr && pParent2 != nullptr) {
		_brain->createFromParents(*pParent1, *pParent2, 0.1f, _generator);
		_brain->mutate(0.08f, 0.6f, _generator);
	}
	else if (pParent1 != nullptr) { // Elite
		*_brain = *pParent1;
		//_brain->mutate(0.08f, 0.85f, _generator);
	}
	else { // Elite
		*_brain = *pParent2;
		//_brain->mutate(0.08f, 0.85f, _generator);
	}
}

void SceneObjectUnit::onAdd() {
	_pRefThis.reset(new d3d::SceneObjectRef(this));

	_trileBatcher = getScene()->getNamed("trileBatcher");

	assert(_trileBatcher.isAlive());

	_physicsWorld = getScene()->getNamed("physWrld");

	assert(_physicsWorld.isAlive());

	std::shared_ptr<d3d::Asset> asset;

	// Load trile
	
	if (!getScene()->getAssetManager("trile", Trile::assetFactory)->getAsset(_trileFileName, asset))
		abort();

	_trile = std::static_pointer_cast<Trile>(asset);

	if (!getScene()->getAssetManager("chunk", TrileVoxelData::assetFactory)->getAsset(_chunkFileName, asset))
		abort();

	_chunk = std::static_pointer_cast<TrileVoxelData>(asset);

	_batcherRef = getScene()->getNamed("smb");

	assert(_batcherRef.isAlive());

	// Create physics
	d3d::SceneObjectPhysicsWorld* pPhysicsWorld = static_cast<d3d::SceneObjectPhysicsWorld*>(_physicsWorld.get());

	_pCollisionShape.reset(new btBoxShape(bt(_trile->getMinBounds().getHalfDims())));

	_pMotionState.reset(new btDefaultMotionState(btTransform(bt(_spawnRotation), bt(_spawnPosition + d3d::Vec3f(0.0f, _trile->getMinBounds().getHalfDims().y, 0.0f)))));

	btVector3 inertia;
	_pCollisionShape->calculateLocalInertia(_mass, inertia);

	btRigidBody::btRigidBodyConstructionInfo rigidBodyCI(_mass, _pMotionState.get(), _pCollisionShape.get(), inertia);

	rigidBodyCI.m_restitution = 0.0f;
	rigidBodyCI.m_friction = _friction;

	_pRigidBody.reset(new btRigidBody(rigidBodyCI));
	_pRigidBody->setUserPointer(_pRefThis.get());

	pPhysicsWorld->_pDynamicsWorld->addRigidBody(_pRigidBody.get());

	_pRigidBody->setAngularFactor(btVector3(0.0f, 1.0f, 0.0f));

	_pRigidBody->setDamping(_linearDamping, _angularDamping);

	_pRigidBody->setActivationState(DISABLE_DEACTIVATION);

	unitOnAdd();
}

void SceneObjectUnit::onDestroy() {
	if (_physicsWorld.isAlive() && _pRigidBody != nullptr) {
		d3d::SceneObjectPhysicsWorld* pPhysicsWorld = static_cast<d3d::SceneObjectPhysicsWorld*>(_physicsWorld.get());

		pPhysicsWorld->_pDynamicsWorld->removeRigidBody(_pRigidBody.get());
	}

	unitOnDestroy();
}

void SceneObjectUnit::update(float dt) {
	d3d::SceneObjectPhysicsWorld* pPhysicsWorld = static_cast<d3d::SceneObjectPhysicsWorld*>(_physicsWorld.get());

	std::vector<d3d::SceneObjectRef> queryResult;
	std::vector<SceneObjectUnit*> friends;
	std::vector<SceneObjectUnit*> enemies;

	getScene()->_octree.queryRegion(d3d::AABB3D(cons(_pRigidBody->getWorldTransform().getOrigin() - btVector3(_visionRange, _visionRange, _visionRange)), cons(_pRigidBody->getWorldTransform().getOrigin() + btVector3(_visionRange, _visionRange, _visionRange))), queryResult);

	for (size_t i = 0; i < queryResult.size(); i++) {
		SceneObjectUnit* pUnit = dynamic_cast<SceneObjectUnit*>(queryResult[i].get());

		if (pUnit != nullptr) {
			if (pUnit->getSide() == getSide())
				friends.push_back(pUnit);
			else
				enemies.push_back(pUnit);
		}
	}

	UnitDistanceComparer comparer;

	comparer._pTarget = this;

	std::sort(friends.begin(), friends.end(), comparer);
	std::sort(enemies.begin(), enemies.end(), comparer);

	// Update feelers
	{
		IgnoreBodyCast callback(_pRigidBody.get());

		d3d::Vec3f from = cons(_pRigidBody->getWorldTransform().getOrigin());
		d3d::Vec3f to = from + ((cons(_pRigidBody->getWorldTransform().getRotation()) * d3d::Quaternion(-d3d::_piOver4, d3d::Vec3f(0.0f, 1.0f, 0.0f)))) * d3d::Vec3f(_visionRange, 0.0f, 0.0f);

		pPhysicsWorld->_pDynamicsWorld->rayTest(bt(from), bt(to), callback);
		
		if (callback.hasHit())
			_feelers.x = callback.m_closestHitFraction;
		else
			_feelers.x = _visionRange;
	}

	{
		IgnoreBodyCast callback(_pRigidBody.get());

		d3d::Vec3f from = cons(_pRigidBody->getWorldTransform().getOrigin());
		d3d::Vec3f to = from + ((cons(_pRigidBody->getWorldTransform().getRotation()) * d3d::Quaternion(0.0f, d3d::Vec3f(0.0f, 1.0f, 0.0f)))) * d3d::Vec3f(_visionRange, 0.0f, 0.0f);

		pPhysicsWorld->_pDynamicsWorld->rayTest(bt(from), bt(to), callback);

		if (callback.hasHit())
			_feelers.y = callback.m_closestHitFraction;
		else
			_feelers.y = _visionRange;
	}

	{
		IgnoreBodyCast callback(_pRigidBody.get());

		d3d::Vec3f from = cons(_pRigidBody->getWorldTransform().getOrigin());
		d3d::Vec3f to = from + ((cons(_pRigidBody->getWorldTransform().getRotation()) * d3d::Quaternion(d3d::_piOver4, d3d::Vec3f(0.0f, 1.0f, 0.0f)))) * d3d::Vec3f(_visionRange, 0.0f, 0.0f);

		pPhysicsWorld->_pDynamicsWorld->rayTest(bt(from), bt(to), callback);

		if (callback.hasHit())
			_feelers.z = callback.m_closestHitFraction;
		else
			_feelers.z = _visionRange;
	}

	SceneObjectArmyController* pArmyController = static_cast<SceneObjectArmyController*>(_armyController.get());

	// Set inputs
	d3d::Vec3f target(pArmyController->getBattlefieldController()->getTargetForSide(_side).normalized());
	d3d::Vec3f facingDirection(cons(_pRigidBody->getWorldTransform().getRotation()) * d3d::Vec3f(1.0f, 0.0f, 0.0f));
	d3d::Vec3f velocities(_pRigidBody->getLinearVelocity().getX(), _pRigidBody->getLinearVelocity().getZ(), _pRigidBody->getAngularVelocity().getY());
	d3d::Vec3f closestFriend1(friends.size() > 0 ? friends[0]->getPosition() - getPosition() : d3d::Vec3f(0.0f, 0.0f, 0.0f));
	d3d::Vec3f closestFriend2(friends.size() > 1 ? friends[1]->getPosition() - getPosition() : d3d::Vec3f(0.0f, 0.0f, 0.0f));
	d3d::Vec3f closestEnemy1(enemies.size() > 0 ? enemies[0]->getPosition() - getPosition() : d3d::Vec3f(0.0f, 0.0f, 0.0f));
	d3d::Vec3f closestEnemy2(enemies.size() > 1 ? enemies[1]->getPosition() - getPosition() : d3d::Vec3f(0.0f, 0.0f, 0.0f));

	_brain->setInput(0, target.x * 4.0f);
	_brain->setInput(1, target.z * 4.0f);
	_brain->setInput(2, facingDirection.x * 4.0f);
	_brain->setInput(3, facingDirection.z * 4.0f);
	_brain->setInput(5, velocities.x * 1.0f);
	_brain->setInput(6, velocities.y * 1.0f);
	_brain->setInput(7, velocities.z * 1.0f);
	_brain->setInput(8, _feelers.x * 1.0f);
	_brain->setInput(9, _feelers.y * 1.0f);
	_brain->setInput(10, _feelers.z * 1.0f);
	_brain->setInput(11, closestFriend1.x * 0.5f);
	_brain->setInput(12, closestFriend1.z * 0.5f);
	_brain->setInput(13, closestFriend2.x * 0.3f);
	_brain->setInput(14, closestFriend2.z * 0.3f);
	_brain->setInput(15, closestEnemy1.x * 0.5f);
	_brain->setInput(16, closestEnemy1.z * 0.5f);
	_brain->setInput(17, closestEnemy2.x * 0.3f);
	_brain->setInput(18, closestEnemy2.z * 0.3f);

	int numEnemiesInRange = 0;

	if (enemies.size() > 0 && (enemies[0]->getPosition() - getPosition()).magnitude() < _attackRange)
		numEnemiesInRange++;

	if (enemies.size() > 1 && (enemies[1]->getPosition() - getPosition()).magnitude() < _attackRange)
		numEnemiesInRange++;

	_brain->setInput(19, static_cast<float>(numEnemiesInRange) * 3.0f); // Exaggerate input a bit, it's an important input!

	for (int i = 0; i < _unitTypeInput.size(); i++)
		_brain->setInput(20 + i, _unitTypeInput[i]);

	float r = _pRigidBody->getAngularVelocity().getY();

	float deltaReward = r - _lowPassReward;

	_brain->update(0.01f, deltaReward * 0.1f, 0.01f, 0.01f, 0.1f, _generator);
	
	_lowPassReward += (r - _lowPassReward) * 2.0f * dt;

	_prevReward = r;

	_leftPower = std::min(1.0f, std::max(-1.0f, _brain->getOutput(0) * 2.0f - 1.0f));
	_rightPower = std::min(1.0f, std::max(-1.0f, _brain->getOutput(1) * 2.0f - 1.0f));
	_attack = _brain->getOutput(2) > 0.4f;
	
	// Ray cast, ignore rigid body
	IgnoreBodyCast rayCallBackBottom(_pRigidBody.get());

	pPhysicsWorld->_pDynamicsWorld->rayTest(_pRigidBody->getWorldTransform().getOrigin(), _pRigidBody->getWorldTransform().getOrigin() - btVector3(0.0f, _trile->getMinBounds().getHalfDims().y + _floorOffset, 0.0f), rayCallBackBottom);

	bool onGround = false;

	// Bump up if hit
	if (rayCallBackBottom.hasHit()) {
		float previousY = _pRigidBody->getWorldTransform().getOrigin().getY();

		if (_pRigidBody->getLinearVelocity().getY() < 0.01f) {
			_pRigidBody->getWorldTransform().getOrigin().setY(previousY + (_trile->getMinBounds().getHalfDims().y + _floorOffset) * (1.0f - rayCallBackBottom.m_closestHitFraction));

			d3d::Vec3f vel(cons(_pRigidBody->getLinearVelocity()));

			// Cancel velocity across normal
			d3d::Vec3f floorNormal = cons(rayCallBackBottom.m_hitNormalWorld);

			d3d::Vec3f velInNormalDir(vel.project(floorNormal));

			// Apply correction
			_pRigidBody->setLinearVelocity(_pRigidBody->getLinearVelocity() - bt(velInNormalDir * 1.05f));

			onGround = true;
		}
	}

	btVector3 forward = bt(cons(_pRigidBody->getWorldTransform().getRotation()) * d3d::Vec3f(1.0f, 0.0f, 0.0f));

	bool drive = true;

	if (_attack && !enemies.empty()) {
		// Face selected enemy (one of two closest)
		SceneObjectUnit* pEnemy;

		if (enemies.size() == 1)
			pEnemy = enemies[0];
		else {
			if (_brain->getOutput(3) > 0.33f)
				pEnemy = (getPosition() - enemies[0]->getPosition()).magnitude() < (getPosition() - enemies[1]->getPosition()).magnitude() ? enemies[0] : enemies[1];
			else
				pEnemy = _brain->getOutput(3) > 0.66f ? enemies[0] : enemies[1];
		}

		// If enemy is in range to attack
		if ((pEnemy->getPosition() - getPosition()).magnitude() < _attackRange) {
			drive = false;

			// Face towards enemy
			// Rotate if not facing location
			d3d::Vec3f toLocation3D = -(pEnemy->getPosition() - getPosition()).normalized();
			d3d::Vec2f toLocation = d3d::Vec2f(toLocation3D.x, toLocation3D.z).normalized();

			d3d::Vec3f direction3D = getRotation() * d3d::Vec3f(1.0f, 0.0f, 0.0f);
			d3d::Vec2f currentDir(direction3D.x, direction3D.z);
			
			float aC = std::atan2f(currentDir.y, currentDir.x);

			float aL = std::atan2f(toLocation.y, toLocation.x);

			aL -= aC;

			if (aL > d3d::_pi)
				aL = -(d3d::_piTimes2 - aL);
			else if (aL < -d3d::_pi)
				aL += d3d::_piTimes2;

			float leftSpeed = _pRigidBody->getVelocityInLocalPoint(btVector3(0.0f, 0.0f, _pCollisionShape->getHalfExtentsWithoutMargin().getZ() * _treadSideOffsetMultiplier)).length();
			float rightSpeed = _pRigidBody->getVelocityInLocalPoint(btVector3(0.0f, 0.0f, -_pCollisionShape->getHalfExtentsWithoutMargin().getZ() * _treadSideOffsetMultiplier)).length();

			if (leftSpeed < _maxSpeed && rightSpeed < _maxSpeed) {
				if (aL > _rotationTolerance) {
					// Turn left
					_pRigidBody->applyTorque(btVector3(0.0f, -_turnRate, 0.0f));
				}
				else if (aL < -_rotationTolerance) {
					// Turn right
					_pRigidBody->applyTorque(btVector3(0.0f, _turnRate, 0.0f));
				}
			}

			if (std::fabsf(aL) < _rotationTolerance && _fireTimer <= 0.0f) {
				_fireTimer = _rateOfFire;

				_attackEnemy = pEnemy;
			}
		}
	}
		
	if (drive) {
		float leftSpeed = _pRigidBody->getVelocityInLocalPoint(btVector3(0.0f, 0.0f, _pCollisionShape->getHalfExtentsWithoutMargin().getZ() * _treadSideOffsetMultiplier)).length();
		float rightSpeed = _pRigidBody->getVelocityInLocalPoint(btVector3(0.0f, 0.0f, -_pCollisionShape->getHalfExtentsWithoutMargin().getZ() * _treadSideOffsetMultiplier)).length();

		if (leftSpeed < _maxSpeed)
			_pRigidBody->applyForce(bt(facingDirection) * _leftPower * _driveRate * (_maxSpeed - leftSpeed) / _maxSpeed, btVector3(0.0f, 0.0f, _pCollisionShape->getHalfExtentsWithoutMargin().getZ() * _treadSideOffsetMultiplier));

		if (rightSpeed < _maxSpeed)
			_pRigidBody->applyForce(bt(facingDirection) * _rightPower * _driveRate * (_maxSpeed - leftSpeed) / _maxSpeed, btVector3(0.0f, 0.0f, -_pCollisionShape->getHalfExtentsWithoutMargin().getZ() * _treadSideOffsetMultiplier));
	}

	// Cancel sideways velocity
	_pRigidBody->setLinearVelocity(bt(cons(_pRigidBody->getLinearVelocity()).project(cons(_pRigidBody->getWorldTransform().getRotation()) * d3d::Vec3f(1.0f, 0.0f, 0.0f))));

	if (_fireTimer > 0.0f)
		_fireTimer -= dt;

	d3d::Matrix4x4f transform;
	_pRigidBody->getWorldTransform().getOpenGLMatrix(&transform._elements[0]);

	transform *= d3d::Matrix4x4f::translateMatrix(d3d::Vec3f(0.0f, 0.5f - _pCollisionShape->getHalfExtentsWithoutMargin().getY(), 0.0f));

	_aabb = _trile->getMinBounds().getTransformedAABB(transform);

	updateAABB();

	if (_age < _lifeSpan)
		_age += dt;
	else
		_kill = true;

	if (_hp <= 0.0f)
		_kill = true;

	// If just spawned and an enemy is in the way, kill self. Otherwise if just spawned and a friendly is in the way, kill the friendly
	float minRange = std::min(_pCollisionShape->getHalfExtentsWithoutMargin().getX(), std::min(_pCollisionShape->getHalfExtentsWithoutMargin().getY(), _pCollisionShape->getHalfExtentsWithoutMargin().getZ()));

	if (_frames < 2) {
		for (size_t i = 0; i < friends.size(); i++)
		if ((friends[i]->getPosition() - getPosition()).magnitude() < minRange) {
			friends[i]->_kill = true;

			break;
		}

		for (size_t i = 0; i < enemies.size(); i++)
		if ((enemies[i]->getPosition() - getPosition()).magnitude() < minRange) {
			_kill = true;

			break;
		}
	}

	unitUpdate(dt);

	_frames++;
}

void SceneObjectUnit::synchronousUpdate(float dt) {
	if (_kill) {
		if (shouldDestroy())
			return;

		d3d::Matrix4x4f transform;
		_pRigidBody->getWorldTransform().getOpenGLMatrix(&transform._elements[0]);

		transform *= d3d::Matrix4x4f::translateMatrix(d3d::Vec3f(0.0f, 0.5f - _pCollisionShape->getHalfExtentsWithoutMargin().getY(), 0.0f)) * d3d::Matrix4x4f::rotateMatrixY(-d3d::_piOver2);

		std::shared_ptr<TrileExplosion> explosion(new TrileExplosion());

		getRenderScene()->add(explosion, false);

		explosion->create(_chunk.get(), transform, transform * d3d::Vec3f(0.0f, -0.9f, 0.0f), 1.0f, 0.1f, 0.5f, 0.2f);

		destroy();

		return;
	}

	if (_attackEnemy.isAlive()) {
		unitAttack(static_cast<SceneObjectUnit*>(_attackEnemy.get()));

		_attackEnemy = nullptr;
	}

	unitSynchronousUpdate(dt);
}

void SceneObjectUnit::deferredRender() {
	btTransform physicsTransform;
	_pMotionState->getWorldTransform(physicsTransform);

	physicsTransform.getOpenGLMatrix(&_fullTransform._elements[0]);

	_fullTransform *= d3d::Matrix4x4f::translateMatrix(d3d::Vec3f(0.0f, 0.5f - _pCollisionShape->getHalfExtentsWithoutMargin().getY(), 0.0f));
	_fullTransform *= _offsetTransform;

	assert(_trileBatcher.isAlive());

	static_cast<TrileBatcher*>(_trileBatcher.get())->add(_trile.get(), _fullTransform);

	unitRender();
}