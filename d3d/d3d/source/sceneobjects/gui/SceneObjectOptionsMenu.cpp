#include <sceneobjects/gui/SceneObjectOptionsMenu.h>

#include <scene/RenderScene.h>

#include <sceneobjects/SceneObjectConfig.h>

#include <util/Math.h>

#include <fstream>

SceneObjectOptionsMenu::SceneObjectOptionsMenu()
: _pixelScalar(6.0f),
_currentTabIndex(0),
_draggingSliderTabIndex(-1),
_draggingSliderSubIndex(-1)
{
	_renderMask = 0xffff;

	_layer = 2.0f;
}

void SceneObjectOptionsMenu::guiOnAdd() {
	_input = getScene()->getNamed("buffIn");

	assert(_input.isAlive());

	std::shared_ptr<d3d::Asset> asset;

	if (!getScene()->getAssetManager("font", Font::assetFactory)->getAsset("resources/gui/fonts/pixelated.ttf", asset))
		abort();

	_font = std::static_pointer_cast<Font>(asset);

	if (!getScene()->getAssetManager("tex2D", d3d::Texture2D::assetFactory)->getAsset("resources/gui/menus/panel1.png", asset))
		abort();

	_panelTexture = std::static_pointer_cast<d3d::Texture2D>(asset);

	if (!getScene()->getAssetManager("tex2D", d3d::Texture2D::assetFactory)->getAsset("resources/gui/menus/cycleSelector1.png", asset))
		abort();

	_cycleSelectorTexture = std::static_pointer_cast<d3d::Texture2D>(asset);

	if (!getScene()->getAssetManager("tex2D", d3d::Texture2D::assetFactory)->getAsset("resources/gui/menus/checkBoxTicked1.png", asset))
		abort();

	_checkBoxTickedTexture = std::static_pointer_cast<d3d::Texture2D>(asset);

	if (!getScene()->getAssetManager("tex2D", d3d::Texture2D::assetFactory)->getAsset("resources/gui/menus/checkBoxUnticked1.png", asset))
		abort();

	_checkBoxUntickedTexture = std::static_pointer_cast<d3d::Texture2D>(asset);

	if (!getScene()->getAssetManager("tex2D", d3d::Texture2D::assetFactory)->getAsset("resources/gui/menus/sliderSlide1.png", asset))
		abort();

	_sliderSlideTexture = std::static_pointer_cast<d3d::Texture2D>(asset);

	if (!getScene()->getAssetManager("tex2D", d3d::Texture2D::assetFactory)->getAsset("resources/gui/menus/sliderHandle1.png", asset))
		abort();

	_sliderHandleTexture = std::static_pointer_cast<d3d::Texture2D>(asset);

	if (!getScene()->getAssetManager("tex2D", d3d::Texture2D::assetFactory)->getAsset("resources/gui/menus/button2up.png", asset))
		abort();

	_button1TextureUp = std::static_pointer_cast<d3d::Texture2D>(asset);

	if (!getScene()->getAssetManager("tex2D", d3d::Texture2D::assetFactory)->getAsset("resources/gui/menus/button2over.png", asset))
		abort();

	_button1TextureOver = std::static_pointer_cast<d3d::Texture2D>(asset);

	if (!getScene()->getAssetManager("tex2D", d3d::Texture2D::assetFactory)->getAsset("resources/gui/menus/button2down.png", asset))
		abort();

	_button1TextureDown = std::static_pointer_cast<d3d::Texture2D>(asset);

	_tabTextures.resize(3);

	if (!getScene()->getAssetManager("tex2D", d3d::Texture2D::assetFactory)->getAsset("resources/gui/menus/tab1.png", asset))
		abort();

	_tabTextures[0] = std::static_pointer_cast<d3d::Texture2D>(asset);

	if (!getScene()->getAssetManager("tex2D", d3d::Texture2D::assetFactory)->getAsset("resources/gui/menus/tab2.png", asset))
		abort();

	_tabTextures[1] = std::static_pointer_cast<d3d::Texture2D>(asset);

	if (!getScene()->getAssetManager("tex2D", d3d::Texture2D::assetFactory)->getAsset("resources/gui/menus/tab3.png", asset))
		abort();

	_tabTextures[2] = std::static_pointer_cast<d3d::Texture2D>(asset);

	_tabButtons.resize(3);

	sf::Vector2f tabOffset(28.0f, 22.0f);

	_tabButtons[0]._rect = sf::FloatRect(-81.5f + tabOffset.x, -78.5f + tabOffset.y, 23.0f, 7.0f);
	_tabButtons[0]._text = "Graphics";

	_tabButtons[1]._rect = sf::FloatRect(-57.5f + tabOffset.x, -78.5f + tabOffset.y, 23.0f, 7.0f);
	_tabButtons[1]._text = "Audio";

	_tabButtons[2]._rect = sf::FloatRect(-33.5f + tabOffset.x, -78.5f + tabOffset.y, 23.0f, 7.0f);
	_tabButtons[2]._text = "Controls";

	_panelTexture->bind();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	_cycleSelectorTexture->bind();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	_checkBoxTickedTexture->bind();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	_checkBoxUntickedTexture->bind();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	_sliderSlideTexture->bind();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	_sliderHandleTexture->bind();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	_button1TextureUp->bind();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	_button1TextureOver->bind();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	_button1TextureDown->bind();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	for (size_t i = 0; i < _tabTextures.size(); i++) {
		_tabTextures[i]->bind();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}

	_tabs.resize(3);

	_tabs[0]._cycleSelectors.resize(2);

	_tabs[0]._cycleSelectors[0]._cycle = 0;

	_modes.reset(new std::vector<sf::VideoMode>());
	(*_modes) = sf::VideoMode::getFullscreenModes();

	for (size_t i = 0; i < _modes->size(); i++)
		_tabs[0]._cycleSelectors[0]._cycleTexts.push_back(std::to_string((*_modes)[i].width) + "x" + std::to_string((*_modes)[i].height));

	_tabs[0]._cycleSelectors[0]._position = sf::Vector2f(-41.5f + tabOffset.x, -68.5f + tabOffset.y);
	_tabs[0]._cycleSelectors[0]._text = "Resolution:";

	_tabs[0]._checkBoxes.resize(5);

	_tabs[0]._checkBoxes[0]._isChecked = false;
	_tabs[0]._checkBoxes[0]._position = sf::Vector2f(-41.5f + tabOffset.x, -58.5f + tabOffset.y);
	_tabs[0]._checkBoxes[0]._text = "Windowed:";

	_tabs[0]._checkBoxes[1]._isChecked = false;
	_tabs[0]._checkBoxes[1]._position = sf::Vector2f(-41.5f + tabOffset.x, -48.5f + tabOffset.y);
	_tabs[0]._checkBoxes[1]._text = "VSync:";

	_tabs[0]._checkBoxes[2]._isChecked = false;
	_tabs[0]._checkBoxes[2]._position = sf::Vector2f(-41.5f + tabOffset.x, -38.5f + tabOffset.y);
	_tabs[0]._checkBoxes[2]._text = "SSAO:";

	_tabs[0]._checkBoxes[3]._isChecked = false;
	_tabs[0]._checkBoxes[3]._position = sf::Vector2f(-41.5f + tabOffset.x, -28.5f + tabOffset.y);
	_tabs[0]._checkBoxes[3]._text = "DOF:";

	_tabs[0]._checkBoxes[4]._isChecked = false;
	_tabs[0]._checkBoxes[4]._position = sf::Vector2f(-41.5f + tabOffset.x, -18.5f + tabOffset.y);
	_tabs[0]._checkBoxes[4]._text = "FXAA:";

	_tabs[0]._cycleSelectors[1]._cycle = 0;
	_tabs[0]._cycleSelectors[1]._cycleTexts.resize(3);
	_tabs[0]._cycleSelectors[1]._cycleTexts[0] = "low";
	_tabs[0]._cycleSelectors[1]._cycleTexts[1] = "medium";
	_tabs[0]._cycleSelectors[1]._cycleTexts[2] = "high";
	_tabs[0]._cycleSelectors[1]._position = sf::Vector2f(-41.5f + tabOffset.x, -8.5f + tabOffset.y);
	_tabs[0]._cycleSelectors[1]._text = "Effects:";

	_tabs[1]._sliders.resize(2);

	_tabs[1]._sliders[0]._slideValue = 1.0f;
	_tabs[1]._sliders[0]._position = sf::Vector2f(-41.5f + tabOffset.x, -68.5f + tabOffset.y);
	_tabs[1]._sliders[0]._text = "SFX Volume:";

	_tabs[1]._sliders[1]._slideValue = 1.0f;
	_tabs[1]._sliders[1]._position = sf::Vector2f(-41.5f + tabOffset.x, -48.5f + tabOffset.y);
	_tabs[1]._sliders[1]._text = "Music Volume:";

	// Shared buttons
	_sharedButtons.resize(2);

	_sharedButtons[0]._rect = sf::FloatRect(-25.0f, 42.0f, 36.0f, 11.0f);
	_sharedButtons[0]._text = "Save Settings";
	_sharedButtons[0]._functor = std::bind(saveSettings, std::placeholders::_1);
	_sharedButtons[0]._state = _up;

	_sharedButtons[1]._rect = sf::FloatRect(15.0f, 42.0f, 36.0f, 11.0f);
	_sharedButtons[1]._text = "Back";
	_sharedButtons[1]._functor = std::bind(back, std::placeholders::_1);
	_sharedButtons[1]._state = _up;

	// ---------------------------------- Load data ----------------------------------

	loadConfig();
}

void SceneObjectOptionsMenu::updateSettings() {
	_resolutionX = (*_modes)[_tabs[0]._cycleSelectors[0]._cycle].width;
	_resolutionY = (*_modes)[_tabs[0]._cycleSelectors[0]._cycle].height;

	_windowed = _tabs[0]._checkBoxes[0]._isChecked;
	_vsync = _tabs[0]._checkBoxes[1]._isChecked;
	_ssao = _tabs[0]._checkBoxes[2]._isChecked;
	_dof = _tabs[0]._checkBoxes[3]._isChecked;
	_fxaa = _tabs[0]._checkBoxes[4]._isChecked;

	_effectsQuality = static_cast<Quality>(_tabs[0]._cycleSelectors[1]._cycle);

	_sfxVolume = static_cast<int>(100.0f * _tabs[1]._sliders[0]._slideValue);
	_musicVolume = static_cast<int>(100.0f * _tabs[1]._sliders[1]._slideValue);
}

void SceneObjectOptionsMenu::saveSettings(SceneObjectOptionsMenu &optionsMenu) {
	// Collect settings
	optionsMenu.updateSettings();

	optionsMenu.saveConfig();

	// Get config
	d3d::SceneObjectRef config = optionsMenu.getScene()->getNamed("config");

	SceneObjectConfig* pConfig = static_cast<SceneObjectConfig*>(config.get());

	pConfig->_resolutionWidth = optionsMenu._resolutionX;
	pConfig->_resolutionHeight = optionsMenu._resolutionY;
	pConfig->_windowed = optionsMenu._windowed;
	pConfig->_vsync = optionsMenu._vsync;
	pConfig->_ssao = optionsMenu._ssao;
	pConfig->_dof = optionsMenu._dof;
	pConfig->_fxaa = optionsMenu._fxaa;
	pConfig->_effectsQuality = static_cast<SceneObjectConfig::EffectsQuality>(optionsMenu._effectsQuality);
	pConfig->_sfxVolume = optionsMenu._sfxVolume;
	pConfig->_musicVolume = optionsMenu._musicVolume;

	pConfig->changed();
}

void SceneObjectOptionsMenu::back(SceneObjectOptionsMenu &optionsMenu) {

}

void SceneObjectOptionsMenu::guiRender(sf::RenderTexture &renderTexture) {
	const float pixelScalar = _pixelScalar;

	sf::Vector2f center = sf::Vector2f(getRenderScene()->getRenderWindow()->getSize().x * 0.5f, getRenderScene()->getRenderWindow()->getSize().y * 0.5f);

	sf::Sprite panelSprite;

	panelSprite.setTexture(_panelTexture->getTexture());

	panelSprite.setScale(pixelScalar, pixelScalar);

	panelSprite.setOrigin(sf::Vector2f(_panelTexture->getWidth() * 0.5f, _panelTexture->getHeight() * 0.5f));

	panelSprite.setPosition(center);

	renderTexture.draw(panelSprite);

	sf::Text text;

	sf::Vector2f tabTextOffset(2.0f, 0.0f);

	text.setFont(_font->_font);

	// Tabs rendering
	text.setColor(sf::Color::Black);
	panelSprite.setColor(sf::Color(200, 200, 200));

	for (size_t i = 0; i < _tabTextures.size(); i++) {
		if (i == _currentTabIndex)
			continue;

		panelSprite.setTexture(_tabTextures[i]->getTexture());

		renderTexture.draw(panelSprite);

		text.setPosition((_tabButtons[i]._rect.left + tabTextOffset.x) * pixelScalar + center.x, (_tabButtons[i]._rect.top + tabTextOffset.y) * pixelScalar + center.y);

		text.setString(_tabButtons[i]._text);

		renderTexture.draw(text);
	}

	text.setColor(sf::Color::Black);
	panelSprite.setColor(sf::Color::White);

	panelSprite.setTexture(_tabTextures[_currentTabIndex]->getTexture());

	renderTexture.draw(panelSprite);

	text.setPosition((_tabButtons[_currentTabIndex]._rect.left + tabTextOffset.x) * pixelScalar + center.x, (_tabButtons[_currentTabIndex]._rect.top + tabTextOffset.y) * pixelScalar + center.y);

	text.setString(_tabButtons[_currentTabIndex]._text);

	renderTexture.draw(text);

	// Render active tab
	for (size_t i = 0; i < _tabs[_currentTabIndex]._buttons.size(); i++) {

	}

	for (size_t i = 0; i < _tabs[_currentTabIndex]._checkBoxes.size(); i++) {
		sf::Sprite checkBoxSprite;

		checkBoxSprite.setPosition(_tabs[_currentTabIndex]._checkBoxes[i]._position * pixelScalar + center);

		checkBoxSprite.setTexture(_tabs[_currentTabIndex]._checkBoxes[i]._isChecked ? _checkBoxTickedTexture->getTexture() : _checkBoxUntickedTexture->getTexture());

		checkBoxSprite.setScale(sf::Vector2f(pixelScalar, pixelScalar));

		renderTexture.draw(checkBoxSprite);

		text.setColor(sf::Color::Black);

		text.setString(_tabs[_currentTabIndex]._checkBoxes[i]._text);

		sf::FloatRect bounds = text.getLocalBounds();

		text.setPosition((_tabs[_currentTabIndex]._checkBoxes[i]._position.x - 2.0f) * pixelScalar - bounds.width + center.x, (_tabs[_currentTabIndex]._checkBoxes[i]._position.y + 4.0f) * pixelScalar - bounds.height + center.y);

		renderTexture.draw(text);
	}

	for (size_t i = 0; i < _tabs[_currentTabIndex]._cycleSelectors.size(); i++) {
		sf::Sprite cycleSelectorSprite;

		cycleSelectorSprite.setPosition(_tabs[_currentTabIndex]._cycleSelectors[i]._position * pixelScalar + center);

		cycleSelectorSprite.setTexture(_cycleSelectorTexture->getTexture());

		cycleSelectorSprite.setScale(sf::Vector2f(pixelScalar, pixelScalar));

		renderTexture.draw(cycleSelectorSprite);

		text.setColor(sf::Color::White);

		if (!_tabs[_currentTabIndex]._cycleSelectors[i]._cycleTexts.empty()) {
			text.setString(_tabs[_currentTabIndex]._cycleSelectors[i]._cycleTexts[_tabs[_currentTabIndex]._cycleSelectors[i]._cycle]);

			sf::FloatRect bounds = text.getLocalBounds();

			text.setPosition((_tabs[_currentTabIndex]._cycleSelectors[i]._position.x + 32.0f) * pixelScalar - bounds.width * 0.5f + center.x, (_tabs[_currentTabIndex]._cycleSelectors[i]._position.y + 4.0f) * pixelScalar - bounds.height + center.y);

			renderTexture.draw(text);
		}

		text.setColor(sf::Color::Black);

		text.setString(_tabs[_currentTabIndex]._cycleSelectors[i]._text);

		sf::FloatRect bounds = text.getLocalBounds();

		text.setPosition((_tabs[_currentTabIndex]._cycleSelectors[i]._position.x - 2.0f) * pixelScalar - bounds.width + center.x, (_tabs[_currentTabIndex]._cycleSelectors[i]._position.y + 4.0f) * pixelScalar - bounds.height + center.y);

		renderTexture.draw(text);
	}

	for (size_t i = 0; i < _tabs[_currentTabIndex]._sliders.size(); i++) {
		sf::Sprite sliderSelectorSprite;

		sliderSelectorSprite.setPosition((_tabs[_currentTabIndex]._sliders[i]._position + sf::Vector2f(0.0f, 2.0f)) * pixelScalar + center);

		sliderSelectorSprite.setTexture(_sliderSlideTexture->getTexture());

		sliderSelectorSprite.setScale(sf::Vector2f(pixelScalar, pixelScalar));

		renderTexture.draw(sliderSelectorSprite);

		sf::Sprite handleSelectorSprite;

		handleSelectorSprite.setPosition(sliderSelectorSprite.getPosition() + sf::Vector2f(_tabs[_currentTabIndex]._sliders[i]._slideValue * ((_sliderSlideTexture->getWidth() - 4.0f) * pixelScalar) + 1.0f * pixelScalar, 0.0f) - pixelScalar * sf::Vector2f(0.0f, 2.0f));

		handleSelectorSprite.setTexture(_sliderHandleTexture->getTexture());

		handleSelectorSprite.setScale(sf::Vector2f(pixelScalar, pixelScalar));

		renderTexture.draw(handleSelectorSprite);

		text.setColor(sf::Color::Black);

		text.setString(_tabs[_currentTabIndex]._sliders[i]._text);

		sf::FloatRect bounds = text.getLocalBounds();

		text.setPosition((_tabs[_currentTabIndex]._sliders[i]._position.x - 2.0f) * pixelScalar - bounds.width + center.x, (_tabs[_currentTabIndex]._sliders[i]._position.y + 4.0f) * pixelScalar - bounds.height + center.y);

		renderTexture.draw(text);
	}

	// Render shared buttons
	for (size_t i = 0; i < _sharedButtons.size(); i++) {
		sf::Sprite buttonSprite;

		buttonSprite.setPosition(sf::Vector2f(_sharedButtons[i]._rect.left, _sharedButtons[i]._rect.top) * pixelScalar + center);

		buttonSprite.setScale(sf::Vector2f(pixelScalar, pixelScalar));

		switch (_sharedButtons[i]._state) {
		case _up:
			{
				buttonSprite.setTexture(_button1TextureUp->getTexture());

				renderTexture.draw(buttonSprite);

				text.setColor(sf::Color::White);

				text.setString(_sharedButtons[i]._text);

				sf::FloatRect bounds = text.getLocalBounds();

				text.setPosition((_sharedButtons[i]._rect.left + 19.0f) * pixelScalar - bounds.width * 0.5f + center.x, (_sharedButtons[i]._rect.top + 6.0f) * pixelScalar - bounds.height + center.y);

				renderTexture.draw(text);

				break;
			}
		case _over:
			{
				buttonSprite.setTexture(_button1TextureOver->getTexture());

				renderTexture.draw(buttonSprite);

				text.setColor(sf::Color::White);

				text.setString(_sharedButtons[i]._text);

				sf::FloatRect bounds = text.getLocalBounds();

				text.setPosition((_sharedButtons[i]._rect.left + 19.0f) * pixelScalar - bounds.width * 0.5f + center.x, (_sharedButtons[i]._rect.top + 6.0f) * pixelScalar - bounds.height + center.y);

				renderTexture.draw(text);

				break;
			}
		case _down:
			{
				buttonSprite.setTexture(_button1TextureDown->getTexture());

				renderTexture.draw(buttonSprite);

				text.setColor(sf::Color::White);

				text.setString(_sharedButtons[i]._text);

				sf::FloatRect bounds = text.getLocalBounds();

				text.setPosition((_sharedButtons[i]._rect.left + 21.0f) * pixelScalar - bounds.width * 0.5f + center.x, (_sharedButtons[i]._rect.top + 8.0f) * pixelScalar - bounds.height + center.y);

				renderTexture.draw(text);

				break;
			}
		}
	}
}

void SceneObjectOptionsMenu::synchronousUpdate(float dt) {
	float pixelScalarInv = 1.0f / _pixelScalar;

	sf::Vector2f center = sf::Vector2f(getRenderScene()->getRenderWindow()->getSize().x * 0.5f, getRenderScene()->getRenderWindow()->getSize().y * 0.5f);

	d3d::SceneObjectBufferedInput* pInput = static_cast<d3d::SceneObjectBufferedInput*>(_input.get());

	sf::Vector2i mousePosi = sf::Mouse::getPosition(*getRenderScene()->getRenderWindow());
	sf::Vector2f mousePosf = sf::Vector2f(mousePosi.x, mousePosi.y);
	sf::Vector2f mousePosfTransformed = sf::Vector2f((mousePosf.x - center.x) * pixelScalarInv, (mousePosf.y - center.y) * pixelScalarInv);

	if (pInput->isLMBPressed()) {
		for (size_t i = 0; i < _tabs[_currentTabIndex]._sliders.size(); i++) {
			sf::Vector2f pos = (_tabs[_currentTabIndex]._sliders[i]._position + sf::Vector2f(0.0f, 2.0f)) * _pixelScalar + center + sf::Vector2f(_tabs[_currentTabIndex]._sliders[i]._slideValue * ((_sliderSlideTexture->getWidth() - 4.0f) * _pixelScalar) + 1.0f * _pixelScalar, 0.0f) - _pixelScalar * sf::Vector2f(0.0f, 2.0f);
			sf::FloatRect handleRect(pos.x, pos.y, 2.0f * _pixelScalar, 8.0f * _pixelScalar);

			if (handleRect.contains(mousePosf)) {
				_draggingSliderTabIndex = _currentTabIndex;
				_draggingSliderSubIndex = i;

				_dragStart = mousePosf;

				_startDragSliderValue = _tabs[_currentTabIndex]._sliders[i]._slideValue;
			}
		}
	}

	if (_draggingSliderTabIndex != -1 && _draggingSliderSubIndex != -1) {
		sf::Vector2f delta = mousePosf - _dragStart;

		_tabs[_draggingSliderTabIndex]._sliders[_draggingSliderSubIndex]._slideValue = std::min(1.0f, std::max(0.0f, delta.x * pixelScalarInv / (_sliderSlideTexture->getWidth() - 3.0f) + _startDragSliderValue));
	}

	if (pInput->isLMBReleased()) {
		// Stop drag
		_draggingSliderTabIndex = -1;
		_draggingSliderSubIndex = -1;

		for (size_t i = 0; i < _tabButtons.size(); i++) {
			if (_tabButtons[i]._rect.contains(mousePosfTransformed)) {
				_currentTabIndex = i;
			}
		}

		for (size_t i = 0; i < _tabs[_currentTabIndex]._buttons.size(); i++) {

		}

		for (size_t i = 0; i < _tabs[_currentTabIndex]._checkBoxes.size(); i++) {
			sf::FloatRect button(_tabs[_currentTabIndex]._checkBoxes[i]._position.x * _pixelScalar + center.x, _tabs[_currentTabIndex]._checkBoxes[i]._position.y * _pixelScalar + center.y, 8.0f * _pixelScalar, 8.0f * _pixelScalar);

			if (button.contains(mousePosf))
				_tabs[_currentTabIndex]._checkBoxes[i]._isChecked = !_tabs[_currentTabIndex]._checkBoxes[i]._isChecked;
		}

		for (size_t i = 0; i < _tabs[_currentTabIndex]._cycleSelectors.size(); i++) {
			sf::FloatRect leftButton(_tabs[_currentTabIndex]._cycleSelectors[i]._position.x * _pixelScalar + center.x, _tabs[_currentTabIndex]._cycleSelectors[i]._position.y * _pixelScalar + center.y, 8.0f * _pixelScalar, 8.0f * _pixelScalar);
			sf::FloatRect rightButton((_tabs[_currentTabIndex]._cycleSelectors[i]._position.x + 56.0f) * _pixelScalar + center.x, _tabs[_currentTabIndex]._cycleSelectors[i]._position.y * _pixelScalar + center.y, 8.0f * _pixelScalar, 8.0f * _pixelScalar);

			if (leftButton.contains(mousePosf))
				_tabs[_currentTabIndex]._cycleSelectors[i]._cycle--;
			else if (rightButton.contains(mousePosf))
				_tabs[_currentTabIndex]._cycleSelectors[i]._cycle++;

			_tabs[_currentTabIndex]._cycleSelectors[i]._cycle = _tabs[_currentTabIndex]._cycleSelectors[i]._cycle % _tabs[_currentTabIndex]._cycleSelectors[i]._cycleTexts.size();

			if (_tabs[_currentTabIndex]._cycleSelectors[i]._cycle < 0)
				_tabs[_currentTabIndex]._cycleSelectors[i]._cycle += _tabs[_currentTabIndex]._cycleSelectors[i]._cycleTexts.size();
		}

		for (size_t i = 0; i < _sharedButtons.size(); i++) {
			if (_sharedButtons[i]._rect.contains(mousePosfTransformed)) {
				_sharedButtons[i]._state = _down;
				_sharedButtons[i]._functor(*this);
			}
		}
	}
	else if (pInput->isLMBDown()) {
		for (size_t i = 0; i < _sharedButtons.size(); i++) {
			if (_sharedButtons[i]._rect.contains(mousePosfTransformed)) {
				_sharedButtons[i]._state = _down;
			}
		}
	}
	else {
		for (size_t i = 0; i < _sharedButtons.size(); i++) {
			if (_sharedButtons[i]._rect.contains(mousePosfTransformed))
				_sharedButtons[i]._state = _over;
			else
				_sharedButtons[i]._state = _up;
		}
	}
}

void SceneObjectOptionsMenu::loadConfig() {
	std::ifstream is("config.txt");

	std::string placeholder, value;

	// Current graphics config
	is >> placeholder >> _resolutionX >> _resolutionY;

	is >> placeholder >> value;

	_windowed = value == "true";

	is >> placeholder >> value;

	_vsync = value == "true";

	is >> placeholder >> value;

	_ssao = value == "true";

	is >> placeholder >> value;

	_dof = value == "true";

	is >> placeholder >> value;

	_fxaa = value == "true";

	is >> placeholder >> value;

	if (value == "low")
		_effectsQuality = _low;
	else if (value == "medium")
		_effectsQuality = _medium;
	else
		_effectsQuality = _high;

	// Current audio config
	is >> placeholder >> _sfxVolume;
	
	is >> placeholder >> _musicVolume;

	is.close();

	// Set up GUI from these settings
	assert(!_modes->empty());

	for (size_t i = 0; i < _modes->size(); i++) {
		if ((*_modes)[i].width == _resolutionX && (*_modes)[i].height == _resolutionY) {
			_tabs[0]._cycleSelectors[0]._cycle = i;
			break;
		}
	}

	_tabs[0]._checkBoxes[0]._isChecked = _windowed;
	_tabs[0]._checkBoxes[1]._isChecked = _ssao;
	_tabs[0]._checkBoxes[2]._isChecked = _dof;
	_tabs[0]._checkBoxes[3]._isChecked = _fxaa;

	_tabs[0]._cycleSelectors[1]._cycle = static_cast<int>(_effectsQuality);

	_tabs[1]._sliders[0]._slideValue = 0.01f * _sfxVolume;
	_tabs[1]._sliders[1]._slideValue = 0.01f * _musicVolume;
}

void SceneObjectOptionsMenu::saveConfig() {
	std::ofstream os("config.txt");

	os << "Resolution: " << _resolutionX << " " << _resolutionY << std::endl;
	os << "Windowed: " << (_windowed ? "true" : "false") << std::endl;
	os << "Vsync: " << (_vsync ? "true" : "false") << std::endl;
	os << "SSAO: " << (_ssao ? "true" : "false") << std::endl;
	os << "DOF: " << (_dof ? "true" : "false") << std::endl;
	os << "FXAA: " << (_fxaa ? "true" : "false") << std::endl;
	os << "Quality: " << (_effectsQuality == _low ? "low" : (_effectsQuality == _medium ? "medium" : "high")) << std::endl;
	os << "SFXVolume: " << _sfxVolume << std::endl;
	os << "MusicVolume: " << _musicVolume << std::endl;

	os.close();
}