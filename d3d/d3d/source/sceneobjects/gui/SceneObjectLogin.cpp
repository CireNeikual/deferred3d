#include <sceneobjects/gui/SceneObjectLogin.h>

SceneObjectLogin::SceneObjectLogin()
: _activeTextBoxIndex(-1)
{
	_layer = 2.0f;

	_renderMask = 0xffff;
}

void SceneObjectLogin::guiOnAdd() {
	_input = getScene()->getNamed("buffIn");

	assert(_input.isAlive());

	std::shared_ptr<d3d::Asset> asset;

	if (!getScene()->getAssetManager("font", Font::assetFactory)->getAsset("resources/gui/fonts/pixelated.ttf", asset))
		abort();

	_font = std::static_pointer_cast<Font>(asset);

	if (!getScene()->getAssetManager("tex2D", d3d::Texture2D::assetFactory)->getAsset("resources/gui/menus/panel1.png", asset))
		abort();

	_panelTexture = std::static_pointer_cast<d3d::Texture2D>(asset);
}

void SceneObjectLogin::guiRender(sf::RenderTexture &renderTexture) {

}

void SceneObjectLogin::synchronousUpdate(float dt) {

}

void SceneObjectLogin::onResize() {

}