#pragma once

#include <sceneobjects/gui/SceneObjectGUIRenderable.h>

#include <sceneobjects/gui/Font.h>

#include <sceneobjects/input/SceneObjectBufferedInput.h>

#include <rendering/texture/Texture2D.h>

class SceneObjectLogin : public SceneObjectGUIRenderable {
public:
	enum ButtonState {
		_up, _over, _down
	};

	struct Button {
		sf::FloatRect _rect;
		std::string _text;
		ButtonState _state;

		std::function<void(SceneObjectLogin &menu)> _functor;
	};

	struct CheckBox {
		sf::Vector2f _position;
		bool _isChecked;

		std::string _text;
	};

	struct TextBox {
		sf::Vector2f _position;

		std::string _text;
		std::string _boxText;

		int _cursorPosition;
	};

private:
	d3d::SceneObjectRef _input;

	std::shared_ptr<Font> _font;

	std::shared_ptr<d3d::Texture2D> _panelTexture;

	std::vector<Button> _buttons;
	std::vector<CheckBox> _checkBoxes;
	std::vector<TextBox> _textBoxes;

	int _activeTextBoxIndex;

public:
	SceneObjectLogin();

	// Inherited from SceneObjectGUIRenderable
	void guiOnAdd();
	void guiRender(sf::RenderTexture &renderTexture);

	void synchronousUpdate(float dt);
	void onResize();

	SceneObjectLogin* copyFactory() {
		return new SceneObjectLogin(*this);
	}
};