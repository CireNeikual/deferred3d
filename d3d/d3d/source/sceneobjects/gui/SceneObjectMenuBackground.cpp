#include <sceneobjects/gui/SceneObjectMenuBackground.h>

#include <util/Math.h>

SceneObjectMenuBackground::SceneObjectMenuBackground()
: _t(0.0f)
{
	_renderMask = 0xffff;
}

void SceneObjectMenuBackground::onDestroy() {
	if (_directionalLight.isAlive())
		_directionalLight->destroy();

	if (_directionalLightShadowed.isAlive())
		_directionalLightShadowed->destroy();
}

void SceneObjectMenuBackground::onAdd() {
	getRenderScene()->_clearColor = d3d::Vec4f(0.8f, 0.8f, 0.8f, 1.0f);

	d3d::SceneObjectRef lighting = getScene()->getNamed("lighting");

	d3d::SceneObjectLighting* pLighting = static_cast<d3d::SceneObjectLighting*>(lighting.get());

	pLighting->_ambientLight = d3d::Vec3f(0.02f, 0.02f, 0.02f);

	std::shared_ptr<d3d::SceneObjectDirectionalLightShadowed> light(new d3d::SceneObjectDirectionalLightShadowed());

	getRenderScene()->add(light);

	light->create(pLighting, 2, 2048, 0.5f, 12.5f, 0.6f);

	light->setDirection(d3d::Vec3f(-0.7f, -1.0f, -0.5f).normalized());

	light->setColor(d3d::Vec3f(1.0f, 1.0f, 1.0f));

	_directionalLightShadowed = *light;

	std::shared_ptr<d3d::SceneObjectDirectionalLight> light2(new d3d::SceneObjectDirectionalLight());

	getRenderScene()->add(light2);

	light2->create(pLighting);

	light2->setDirection(d3d::Vec3f(0.2f, 1.0f, 0.1f).normalized());

	light2->setColor(d3d::Vec3f(0.1f, 0.1f, 0.1f));

	_directionalLight = *light2;

	_batcherRef = getScene()->getNamed("smb");

	std::shared_ptr<d3d::Asset> asset;

	if (!getScene()->getAssetManager("MOBJ", d3d::StaticModelOBJ::assetFactory)->getAsset("resources/models/doublehelix.obj", asset))
		abort();

	_doubleHelix = std::static_pointer_cast<d3d::StaticModelOBJ>(asset);
}

void SceneObjectMenuBackground::update(float dt) {
	_t = std::fmod(_t + 0.1f * dt, d3d::_piTimes2);

	getRenderScene()->_logicCamera._position = d3d::Vec3f(0.0f, 0.0f, 0.0f);
	getRenderScene()->_logicCamera._rotation = d3d::Quaternion::identityMult();
}

void SceneObjectMenuBackground::deferredRender() {
	d3d::SceneObjectStaticModelBatcher* pBatcher = static_cast<d3d::SceneObjectStaticModelBatcher*>(_batcherRef.get());

	d3d::Matrix4x4f transform(d3d::Matrix4x4f::translateMatrix(d3d::Vec3f(0.0f, 0.0f, -9.0f)) * d3d::Matrix4x4f::rotateMatrixZ(d3d::_piOver2) * d3d::Matrix4x4f::rotateMatrixY(_t) * d3d::Matrix4x4f::scaleMatrix(d3d::Vec3f(3.0f, 3.0f, 3.0f)));

	_doubleHelix->render(pBatcher, transform);
}