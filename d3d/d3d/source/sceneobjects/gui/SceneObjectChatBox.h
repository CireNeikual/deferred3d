#pragma once

#include <network/RakWin.h>
#include <scene/SceneObject.h>
#include <scene/RenderScene.h>
#include <sceneobjects/network/SceneObjectClient.h>
#include <sceneobjects/input/SceneObjectBufferedInput.h>
#include <sceneobjects/gui/SceneObjectGUIRenderable.h>
#include <sceneobjects/gui/Font.h>
#include <scene/SceneObjectRef.h>

#include <SFML/Graphics.hpp>

#include <network/packets/PacketSendMessage.h>

class SceneObjectChatBox :
	public SceneObjectGUIRenderable
{
public:
	SceneObjectChatBox();

	void onQueue();
	void guiOnAdd();
	void synchronousUpdate(float dt);
	void guiRender(sf::RenderTexture &renderTexture);

	//Adds the message from the specified user to that chatlog.
	void addMessage(const std::string &username, const std::string &message);

	SceneObjectChatBox* copyFactory(){
		return new SceneObjectChatBox(*this);
	}

private:
	std::vector<std::string> _usernames;
	std::vector<std::string> _messages;
	unsigned int _startingMessageIndex; //Index of the message that will appear at the top of the box.
	unsigned int _messageDisplayRange; //Amount of messages to display in the box at any given time.


	d3d::SceneObjectRef _client;
	d3d::SceneObjectRef _input;

	bool _tracking;
	std::shared_ptr<std::string> _message;

	//renderables
	std::shared_ptr<Font> _font;
	std::shared_ptr<sf::Text> _guimessage;
	
	std::shared_ptr<d3d::Texture2D> _boxpanel;
};

