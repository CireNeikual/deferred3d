#pragma once

#include <scene/SceneObject.h>

#include <rendering/model/StaticModelOBJ.h>
#include <rendering/model/SceneObjectStaticModelBatcher.h>
#include <rendering/lighting/SceneObjectDirectionalLight.h>
#include <rendering/lighting/SceneObjectDirectionalLightShadowed.h>

class SceneObjectMenuBackground : public d3d::SceneObject {
private:
	d3d::SceneObjectRef _directionalLightShadowed;
	d3d::SceneObjectRef _directionalLight;

	d3d::SceneObjectRef _batcherRef;

	std::shared_ptr<d3d::StaticModelOBJ> _doubleHelix;

	float _t;

public:
	SceneObjectMenuBackground();

	// Inherited from SceneObject
	void onDestroy();
	void onAdd();
	void update(float dt);
	void deferredRender();

	SceneObjectMenuBackground* copyFactory() {
		return new SceneObjectMenuBackground(*this);
	}
};