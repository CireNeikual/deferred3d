#pragma once

#include <sceneobjects/gui/SceneObjectGUIRenderable.h>
#include <sceneobjects/gui/Font.h>

#include <sceneobjects/input/SceneObjectBufferedInput.h>

#include <rendering/texture/Texture2D.h>

class SceneObjectOptionsMenu : public SceneObjectGUIRenderable {
public:
	enum ButtonState {
		_up, _over, _down
	};

	struct Button {
		sf::FloatRect _rect;
		std::string _text;
		ButtonState _state;

		std::function<void(SceneObjectOptionsMenu &menu)> _functor;
	};

	struct CheckBox {
		sf::Vector2f _position;
		bool _isChecked;

		std::string _text;
	};

	struct CycleSelector {
		sf::Vector2f _position;
		int _cycle;
		std::vector<std::string> _cycleTexts;

		std::string _text;
	};

	struct Slider {
		sf::Vector2f _position;
		float _slideValue;

		std::string _text;
	};

	struct Tab {
		std::vector<Button> _buttons;
		std::vector<CheckBox> _checkBoxes;
		std::vector<CycleSelector> _cycleSelectors;
		std::vector<Slider> _sliders;

		std::string _text;
	};

	enum Quality {
		_low = 0, _medium, _high
	};

private:
	// For sliders
	sf::Vector2f _dragStart;
	int _draggingSliderTabIndex;
	int _draggingSliderSubIndex;
	float _startDragSliderValue;

	float _pixelScalar;

	std::shared_ptr<std::vector<sf::VideoMode>> _modes;

	d3d::SceneObjectRef _input;

	std::shared_ptr<Font> _font;

	std::shared_ptr<d3d::Texture2D> _panelTexture;

	std::shared_ptr<d3d::Texture2D> _cycleSelectorTexture;
	std::shared_ptr<d3d::Texture2D> _checkBoxTickedTexture;
	std::shared_ptr<d3d::Texture2D> _checkBoxUntickedTexture;
	std::shared_ptr<d3d::Texture2D> _sliderSlideTexture;
	std::shared_ptr<d3d::Texture2D> _sliderHandleTexture;

	std::shared_ptr<d3d::Texture2D> _button1TextureUp;
	std::shared_ptr<d3d::Texture2D> _button1TextureOver;
	std::shared_ptr<d3d::Texture2D> _button1TextureDown;

	std::vector<std::shared_ptr<d3d::Texture2D>> _tabTextures;
	std::vector<Button> _tabButtons;
	int _currentTabIndex;
	std::vector<Tab> _tabs;

	std::vector<Button> _sharedButtons;

	// Current graphics config
	unsigned int _resolutionX, _resolutionY;
	bool _windowed;
	bool _vsync;
	bool _ssao;
	bool _dof;
	bool _fxaa;

	Quality _effectsQuality;

	// Current audio config
	unsigned int _sfxVolume;
	unsigned int _musicVolume;

	static void saveSettings(SceneObjectOptionsMenu &optionsMenu);
	static void back(SceneObjectOptionsMenu &optionsMenu);

	void updateSettings();
	void loadConfig();
	void saveConfig();

public:
	SceneObjectOptionsMenu();

	// Inherited from SceneObjectGUIRenderable
	void guiOnAdd();
	void guiRender(sf::RenderTexture &renderTexture);

	void synchronousUpdate(float dt);

	SceneObjectOptionsMenu* copyFactory() {
		return new SceneObjectOptionsMenu(*this);
	}
};