#include "SceneObjectChatBox.h"


SceneObjectChatBox::SceneObjectChatBox() : _startingMessageIndex(0), _messageDisplayRange(5)
{
	_tracking = false;
	_layer = 3.0f;
	_renderMask = 0xffff;

	_message.reset(new std::string());
}

void SceneObjectChatBox::onQueue()
{
	d3d::RenderScene* scene = getRenderScene();
	_input = scene->getNamedCheckQueue("buffIn");
	_client = scene->getNamedCheckQueue("client");

	d3d::SceneObjectBufferedInput* input = static_cast<d3d::SceneObjectBufferedInput*>(_input.get());
	d3d::SceneObjectBufferedInput::Binding binding;
	binding._key = sf::Keyboard::Key::Return;
	input->setBinding("openChat", binding);
	
	//d3d::SceneObjectClient* client = static_cast<d3d::SceneObjectClient*>(_client.get());
}

void SceneObjectChatBox::addMessage(const std::string& username, const std::string &message)
{
	std::cout << "Adding message" << std::endl;
	_usernames.push_back(username);
	_messages.push_back(message);
}

void SceneObjectChatBox::guiOnAdd()
{
	std::shared_ptr<d3d::Asset> asset;

	if (!getScene()->getAssetManager("font", Font::assetFactory)->getAsset("resources/gui/fonts/pixelated.ttf", asset))
		abort();

	_font = std::static_pointer_cast<Font>(asset);

	if (!getScene()->getAssetManager("tex2D", d3d::Texture2D::assetFactory)->getAsset("resources/gui/menus/panel1.png", asset))
		abort();

	_boxpanel = std::static_pointer_cast<d3d::Texture2D>(asset);

	_guimessage.reset(new sf::Text());
	_guimessage->setFont(_font->_font);
	_guimessage->setColor(sf::Color::Black);
	_guimessage->setPosition(sf::Vector2f(50.0f, 50.0f));
	_guimessage->setCharacterSize(30);
}

void SceneObjectChatBox::guiRender(sf::RenderTexture &renderTexture)
{
	sf::Sprite panelsprite;
	panelsprite.setTexture(_boxpanel->getTexture());
	
	panelsprite.setColor(sf::Color(255, 255, 255, 50));
	//panelsprite.setScale(3.5f, 2.0f);
	panelsprite.setScale(getRenderScene()->getRenderWindow()->getSize().x / 300.0f, 2.0f);

	sf::Vector2u resolution = getRenderScene()->getRenderWindow()->getSize();
	panelsprite.setPosition(sf::Vector2f(resolution.x / 2 - (panelsprite.getTexture()->getSize().x * panelsprite.getScale().x) / 2, resolution.y / 2 - (panelsprite.getTexture()->getSize().y * panelsprite.getScale().y) / 3.0f));

	//if (_tracking)
		renderTexture.draw(panelsprite);

	for (size_t i = _startingMessageIndex; i < _messageDisplayRange && i < _usernames.size(); i++)
	{
		if (i >= _usernames.size())
			continue;

		//Draw the username
		_guimessage->setColor(sf::Color::Red);
		_guimessage->setCharacterSize(30);
		_guimessage->setPosition(panelsprite.getPosition() + sf::Vector2f(9 * panelsprite.getScale().x, (8 * panelsprite.getScale().y) + _guimessage->getFont()->getLineSpacing(_guimessage->getCharacterSize()) * i));
		_guimessage->setString(_usernames[i] + ":");
		renderTexture.draw(*_guimessage);

		//Draw the message
		_guimessage->setColor(sf::Color::White);
		_guimessage->setPosition(sf::Vector2f(_guimessage->findCharacterPos(-1).x, _guimessage->findCharacterPos(-1).y + _guimessage->getCharacterSize()/2));
		_guimessage->setCharacterSize(14);
		_guimessage->setString(_messages[i]);
		renderTexture.draw(*_guimessage);

	}

	//Draw the chat entry box.
	if (_tracking)
	{
		sf::RectangleShape rectangle;
		rectangle.setFillColor(sf::Color::White);
		rectangle.setOutlineColor(sf::Color::Black);
		rectangle.setOutlineThickness(1.0f);

		rectangle.setPosition(panelsprite.getPosition() + sf::Vector2f(9 * panelsprite.getScale().x, (panelsprite.getGlobalBounds().top - 120 + 25)));// - (8 * panelsprite.getScale().y))));
		rectangle.setSize(sf::Vector2f(panelsprite.getGlobalBounds().width - (120 - 9 * panelsprite.getScale().x), 25));
		renderTexture.draw(rectangle);

		_guimessage->setColor(sf::Color::Black);
		_guimessage->setString(*_message);
		_guimessage->setCharacterSize(14);
		_guimessage->setPosition(sf::Vector2f(rectangle.getPosition().x + 3, rectangle.getPosition().y));
		renderTexture.draw(*_guimessage);
	}
}

void SceneObjectChatBox::synchronousUpdate(float dt)
{
	d3d::SceneObjectBufferedInput* input = static_cast<d3d::SceneObjectBufferedInput*>(_input.get());
	//if (input->isBindingDown("openChat"))
	if (input->isKeyPressed(sf::Keyboard::Key::Y))
		addMessage("Fitts", "Hey, I'm just testing the message system.");
	if (input->isKeyPressed(sf::Keyboard::Key::Return))
	{
		if (_tracking)
		{
			//Only send the message if the message is longer than 0 characters.
			if (_message->size() != 0)
			{ 
				//Send message to server.
				PacketSendMessage message(*_message);
				d3d::SceneObjectClient* client = static_cast<d3d::SceneObjectClient*>(_client.get());
				client->send(message);

				_message->clear();
				_guimessage->setString(*_message);
			}

		}

		_tracking = !_tracking;
	}

	if (_tracking)
	{
		//std::cout << input->_events.size() << std::endl;
		for (size_t i = 0; i < input->_events.size(); i++)
		{
			if (input->_events[i].type == sf::Event::TextEntered)
			{
				if (input->_events[i].text.unicode == 8 && _message->size() > 1)
					_message->resize(_message->size() - 1);
				else if (input->_events[i].text.unicode == 8)
					continue;
				else if (input->_events[i].text.unicode < 127)
					_message->push_back((char)input->_events[i].text.unicode);
				//_guimessage->setString(*_message);
			}
		}
	}

}