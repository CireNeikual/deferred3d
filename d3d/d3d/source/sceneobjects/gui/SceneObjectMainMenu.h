#pragma once

#include <sceneobjects/gui/SceneObjectGUIRenderable.h>

#include <sceneobjects/gui/Font.h>

#include <sceneobjects/input/SceneObjectBufferedInput.h>

#include <rendering/texture/Texture2D.h>

class SceneObjectMainMenu : public SceneObjectGUIRenderable {
public:
	SceneObjectMainMenu() {
		_renderMask = 0xffff;
	}

	// Inherited from SceneObjectGUIRenderable
	void guiOnAdd();
	void guiRender(sf::RenderTexture &renderTexture);

	void synchronousUpdate(float dt);
	void onResize();

	SceneObjectMainMenu* copyFactory() {
		return new SceneObjectMainMenu(*this);
	}
};