#include <sceneobjects/input/SceneObjectBufferedInput.h>

using namespace d3d;

SceneObjectBufferedInput::SceneObjectBufferedInput() {
	_prevKeyStates.assign(sf::Keyboard::KeyCount, false);
	_currentKeyStates.assign(sf::Keyboard::KeyCount, false);

	_prevLMBDown = false;
	_prevRMBDown = false;
	_prevMMBDown = false;

	_currentLMBDown = false;
	_currentRMBDown = false;
	_currentMMBDown = false;

	_layer = 1.0f;
}

void SceneObjectBufferedInput::synchronousUpdate(float dt) {
	for (size_t i = 0; i < _currentKeyStates.size(); i++) {
		_prevKeyStates[i] = _currentKeyStates[i];

		_currentKeyStates[i] = sf::Keyboard::isKeyPressed(static_cast<sf::Keyboard::Key>(i));
	}

	_prevLMBDown = _currentLMBDown;
	_prevRMBDown = _currentRMBDown;
	_prevMMBDown = _currentMMBDown;

	_currentLMBDown = sf::Mouse::isButtonPressed(sf::Mouse::Left);
	_currentRMBDown = sf::Mouse::isButtonPressed(sf::Mouse::Right);
	_currentMMBDown = sf::Mouse::isButtonPressed(sf::Mouse::Middle);

	_mouseWheelDelta = 0;

	for (size_t i = 0; i < _events.size(); i++)
	if (_events[i].type == sf::Event::MouseWheelMoved)
		_mouseWheelDelta += _events[i].mouseWheel.delta;

	//_events.clear();
}

bool SceneObjectBufferedInput::isBindingDown(const std::string &name) {
	const Binding &binding = _bindings[name];

	switch (binding._type) {
	case _keyboard:
		return isKeyDown(binding._key);
	case _mouse:
		switch (binding._mouseButton) {
		case sf::Mouse::Left:
			return isLMBDown();
		case sf::Mouse::Right:
			return isRMBDown();
		case sf::Mouse::Middle:
			return isMMBDown();
		}
	}

	return false;
}

bool SceneObjectBufferedInput::wasBindingDown(const std::string &name) {
	const Binding &binding = _bindings[name];

	switch (binding._type) {
	case _keyboard:
		return wasKeyDown(binding._key);
	case _mouse:
		switch (binding._mouseButton) {
		case sf::Mouse::Left:
			return wasLMBDown();
		case sf::Mouse::Right:
			return wasRMBDown();
		case sf::Mouse::Middle:
			return wasMMBDown();
		}
	}

	return false;
}

bool SceneObjectBufferedInput::isBindingPressed(const std::string &name) {
	const Binding &binding = _bindings[name];

	switch (binding._type) {
	case _keyboard:
		return isKeyPressed(binding._key);
	case _mouse:
		switch (binding._mouseButton) {
		case sf::Mouse::Left:
			return isLMBPressed();
		case sf::Mouse::Right:
			return isRMBPressed();
		case sf::Mouse::Middle:
			return isMMBPressed();
		}
	}

	return false;
}

bool SceneObjectBufferedInput::isBindingReleased(const std::string &name) {
	const Binding &binding = _bindings[name];

	switch (binding._type) {
	case _keyboard:
		return isKeyReleased(binding._key);
	case _mouse:
		switch (binding._mouseButton) {
		case sf::Mouse::Left:
			return isLMBReleased();
		case sf::Mouse::Right:
			return isRMBReleased();
		case sf::Mouse::Middle:
			return isMMBReleased();
		}
	}

	return false;
}