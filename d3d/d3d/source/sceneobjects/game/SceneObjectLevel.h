#pragma once

#include <scene/Scene.h>

class SceneObjectLevel : public d3d::SceneObject {
private:

public:
	// Inherited from SceneObject
	void onAdd();

	SceneObject* copyFactory() {
		return new SceneObjectLevel(*this);
	}
};