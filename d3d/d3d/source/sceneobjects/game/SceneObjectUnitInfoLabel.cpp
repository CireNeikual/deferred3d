#include <sceneobjects/game/SceneObjectUnitInfoLabel.h>

#include <scene/RenderScene.h>

void SceneObjectUnitInfoLabel::guiOnAdd() {
	getRenderScene()->getRenderWindow()->setMouseCursorVisible(false);

	std::shared_ptr<d3d::Asset> asset;

	if (!getScene()->getAssetManager("tex", d3d::Texture2D::assetFactory)->getAsset("resources/gui/labels/label1.png", asset))
		abort();

	_labelBackgroundTexture = std::static_pointer_cast<d3d::Texture2D>(asset);

	_labelBackgroundTexture->bind();

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	if (!getScene()->getAssetManager("font", Font::assetFactory)->getAsset("resources/gui/fonts/pixelated.ttf", asset))
		abort();

	_font = std::static_pointer_cast<Font>(asset);
}

void SceneObjectUnitInfoLabel::guiRender(sf::RenderTexture &renderTexture) {
	if (!_enabled)
		return;

	sf::Sprite sprite;

	sprite.setTexture(_labelBackgroundTexture->getTexture());

	sf::Vector2i mousePos = sf::Mouse::getPosition(*getRenderScene()->getRenderWindow());

	sprite.setPosition(sf::Vector2f(mousePos.x + 16.0f, mousePos.y + 16.0f));
	sprite.setScale(sf::Vector2f(3.0f, 3.0f));

	renderTexture.draw(sprite);

	sf::Text text;

	text.setFont(_font->_font);
	text.setScale(sf::Vector2f(0.5f, 0.5f));

	text.setString(_unitName);

	text.setPosition(sprite.getPosition() + sf::Vector2f(16.0f, 16.0f));

	renderTexture.draw(text);

	text.setString("R:    " + std::to_string(_reward));

	text.setPosition(sprite.getPosition() + sf::Vector2f(16.0f, 32.0f));

	renderTexture.draw(text);

	text.setString("HP:   " + std::to_string(_hp));

	text.setPosition(sprite.getPosition() + sf::Vector2f(16.0f, 48.0f));

	renderTexture.draw(text);
}

void SceneObjectUnitInfoLabel::synchronousUpdate(float dt) {

}