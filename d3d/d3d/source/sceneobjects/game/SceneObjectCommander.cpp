#include <sceneobjects/game/SceneObjectCommander.h>

#include <SFML/Graphics.hpp>

#include <util/Math.h>

#include <sceneobjects/physics/SceneObjectPhysicsWorld.h>

#include <sceneobjects/units/SceneObjectBattlefieldController.h>

#include <sceneobjects/units/SceneObjectUnit.h>
#include <sceneobjects/units/SceneObjectArmyController.h>

#include <sceneobjects/units/SceneObjectSpawner.h>

#include <sceneobjects/units/SceneObjectUnitMediumTank.h>

class BodyCast : public btCollisionWorld::ClosestRayResultCallback {
private:
	float _fieldLevel;

public:
	SceneObjectUnit* _pUnit;

	BodyCast(float fieldLevel, const btVector3 &fromWorld, const btVector3 &toWorld)
		: btCollisionWorld::ClosestRayResultCallback(fromWorld, toWorld), _pUnit(nullptr), _fieldLevel(fieldLevel)
	{}

	btScalar addSingleResult(btCollisionWorld::LocalRayResult &rayResult, bool normalInWorldSpace) {
		if (_pUnit == nullptr && rayResult.m_collisionObject->getUserPointer() != nullptr) {
			d3d::SceneObject* pSceneObject = static_cast<d3d::SceneObject*>(static_cast<d3d::SceneObjectRef*>(rayResult.m_collisionObject->getUserPointer())->get());

			if (pSceneObject->_tag == "unit") {
				_pUnit = static_cast<SceneObjectUnit*>(pSceneObject);

				return ClosestRayResultCallback::addSingleResult(rayResult, normalInWorldSpace);
			}
		}

		m_hitPointWorld = m_rayFromWorld + rayResult.m_hitFraction * (m_rayToWorld - m_rayFromWorld);

		if (std::fabsf(m_hitPointWorld.getY() - _fieldLevel + 0.5f) < 0.01f)
			return ClosestRayResultCallback::addSingleResult(rayResult, normalInWorldSpace);

		return 1.0f;
	}
};

SceneObjectCommander::SceneObjectCommander()
: _panRate(6.0f), _operatingHeight(8.0f), _fovy(d3d::_piOver4),
_translation(0.0f, 0.0f),//_mapBoundsIncreaseMultiplier(1.3f)
_cameraBounds(d3d::Vec2f(-9.0f, -1.5f), d3d::Vec2f(9.0f, 7.0f)),
_edgeSize(8.0f), _activeTab(0),
_currentButtonIndex(-1), _currentButtonState(0),
_currentMode(_default)
{
	_rotation = d3d::Quaternion(d3d::_piOver4 * 0.4f - d3d::_piOver2 , d3d::Vec3f(1.0f, 0.0f, 0.0f));

	_renderMask = 0xffff;
}

void SceneObjectCommander::create(const std::shared_ptr<TrileMap> &map, const d3d::SceneObjectRef &armyController) {
	_map = map;
	_armyController = armyController;
}

bool SceneObjectCommander::isSpawnerValid(SceneObjectSpawner* pSpawner) {
	SceneObjectArmyController* pArmyController = static_cast<SceneObjectArmyController*>(_armyController.get());

	std::vector<d3d::SceneObjectRef> sceneObjects;

	getScene()->_octree.queryRegion(pSpawner->getAABB(), sceneObjects);

	for (size_t i = 0; i < sceneObjects.size(); i++)
		if (sceneObjects[i]->_tag == "spawner" && sceneObjects[i].get() != pSpawner)
			return false;

	if (pSpawner->getAABB()._upperBound.z > pArmyController->getBattlefieldController()->getLaneWidth() * 0.5f ||
		pSpawner->getAABB()._lowerBound.z < -pArmyController->getBattlefieldController()->getLaneWidth() * 0.5f)
		return false;

	d3d::SceneObjectPhysicsWorld* pPhysicsWorld = static_cast<d3d::SceneObjectPhysicsWorld*>(_physicsWorld.get());

	if (pSpawner->_position.y > pArmyController->getBattlefieldController()->getFieldLevel() - 0.001f && pSpawner->_position.y < pArmyController->getBattlefieldController()->getFieldLevel() + 0.001f)
		return true;

	return false;
}

void SceneObjectCommander::guiOnAdd() {
	_input = getScene()->getNamed("buffIn");

	assert(_input.isAlive());

	_physicsWorld = getScene()->getNamed("physWrld");

	assert(_physicsWorld.isAlive());

	// -------------------------------- Set up GUI --------------------------------

	_pageWidth = 256;
	_pageHeight = 64;

	_pixelScalar = static_cast<float>(getRenderScene()->_gBuffer.getWidth()) / static_cast<float>(_pageWidth);

	_scaledPageWidth = _pageWidth * _pixelScalar;
	_scaledPageHeight = _pageHeight * _pixelScalar;

	std::shared_ptr<SceneObjectUnitInfoLabel> infoLabel(new SceneObjectUnitInfoLabel());

	getRenderScene()->add(infoLabel, false);

	_infoLabel = infoLabel.get();

	_pages[0].reset(new sf::Texture());
	_pages[0]->loadFromFile("resources/gui/game/page1.png");
	_pages[0]->setSmooth(false);

	_pages[1].reset(new sf::Texture());
	_pages[1]->loadFromFile("resources/gui/game/page2.png");
	_pages[1]->setSmooth(false);

	_pages[2].reset(new sf::Texture());
	_pages[2]->loadFromFile("resources/gui/game/page3.png");
	_pages[2]->setSmooth(false);

	_pageButtons[0] = sf::FloatRect(0.0f, getRenderScene()->_gBuffer.getHeight() - _pixelScalar * 39.0f, 30.0f * _pixelScalar, 7.0f * _pixelScalar);
	_pageButtons[1] = sf::FloatRect(30.0f * _pixelScalar, getRenderScene()->_gBuffer.getHeight() - _pixelScalar * 39.0f, 30.0f * _pixelScalar, 7.0f * _pixelScalar);
	_pageButtons[2] = sf::FloatRect(60.0f * _pixelScalar, getRenderScene()->_gBuffer.getHeight() - _pixelScalar * 39.0f, 30.0f * _pixelScalar, 7.0f * _pixelScalar);

	_pageTexts[0] = "Building";
	_pageTexts[1] = "Invest";
	_pageTexts[2] = "Evo";

	std::shared_ptr<d3d::Asset> asset;

	getScene()->getAssetManager("font", Font::assetFactory)->getAsset("resources/gui/fonts/pixelated.ttf", asset);

	_font = std::static_pointer_cast<Font>(asset);

	getScene()->getAssetManager("tex", d3d::Texture2D::assetFactory)->getAsset("resources/gui/game/separator.png", asset);

	_separatorTexture = std::static_pointer_cast<d3d::Texture2D>(asset);

	_separatorTexture->bind();

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	getScene()->getAssetManager("tex", d3d::Texture2D::assetFactory)->getAsset("resources/gui/game/button1up.png", asset);

	_button1TextureUp = std::static_pointer_cast<d3d::Texture2D>(asset);

	_button1TextureUp->bind();

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	getScene()->getAssetManager("tex", d3d::Texture2D::assetFactory)->getAsset("resources/gui/game/button1over.png", asset);

	_button1TextureOver = std::static_pointer_cast<d3d::Texture2D>(asset);

	_button1TextureOver->bind();

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	getScene()->getAssetManager("tex", d3d::Texture2D::assetFactory)->getAsset("resources/gui/game/button1down.png", asset);

	_button1TextureDown = std::static_pointer_cast<d3d::Texture2D>(asset);

	_button1TextureDown->bind();

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	sf::Texture::bind(nullptr);

	_buttons.reset(new std::array<std::vector<sf::FloatRect>, 3>());

	// Add buttons
	float smallButtonScaledWidth = 27.0f * _pixelScalar;
	float smallButtonScaledHeight = 9.0f * _pixelScalar;

	sf::FloatRect scoutButton(4.0f * _pixelScalar, getRenderScene()->_gBuffer.getHeight() - _pixelScalar * 22.0f, smallButtonScaledWidth, smallButtonScaledHeight);
	(*_buttons)[0].push_back(scoutButton);

	sf::FloatRect dozerButton(8.0f * _pixelScalar + smallButtonScaledWidth, getRenderScene()->_gBuffer.getHeight() - _pixelScalar * 22.0f, smallButtonScaledWidth, smallButtonScaledHeight);
	(*_buttons)[0].push_back(dozerButton);
}

void SceneObjectCommander::update(float dt) {
	
}

void SceneObjectCommander::buttonActivated(unsigned char tab, size_t buttonIndex) {
	switch (tab) {
	case 0: // First tab
		{
			switch (buttonIndex) {
			case 0: // Scout button
				{
					_currentMode = _place;

					std::shared_ptr<SceneObjectSpawner> spawner(new SceneObjectSpawner());

					getScene()->add(spawner, true);

					std::shared_ptr<d3d::Asset> asset;

					getScene()->getAssetManager("trile", Trile::assetFactory)->getAsset("resources/trixel/units/tier2/tankSpawner.trile resources/trixel/units/tier2/tankSpawner.png", asset);

					spawner->create(std::static_pointer_cast<Trile>(asset), static_cast<SceneObjectArmyController*>(_armyController.get()), SceneObjectUnitMediumTank::unitFactory);

					_placeSpawnerRef = spawner.get();
				}

				break;

			case 1: // Dozer button
				{
					//_currentMode = _place;
				}

				break;
			}
		}

		break;

	case 1: // Second tab
		{

		}

		break;

	case 2: // Third tab
		{

		}

		break;
	}
}

void SceneObjectCommander::synchronousUpdate(float dt) {
	d3d::SceneObjectBufferedInput* pInput = static_cast<d3d::SceneObjectBufferedInput*>(_input.get());

	SceneObjectArmyController* pArmyController = static_cast<SceneObjectArmyController*>(_armyController.get());

	SceneObjectUnitInfoLabel* pInfoLabel = static_cast<SceneObjectUnitInfoLabel*>(_infoLabel.get());

	pInfoLabel->_enabled = false;

	sf::Vector2i mousePos = sf::Mouse::getPosition(*getRenderScene()->getRenderWindow());

	_hoverUnit = nullptr;

	d3d::SceneObjectPhysicsWorld* pPhysicsWorld = static_cast<d3d::SceneObjectPhysicsWorld*>(_physicsWorld.get());

	// Ray cast
	if (mousePos.y < getRenderScene()->_gBuffer.getHeight() - _pixelScalar * 32.0f) {
		d3d::Vec4f homogenous(2.0f * static_cast<float>(mousePos.x) / static_cast<float>(getRenderScene()->getRenderWindow()->getSize().x) - 1.0f,
			2.0f * (1.0f - static_cast<float>(mousePos.y) / static_cast<float>(getRenderScene()->getRenderWindow()->getSize().y)) - 1.0f,
			-1.0f, 1.0f);

		d3d::Vec4f world = getScene()->_logicCamera.getProjectionViewInverseMatrix() * homogenous;

		float wInv = 1.0f / world.w;
		world.x *= wInv;
		world.y *= wInv;
		world.z *= wInv;

		d3d::Vec3f dir = d3d::Vec3f(world.x, world.y, world.z) - getScene()->_logicCamera._position;
		dir.normalize();

		btVector3 fromWorld = bt(getScene()->_logicCamera._position);
		btVector3 toWorld = bt(getScene()->_logicCamera._position) + bt(dir) * 1000.0f;

		BodyCast callback(pArmyController->getBattlefieldController()->getFieldLevel(), fromWorld, toWorld);

		pPhysicsWorld->_pDynamicsWorld->rayTest(fromWorld, toWorld, callback);

		if (callback.hasHit()) {
			if (_currentMode == _place) {
				assert(_placeSpawnerRef != nullptr);

				SceneObjectSpawner* pSpawner = static_cast<SceneObjectSpawner*>(_placeSpawnerRef.get());

				if (pInput->isLMBPressed()) {
					if (isSpawnerValid(pSpawner)) {
						pArmyController->addSpawner(pSpawner);

						pSpawner->_placed = true;

						_placeSpawnerRef = nullptr;

						_currentMode = _default;
					}
					else {
						// TODO: Make beeping sound or something
					}
				}
				else {
					pSpawner->_position = getScene()->_logicCamera._position + dir * callback.m_closestHitFraction * 1000.0f;

					pSpawner->_position.x = static_cast<int>(pSpawner->_position.x * 4.0f) / 4.0f;
					pSpawner->_position.z = static_cast<int>(pSpawner->_position.z * 4.0f) / 4.0f;
					pSpawner->_position.y += 0.5f;

					pSpawner->calculateAABB();
				}
			}
			else {
				if (callback._pUnit != nullptr)
					_hoverUnit = callback._pUnit;
			}
		}
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		_translation += d3d::Vec2f(0.0f, -_panRate) * dt;
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		_translation += d3d::Vec2f(0.0f, _panRate) * dt;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		_translation += d3d::Vec2f(_panRate, 0.0f) * dt;
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		_translation += d3d::Vec2f(-_panRate, 0.0f) * dt;

	if (mousePos.x < _edgeSize)
		_translation += d3d::Vec2f(-_panRate, 0.0f) * dt;
	else if (mousePos.x > getRenderScene()->getRenderWindow()->getSize().x - _edgeSize)
		_translation += d3d::Vec2f(_panRate, 0.0f) * dt;

	if (mousePos.y < _edgeSize)
		_translation += d3d::Vec2f(0.0f, -_panRate) * dt;
	else if (mousePos.y > getRenderScene()->getRenderWindow()->getSize().y - _edgeSize)
		_translation += d3d::Vec2f(0.0f, _panRate) * dt;

	if (_translation.x < _cameraBounds._lowerBound.x)
		_translation.x = _cameraBounds._lowerBound.x;
	if (_translation.y < _cameraBounds._lowerBound.y)
		_translation.y = _cameraBounds._lowerBound.y;
	if (_translation.x > _cameraBounds._upperBound.x)
		_translation.x = _cameraBounds._upperBound.x;
	if (_translation.y > _cameraBounds._upperBound.y)
		_translation.y = _cameraBounds._upperBound.y;

	getScene()->_logicCamera._position = d3d::Vec3f(_translation.x, _operatingHeight, _translation.y);
	getScene()->_logicCamera._rotation = _rotation;

	if (pInput->isLMBReleased() && _currentMode == _default) {
		for (unsigned char i = 0; i < _pageButtons.size(); i++) {
			if (_pageButtons[i].contains(sf::Vector2f(static_cast<float>(mousePos.x), static_cast<float>(mousePos.y)))) {
				_activeTab = i;
				break;
			}
		}
	}

	_currentButtonIndex = -1;
	_currentButtonState = 0;

	if (_currentMode == _default) {
		for (size_t i = 0; i < (*_buttons)[_activeTab].size(); i++) {
			sf::FloatRect rect = (*_buttons)[_activeTab][i];

			if (rect.contains(sf::Vector2f(static_cast<float>(mousePos.x), static_cast<float>(mousePos.y)))) {
				_currentButtonIndex = i;

				if (pInput->isLMBReleased())
					buttonActivated(_activeTab, i);

				if (pInput->isLMBDown())
					_currentButtonState = 2;
				else
					_currentButtonState = 1;

				break;
			}
		}
	}

	if (_hoverUnit != nullptr && _currentMode == _default) {
		SceneObjectUnit* pUnit = static_cast<SceneObjectUnit*>(_hoverUnit.get());
		
		if (pUnit->getSide() == pArmyController->getSide()) {
			pUnit->_reward += pInput->getMouseWheelDelta();

			if (pInput->isLMBPressed())
				pUnit->_reward++;
			else if (pInput->isRMBPressed())
				pUnit->_reward--;

			pInfoLabel->_enabled = true;

			pInfoLabel->_unitName = pUnit->getUnitName();
			pInfoLabel->_hp = static_cast<int>(pUnit->getHP());
			pInfoLabel->_reward = static_cast<int>(pUnit->_reward);
		}
	}

	sf::Listener::setPosition(sf::Vector3f(getScene()->_logicCamera._position.x, getScene()->_logicCamera._position.y, getScene()->_logicCamera._position.z));

	d3d::Vec3f forward = getScene()->_logicCamera._rotation * d3d::Vec3f(0.0f, 0.0f, -1.0f);

	sf::Listener::setDirection(sf::Vector3f(forward.x, forward.y, forward.z));
}

void SceneObjectCommander::guiRender(sf::RenderTexture &renderTexture) {
	sf::Sprite sprite;
	sprite.setScale(sf::Vector2f(_pixelScalar, _pixelScalar));

	sprite.setPosition(sf::Vector2f(0.0f, renderTexture.getSize().y - _scaledPageHeight));

	for (unsigned char i = 0; i < _pages.size(); i++) {
		if (i != _activeTab) {
			sprite.setTexture(*_pages[i]);

			sprite.setColor(sf::Color(200, 200, 200));

			renderTexture.draw(sprite);
		}
	}

	sprite.setColor(sf::Color::White);

	sprite.setTexture(*_pages[_activeTab]);

	renderTexture.draw(sprite);

	sf::Text text;
	text.setFont(_font->_font);
	text.setScale(sf::Vector2f(0.7f, 0.7f));

	for (unsigned char i = 0; i < _pageTexts.size(); i++) {
		if (i != _activeTab)
			text.setColor(sf::Color(200, 200, 200));
		else
			text.setColor(sf::Color(255, 255, 255));

		text.setString(_pageTexts[i]);

		text.setPosition(sf::Vector2f(_pageButtons[i].left + 3.0f * _pixelScalar, _pageButtons[i].top + 1.0f * _pixelScalar));

		renderTexture.draw(text);
	}

	text.setColor(sf::Color(255, 255, 255));

	sprite.setTextureRect(sf::IntRect(0, 0, 32, 16));

	// Render active tab buttons
	for (size_t i = 0; i < (*_buttons)[_activeTab].size(); i++) {
		sf::FloatRect rect = (*_buttons)[_activeTab][i];

		sprite.setPosition(sf::Vector2f(rect.left, rect.top));

		if (i != _currentButtonIndex)
			sprite.setTexture(_button1TextureUp->getTexture());
		else {
			if (_currentButtonState == 1)
				sprite.setTexture(_button1TextureOver->getTexture());
			else
				sprite.setTexture(_button1TextureDown->getTexture());
		}
		
		renderTexture.draw(sprite);

		text.setPosition(sf::Vector2f(rect.left + 15.5f * _pixelScalar, rect.top + 4.5f * _pixelScalar));

		switch (i) {
		case 0:
			text.setString("Scout $10");
			break;
		case 1:
			text.setString("Dozer $10");
			break;
		}

		text.setPosition(sf::Vector2f(text.getPosition().x - 0.5f * text.getLocalBounds().width, text.getPosition().y - 0.5f * text.getLocalBounds().height));

		renderTexture.draw(text);
	}

	switch (_activeTab) {
	case 0:
		text.setPosition(sf::Vector2f(28.0f * _pixelScalar, renderTexture.getSize().y - 30.0f * _pixelScalar));
		text.setString("Tier 1");

		renderTexture.draw(text);

		sprite.setTexture(_separatorTexture->getTexture());
		sprite.setTextureRect(sf::IntRect(0, 0, 1, 32));

		sprite.setPosition(sf::Vector2f(66.0f * _pixelScalar, renderTexture.getSize().y - 32.0f * _pixelScalar));

		renderTexture.draw(sprite);

		break;
	}
}