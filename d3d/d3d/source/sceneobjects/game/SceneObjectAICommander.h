#pragma once

#include <scene/Scene.h>

#include <constructs/AABB2D.h>

#include <sceneobjects/trixel/TrileMap.h>

#include <sceneobjects/gui/SceneObjectGUI.h>
#include <sceneobjects/gui/SceneObjectGUIRenderable.h>

#include <sceneobjects/game/SceneObjectUnitInfoLabel.h>

class SceneObjectAICommander : public d3d::SceneObject {
private:
	std::shared_ptr<TrileMap> _map;

	d3d::SceneObjectRef _physicsWorld;

	d3d::SceneObjectRef _armyController;

public:
	unsigned int _money;

	SceneObjectAICommander();

	void create(const std::shared_ptr<TrileMap> &map, const d3d::SceneObjectRef &armyController);

	// Inherited from SceneObject
	void onAdd();
	void update(float dt);
	void synchronousUpdate(float dt);

	d3d::SceneObject* copyFactory() {
		return new SceneObjectAICommander(*this);
	}
};