#include <sceneobjects/game/SceneObjectAICommander.h>

#include <SFML/Graphics.hpp>

#include <util/Math.h>

#include <sceneobjects/physics/SceneObjectPhysicsWorld.h>

#include <sceneobjects/units/SceneObjectBattlefieldController.h>

#include <sceneobjects/units/SceneObjectUnit.h>
#include <sceneobjects/units/SceneObjectArmyController.h>

#include <sceneobjects/units/SceneObjectSpawner.h>

#include <sceneobjects/units/SceneObjectUnitMediumTank.h>

SceneObjectAICommander::SceneObjectAICommander()
{}

void SceneObjectAICommander::create(const std::shared_ptr<TrileMap> &map, const d3d::SceneObjectRef &armyController) {
	_map = map;
	_armyController = armyController;
}

void SceneObjectAICommander::onAdd() {
	_physicsWorld = getScene()->getNamed("physWrld");

	assert(_physicsWorld.isAlive());

	SceneObjectArmyController* pArmyController = static_cast<SceneObjectArmyController*>(_armyController.get());

	for (size_t i = 0; i < 5; i++) {
		std::shared_ptr<SceneObjectSpawner> spawner(new SceneObjectSpawner());

		getScene()->add(spawner, true);

		std::shared_ptr<d3d::Asset> asset;

		getScene()->getAssetManager("trile", Trile::assetFactory)->getAsset("resources/trixel/units/tier2/tankSpawner.trile resources/trixel/units/tier2/tankSpawner.png", asset);

		spawner->create(std::static_pointer_cast<Trile>(asset), static_cast<SceneObjectArmyController*>(_armyController.get()), SceneObjectUnitMediumTank::unitFactory);

		pArmyController->addSpawner(spawner.get());

		spawner->_position = d3d::Vec3f(9.0f, pArmyController->getBattlefieldController()->getFieldLevel(), (static_cast<float>(i) - 2.0f) * 1.0f);

		spawner->_placed = true;

		spawner->calculateAABB();
	}
}

void SceneObjectAICommander::update(float dt) {

}

void SceneObjectAICommander::synchronousUpdate(float dt) {
	SceneObjectArmyController* pArmyController = static_cast<SceneObjectArmyController*>(_armyController.get());

	//d3d::SceneObjectPhysicsWorld* pPhysicsWorld = static_cast<d3d::SceneObjectPhysicsWorld*>(_physicsWorld.get());

	for (size_t i = 0; i < pArmyController->_waves.size(); i++) {
		for (size_t j = 0; j < pArmyController->_waves[i]._units.size(); j++) {
			if (pArmyController->_waves[i]._units[j] != nullptr && pArmyController->_waves[i]._units[j].isAlive()) {
				SceneObjectUnit* pUnit = static_cast<SceneObjectUnit*>(pArmyController->_waves[i]._units[j].get());

				if (pUnit->created()) {
					float positionFactor = pUnit->getPosition().x * (pArmyController->getSide() == 0 ? 1.0f : -1.0f);

					pUnit->_reward = 0.5f * positionFactor;
				}
			}
		}
	}
}