#pragma once

#include <sceneobjects/gui/SceneObjectGUIRenderable.h>
#include <sceneobjects/gui/Font.h>

#include <rendering/texture/Texture2D.h>

class SceneObjectUnitInfoLabel : public SceneObjectGUIRenderable {
private:
	std::shared_ptr<d3d::Texture2D> _labelBackgroundTexture;

	std::shared_ptr<Font> _font;

public:
	bool _enabled;

	// Statistics to display
	std::string _unitName;
	int _reward;
	int _hp;

	SceneObjectUnitInfoLabel()
		: _unitName("Unset"),
		_reward(0.0f),
		_hp(100)
	{
		_renderMask = 0xffff;
	}

	// Inherited from SceneObjectGUIRenderable
	void guiOnAdd();
	void guiRender(sf::RenderTexture &renderTexture);

	void synchronousUpdate(float dt);

	SceneObjectUnitInfoLabel* copyFactory() {
		return new SceneObjectUnitInfoLabel(*this);
	}
};