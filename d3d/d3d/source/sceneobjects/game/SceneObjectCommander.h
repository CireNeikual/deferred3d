#pragma once

#include <scene/Scene.h>

#include <constructs/AABB2D.h>

#include <sceneobjects/trixel/TrileMap.h>

#include <sceneobjects/gui/SceneObjectGUI.h>
#include <sceneobjects/gui/SceneObjectGUIRenderable.h>

#include <sceneobjects/game/SceneObjectUnitInfoLabel.h>

#include <SFML/Audio.hpp>

class SceneObjectCommander : public SceneObjectGUIRenderable {
public:
	enum Mode {
		_default,
		_place
	};

private:
	Mode _currentMode;

	d3d::SceneObjectRef _placeSpawnerRef;

	std::shared_ptr<TrileMap> _map;

	d3d::SceneObjectRef _input;

	d3d::SceneObjectRef _physicsWorld;

	d3d::SceneObjectRef _armyController;

	// Control
	d3d::SceneObjectRef _hoverUnit;

	d3d::SceneObjectRef _infoLabel;

	// GUI
	std::array<std::shared_ptr<sf::Texture>, 3> _pages;
	std::array<sf::FloatRect, 3> _pageButtons;
	std::array<std::string, 3> _pageTexts;

	std::shared_ptr<Font> _font;

	std::shared_ptr<d3d::Texture2D> _separatorTexture;

	std::shared_ptr<d3d::Texture2D> _button1TextureUp;
	std::shared_ptr<d3d::Texture2D> _button1TextureOver;
	std::shared_ptr<d3d::Texture2D> _button1TextureDown;

	std::shared_ptr<std::array<std::vector<sf::FloatRect>, 3>> _buttons;

	int _currentButtonIndex;
	int _currentButtonState;

	unsigned char _activeTab;

	unsigned int _pageWidth, _pageHeight;
	float _pixelScalar;
	float _scaledPageWidth, _scaledPageHeight;

	void buttonActivated(unsigned char tab, size_t buttonIndex);
	bool isSpawnerValid(class SceneObjectSpawner* pSpawner);

public:
	unsigned int _money;

	float _operatingHeight;

	d3d::Vec2f _translation;

	d3d::Quaternion _rotation;

	float _panRate;

	float _fovy;

	float _edgeSize;

	d3d::AABB2D _cameraBounds;

	SceneObjectCommander();

	void create(const std::shared_ptr<TrileMap> &map, const d3d::SceneObjectRef &armyController);

	// Inherited from SceneObject
	void guiOnAdd();
	void update(float dt);
	void synchronousUpdate(float dt);

	void guiRender(sf::RenderTexture &renderTexture);

	d3d::SceneObject* copyFactory() {
		return new SceneObjectCommander(*this);
	}
};