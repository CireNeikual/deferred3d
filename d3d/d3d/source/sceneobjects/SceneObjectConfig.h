#pragma once

#include <scene/SceneObject.h>

class SceneObjectConfig : public d3d::SceneObject {
public:
	enum EffectsQuality {
		_low = 0, _medium, _high
	};

private:
	bool _originalWindowed;

	EffectsQuality _originalEffectsQuality;

public:
	unsigned int _resolutionWidth, _resolutionHeight;
	bool _windowed;
	bool _vsync;
	bool _ssao;
	bool _dof;
	bool _fxaa;
	EffectsQuality _effectsQuality;

	unsigned int _sfxVolume;
	unsigned int _musicVolume;

	bool load(const std::string &fileName);

	void changed();

	SceneObjectConfig* copyFactory() {
		return new SceneObjectConfig(*this);
	}
};