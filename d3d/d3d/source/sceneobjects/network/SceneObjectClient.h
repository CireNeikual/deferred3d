#pragma once

#include <network/RakWin.h>
#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>
#include <RakNetTypes.h>

#include <scene/Scene.h>
#include <network/PacketList.h>

namespace d3d
{
	class SceneObjectClient :
		public d3d::SceneObject
	{
	public:
		SceneObjectClient();

		//Inherited from d3d::SceneObject
		void onAdd();
		void onDestroy();
		void preSynchronousUpdate(float dt); //Process all incoming packets.

		void connect(const std::string &ip, unsigned short port, RakNet::PublicKey* publickey = nullptr);
		void send(d3d::Packet &packet);

		bool isConnected();

		SceneObject* copyFactory(){
			return new SceneObjectClient(*this);
		}

	private:
		std::shared_ptr<RakNet::RakPeerInterface> _peer;
		std::shared_ptr<RakNet::SocketDescriptor> _sd;

		bool _connected;
	};
}

