#pragma once

#include <network/RakWin.h>
#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>
#include <RakNetTypes.h>

#include <scene/SceneObject.h>
#include <network/PacketList.h>

namespace d3d
{
	class SceneObjectGameServer :
		public d3d::SceneObject
	{
	public:
		//Inherited from SceneObject
		void onQueue();
		void onAdd();
		void onDestroy();
		void preSynchronousUpdate(float dt);

		void start(unsigned short port, unsigned short maxConnections);
		void send(d3d::Packet &packet, RakNet::AddressOrGUID recipiant); //send packet to specific client.
		void send(d3d::Packet &packet); //send packet to all connected clients.

		const int getConnectedCount() const;
		const bool isStarted() const;

		SceneObject* copyFactory(){
			return new SceneObjectGameServer(*this);
		}

	private:
		std::shared_ptr<RakNet::RakPeerInterface> _peer;
		std::shared_ptr<RakNet::SocketDescriptor> _sd;

		int _connectedcount;
		bool _listening;
	};
}
