#include "SceneObjectGameServer.h"


void d3d::SceneObjectGameServer::start(unsigned short port, unsigned short maxConnections)
{
	_sd.reset(new RakNet::SocketDescriptor(port, NULL));
	_peer.reset(RakNet::RakPeerInterface::GetInstance());
	if (_peer->Startup(5, _sd.get(), 1) == RakNet::StartupResult::RAKNET_STARTED)
		std::cout << "Server successfully started on port " << port << " with an allowance of " << maxConnections << " connections." << std::endl;
	else
		std::cerr << "There was an error attempting to start the server on port " << port << " with an allowance of  " << maxConnections << " connections." << std::endl;
	_peer->SetMaximumIncomingConnections(maxConnections);

	_listening = true; //Start processing the message queue.
}

void d3d::SceneObjectGameServer::send(d3d::Packet &packet, RakNet::AddressOrGUID recipiant)
{
	RakNet::BitStream bs;
	packet.WriteToMessage(bs);

	_peer->Send(&bs, packet._priority, packet._reliability, packet._channel, recipiant, false);
}

void d3d::SceneObjectGameServer::send(d3d::Packet &packet)
{
	RakNet::BitStream bs;
	packet.WriteToMessage(bs);

	_peer->Send(&bs, packet._priority, packet._reliability, packet._channel, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void d3d::SceneObjectGameServer::onQueue()
{
	_listening = false;
}

void d3d::SceneObjectGameServer::onAdd()
{
	_layer = -9999;
}

void d3d::SceneObjectGameServer::onDestroy()
{
	RakNet::RakPeerInterface::DestroyInstance(_peer.get());
}

void d3d::SceneObjectGameServer::preSynchronousUpdate(float dt)
{
	if (!_listening) //Check to make sure the server is running before processing the message queue.
		return;

	//std::cout << "testing to see if the update function is being called." << std::endl;

	RakNet::Packet* packet;

	//Iterate through packet queue
	for (packet = _peer->Receive(); packet; _peer->DeallocatePacket(packet), packet = _peer->Receive())
	{
		std::cout << "Server received arbitrary data." << std::endl;
		switch (packet->data[0])
		{
		case ID_NEW_INCOMING_CONNECTION:
			std::cout << "Server received connection attempt. " << std::endl;
			break;
		default:
			std::cout << "Server received packet with id " << (int)packet->data[0] << std::endl;
			Packet::Ptr formpack = PacketList::ConstructPacket(packet->data[0]);

			if (formpack == nullptr)
				break; //Packet header was malformed, the response is to silenty discard.

			std::cout << "Packet not discarded" << std::endl;

			formpack->_scene = getScene();
			formpack->_server = getThis();
			formpack->_client = nullptr;
			formpack->_sender = packet->guid;

			RakNet::BitStream bitstream(packet->data, packet->length, false);
			bitstream.IgnoreBytes(sizeof(unsigned char));

			if (formpack->ReadFromMessage(bitstream))
				formpack->ProcessServer();
			else
				break; //Packet was malformed, silenty discard.

		}
	}
}

const int d3d::SceneObjectGameServer::getConnectedCount() const
{
	return _connectedcount;
}

const bool d3d::SceneObjectGameServer::isStarted() const
{
	return _listening;
}