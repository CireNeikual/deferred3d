#include "SceneObjectClient.h"


d3d::SceneObjectClient::SceneObjectClient()
{
	_connected = false;
	_peer.reset(RakNet::RakPeerInterface::GetInstance());
	_sd.reset(new RakNet::SocketDescriptor());

	_peer->Startup(1, _sd.get(), 1);
}

void d3d::SceneObjectClient::onAdd()
{
	std::cout << "adding" << std::endl;

	_layer = -999; //Increase priority so received packets can be processed before the rest of the frame.
}

void d3d::SceneObjectClient::onDestroy()
{
	RakNet::RakPeerInterface::DestroyInstance(_peer.get());
}


void d3d::SceneObjectClient::connect(const std::string &ip, unsigned short port, RakNet::PublicKey* publickey)
{
	std::cout << "Attempting to connect to " << ip << " on port " << port << "..." << std::endl;
	_peer->Connect(ip.c_str(), port, NULL, 0);
}

void d3d::SceneObjectClient::send(d3d::Packet &packet)
{
	RakNet::BitStream bs;
	packet.WriteToMessage(bs);

	_peer->Send(&bs, packet._priority, packet._reliability, packet._channel, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void d3d::SceneObjectClient::preSynchronousUpdate(float dt)
{
	RakNet::Packet* packet;

	//Iterate through packet queue
	for (packet = _peer->Receive(); packet; _peer->DeallocatePacket(packet), packet = _peer->Receive())
	{
		switch (packet->data[0])
		{
		case ID_CONNECTION_REQUEST_ACCEPTED:
			_connected = true;
			std::cout << "Successfully connected to the server." << std::endl;
		default:
			d3d::Packet::Ptr formpack = PacketList::ConstructPacket(packet->data[0]);

			if (formpack == nullptr)
				break; //Packet header was malformed, the response is to silenty discard.

			formpack->_scene = getScene();
			formpack->_client = getThis();
			formpack->_server = nullptr;
			formpack->_sender = packet->guid;

			RakNet::BitStream bitstream(packet->data, packet->length, false);
			bitstream.IgnoreBytes(sizeof(unsigned char));

			if (formpack->ReadFromMessage(bitstream))
				formpack->ProcessClient();
			else
				break; //Packet was malformed, silenty discard.

		}
	}
}

bool d3d::SceneObjectClient::isConnected()
{
	return _connected;
}