#include <sceneobjects/SceneObjectConfig.h>

#include <scene/RenderScene.h>

#include <rendering/imageeffects/SceneObjectEffectBuffer.h>
#include <rendering/imageeffects/SceneObjectSSAO.h>
#include <rendering/imageeffects/SceneObjectLightScattering.h>
#include <rendering/imageeffects/SceneObjectDepthOfField.h>
#include <rendering/imageeffects/SceneObjectSSR.h>
#include <rendering/imageeffects/SceneObjectFXAA.h>
#include <rendering/imageeffects/SceneObjectFog.h>

#include <util/Math.h>

#include <fstream>

bool SceneObjectConfig::load(const std::string &fileName) {
	std::ifstream is(fileName);

	if (!is.is_open())
		return false;

	std::string placeholder, value;

	// Current graphics config
	is >> placeholder >> _resolutionWidth >> _resolutionHeight;

	is >> placeholder >> value;

	_originalWindowed = _windowed = value == "true";

	is >> placeholder >> value;

	_vsync = value == "true";

	is >> placeholder >> value;

	_ssao = value == "true";

	is >> placeholder >> value;

	_dof = value == "true";

	is >> placeholder >> value;

	_fxaa = value == "true";

	is >> placeholder >> value;

	if (value == "low")
		_originalEffectsQuality = _effectsQuality = _low;
	else if (value == "medium")
		_originalEffectsQuality = _effectsQuality = _medium;
	else
		_originalEffectsQuality = _effectsQuality = _high;

	// Current audio config
	is >> placeholder >> _sfxVolume;

	is >> placeholder >> _musicVolume;

	is.close();

	return true;
}

void SceneObjectConfig::changed() {
	// Remove effects that are no longer present
	d3d::SceneObjectRef ssao = getRenderScene()->getNamed("ssao");

	if (ssao.isAlive() && !_ssao)
		ssao->destroy();

	d3d::SceneObjectRef dof = getRenderScene()->getNamed("dof");

	if (dof.isAlive() && !_dof)
		dof->destroy();

	d3d::SceneObjectRef fxaa = getRenderScene()->getNamed("fxaa");

	if (fxaa.isAlive() && !_fxaa)
		fxaa->destroy();

	bool needsResize = false;

	if (_windowed != _originalWindowed || _resolutionWidth != getRenderScene()->getRenderWindow()->getSize().x || _resolutionHeight != getRenderScene()->getRenderWindow()->getSize().y) {
		needsResize = true;
		
		_originalWindowed = _windowed;
		
		getRenderScene()->getRenderWindow()->close();

		sf::ContextSettings settings;

		settings.majorVersion = 4;
		settings.minorVersion = 3;
		settings.stencilBits = 0;
		settings.antialiasingLevel = 0;

		getRenderScene()->getRenderWindow()->create(sf::VideoMode(_resolutionWidth, _resolutionHeight), "d3d", _windowed ? sf::Style::Default : sf::Style::Fullscreen, settings);

		getRenderScene()->resize(getRenderScene()->getRenderWindow());

		getRenderScene()->_renderCamera._projectionMatrix = getScene()->_logicCamera._projectionMatrix = d3d::Matrix4x4f::perspectiveMatrix(d3d::_piOver4, static_cast<float>(_resolutionWidth) / static_cast<float>(_resolutionHeight), 0.1f, 10000.0f);

		getScene()->_logicCamera.fullUpdate();
		getRenderScene()->_renderCamera.fullUpdate();
	}

	if (_effectsQuality != _originalEffectsQuality) {
		_originalEffectsQuality = _effectsQuality;

		d3d::SceneObjectRef effectBuffer = getScene()->getNamed("ebuf");

		d3d::SceneObjectEffectBuffer* pEffectBuffer = static_cast<d3d::SceneObjectEffectBuffer*>(effectBuffer.get());

		switch (_effectsQuality) {
		case SceneObjectConfig::_low:
			pEffectBuffer->create(0.5f);
			break;
		case SceneObjectConfig::_medium:
			pEffectBuffer->create(0.75f);
			break;
		case SceneObjectConfig::_high:
			pEffectBuffer->create(1.0f);
			break;
		}
	}

	std::shared_ptr<d3d::Asset> assetBlurHorizontal;

	getScene()->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/blurHorizontal.frag", assetBlurHorizontal);

	std::shared_ptr<d3d::Shader> blurShaderHorizontal = std::static_pointer_cast<d3d::Shader>(assetBlurHorizontal);

	std::shared_ptr<d3d::Asset> assetBlurVertical;

	getScene()->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/blurVertical.frag", assetBlurVertical);

	std::shared_ptr<d3d::Shader> blurShaderVertical = std::static_pointer_cast<d3d::Shader>(assetBlurVertical);

	std::shared_ptr<d3d::Asset> assetRenderImage;

	getScene()->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/renderImage.frag", assetRenderImage);

	std::shared_ptr<d3d::Shader> renderImageShader = std::static_pointer_cast<d3d::Shader>(assetRenderImage);

	std::shared_ptr<d3d::Asset> assetNoise;

	getScene()->getAssetManager("tex2D", d3d::Texture2D::assetFactory)->getAsset("resources/textures/noise.bmp", assetNoise);

	std::shared_ptr<d3d::Texture2D> noiseMap = std::static_pointer_cast<d3d::Texture2D>(assetNoise);

	std::shared_ptr<d3d::Asset> assetBlurHorizontalEdgeAware;

	getScene()->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/blurHorizontalEdgeAware.frag", assetBlurHorizontalEdgeAware);

	std::shared_ptr<d3d::Shader> blurShaderHorizontalEdgeAware = std::static_pointer_cast<d3d::Shader>(assetBlurHorizontalEdgeAware);

	std::shared_ptr<d3d::Asset> assetBlurVerticalEdgeAware;

	getScene()->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/blurVerticalEdgeAware.frag", assetBlurVerticalEdgeAware);

	std::shared_ptr<d3d::Shader> blurShaderVerticalEdgeAware = std::static_pointer_cast<d3d::Shader>(assetBlurVerticalEdgeAware);

	// Add missing effects
	if (!ssao.isAlive() && _ssao) {
		std::shared_ptr<d3d::Shader> ssaoShader(new d3d::Shader());

		ssaoShader->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/imageeffects/ssao.frag");

		std::shared_ptr<d3d::SceneObjectSSAO> ssao(new d3d::SceneObjectSSAO());

		getRenderScene()->addNamed(ssao, "ssao", false);

		ssao->create(blurShaderHorizontalEdgeAware, blurShaderVerticalEdgeAware, ssaoShader, renderImageShader, noiseMap);

		ssao->_ssaoRadius = 0.25f;
		ssao->_ssaoStrength = 4.0f;
		ssao->_blurRadius = 0.00153f;
		ssao->_numBlurPasses = 2;
	}

	if (!dof.isAlive() && _dof) {
		std::shared_ptr<d3d::Shader> depthOfFieldBlurShaderHorizontal(new d3d::Shader());

		depthOfFieldBlurShaderHorizontal->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/imageeffects/depthOfFieldBlurHorizontal.frag");

		std::shared_ptr<d3d::Shader> depthOfFieldBlurShaderVertical(new d3d::Shader());

		depthOfFieldBlurShaderVertical->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/imageeffects/depthOfFieldBlurVertical.frag");

		std::shared_ptr<d3d::SceneObjectDepthOfField> depthOfField(new d3d::SceneObjectDepthOfField());

		getRenderScene()->addNamed(depthOfField, "dof", false);

		depthOfField->create(depthOfFieldBlurShaderHorizontal, depthOfFieldBlurShaderVertical, renderImageShader);

		depthOfField->_layer = 1.5f;

		depthOfField->_focalDistance = 9.0f;
		depthOfField->_focalRange = 0.4f;
		depthOfField->_blurRadius = 0.0017123f;
		depthOfField->_numBlurPasses = 2;
	}

	if (!fxaa.isAlive() && _fxaa) {
		std::shared_ptr<d3d::Shader> lumaShader(new d3d::Shader());

		lumaShader->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/imageeffects/lumaRender.frag");

		std::shared_ptr<d3d::Shader> fxaaShader(new d3d::Shader());

		fxaaShader->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/imageeffects/fxaa.frag");

		std::shared_ptr<d3d::SceneObjectFXAA> fxaa(new d3d::SceneObjectFXAA());

		getRenderScene()->addNamed(fxaa, "fxaa", false);

		fxaa->create(fxaaShader, lumaShader);
	}

	getRenderScene()->getRenderWindow()->setActive();
}