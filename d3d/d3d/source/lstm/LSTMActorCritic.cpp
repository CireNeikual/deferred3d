#include <lstm/LSTMActorCritic.h>

#include <iostream>

using namespace lstm;

void LSTMActorCritic::createRandom(size_t numInputs, size_t numOutputs, size_t actorHiddenSize, size_t numActorMemoryCells,
	size_t criticHiddenSize, size_t numCriticMemoryCells,
	float minWeight, float maxWeight, std::mt19937 &generator)
{
	_actor.createRandom(numInputs, numOutputs, actorHiddenSize, numActorMemoryCells, minWeight, maxWeight, generator);
	//_critic.createRandom(numInputs + numOutputs, 1, criticHiddenSize, numCriticMemoryCells, minWeight, maxWeight, generator);
	_critic.createRandom(numInputs, 1, criticHiddenSize, numCriticMemoryCells, minWeight, maxWeight, generator);

	_currentInputs.assign(numInputs, 0.0f);
}

void LSTMActorCritic::step(float reward, float actorAlpha, float actorLambdaGamma, float offsetStdDev, float criticAlpha, float criticGamma, float criticLambda, std::mt19937 &generator) {
	float criticGammaLambda = criticGamma * criticLambda;

	LSTMNet criticLookAhead = _critic;

	for (size_t i = 0; i < _currentInputs.size(); i++)
		criticLookAhead.setInput(i, _currentInputs[i]);

	criticLookAhead.step(criticGammaLambda);

	float q = reward + criticGamma * _critic.getOutput(0);

	_error = q - _critic.getOutput(0);

	_critic.reinforce(_error * criticAlpha);

	for (size_t i = 0; i < _currentInputs.size(); i++)
		_critic.setInput(i, _currentInputs[i]);

	_critic.step(criticGammaLambda);

	_actor.reinforce(_error > 0.0f ? actorAlpha : 0.0f);

	for (size_t i = 0; i < _currentInputs.size(); i++)
		_actor.setInput(i, _currentInputs[i]);

	_actor.stepPerturbOutput(actorLambdaGamma, offsetStdDev, generator);
}

void LSTMActorCritic::mutate(float mutationRate, float maxPerturbation, std::mt19937 &generator) {
	_actor.mutate(mutationRate, maxPerturbation, generator);
	_critic.mutate(mutationRate, maxPerturbation, generator);
}

void LSTMActorCritic::crossover(const LSTMActorCritic &parent1, const LSTMActorCritic &parent2, float averageChance, size_t numInputs, size_t numOutputs, size_t actorHiddenSize, size_t numActorMemoryCells,
	size_t criticHiddenSize, size_t numCriticMemoryCells, float minWeight, float maxWeight, std::mt19937 &generator)
{
	_actor.crossover(parent1._actor, parent2._actor, averageChance, numInputs, numOutputs, actorHiddenSize, numActorMemoryCells, minWeight, maxWeight, generator);
	_critic.crossover(parent1._critic, parent2._critic, averageChance, numInputs + numOutputs, 1, criticHiddenSize, numCriticMemoryCells, minWeight, maxWeight, generator);

	_currentInputs.assign(numInputs, 0.0f);
}

void LSTMActorCritic::clearMemory() {
	_prevValue = 0.0f;

	_error = 0.0f;
	
	for (size_t i = 0; i < _currentInputs.size(); i++)
		_currentInputs[i] = 0.0f;

	_actor.clearMemory();
	_critic.clearMemory();
}

void LSTMActorCritic::saveToFile(const std::string &actorFileName, const std::string &criticFileName) {
	_actor.saveToFile(actorFileName);
	_critic.saveToFile(criticFileName);
}

bool LSTMActorCritic::createFromFile(const std::string &actorFileName, const std::string &criticFileName) {
	if (!_actor.createFromFile(actorFileName))
		return false;

	if (!_critic.createFromFile(criticFileName))
		return false;

	_currentInputs.assign(_actor.getNumInputs(), 0.0f);

	_prevValue = 0.0f;
	_error = 0.0f;

	return true;
}