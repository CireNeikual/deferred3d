#pragma once

#include <vector>
#include <random>

namespace lstm {
	class LSTMNet {
	public:
		struct Synapse {
			float _weight;
			float _trace;
		};

		struct MemorySynapse {
			float _weight;
			float _trace;
			float _derivative;
		};

		struct Node {
			std::vector<Synapse> _synapses;
			Synapse _bias;

			float _prevOutput;
			float _output;
		};

		struct MemoryNode {
			std::vector<MemorySynapse> _synapses;
			MemorySynapse _bias;

			float _prevOutput;
			float _output;
		};

		struct MemoryCell {
			MemoryNode _inputGate;
			Node _outputGate;
			MemoryNode _forgetGate;

			std::vector<MemorySynapse> _synapses;
			MemorySynapse _bias;

			float _net;
			float _state;
			float _prevOutput;
			float _output;
		};

		static float sigmoid(float x) {
			return 1.0f / (1.0f + std::expf(-x));
		}

	private:
		std::vector<Node> _outputNodes;
		std::vector<std::vector<Node>> _hiddenGroups;
		std::vector<MemoryCell> _memoryCells;

		std::vector<float> _currentInputs;

	public:
		void createRandom(size_t numInputs, size_t numOutputs, size_t hiddenSize, size_t numMemoryCells, float minWeight, float maxWeight, std::mt19937 &generator);

		void step(float gammaLambda);
		void stepPerturbOutput(float gammaLambda, float offsetStdDev, std::mt19937 &generator);
		void reinforce(float error);

		void setInput(size_t index, float value) {
			_currentInputs[index] = value;
		}

		float getInput(size_t index) const {
			return _currentInputs[index];
		}

		float getOutput(size_t index) const {
			return _outputNodes[index]._output;
		}

		size_t getNumInputs() const {
			return _currentInputs.size();
		}

		size_t getNumOutputs() const {
			return _outputNodes.size();
		}

		size_t getHiddenSize() {
			return _hiddenGroups[0].size();
		}

		// For evolution
		void mutate(float mutationRate, float maxPerturbation, std::mt19937 &generator);
		void crossover(const LSTMNet &parent1, const LSTMNet &parent2, float averageChance, size_t numInputs, size_t numOutputs, size_t hiddenSize, size_t numMemoryCells, float minWeight, float maxWeight, std::mt19937 &generator);

		void clearMemory();

		void saveToFile(const std::string &fileName);
		bool createFromFile(const std::string &fileName);
	};
}