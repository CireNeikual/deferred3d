#pragma once

#include <lstm/LSTMNet.h>

namespace lstm {
	class LSTMActorCritic {
	private:
		LSTMNet _actor;
		LSTMNet _critic;

		std::vector<float> _currentInputs;

		float _prevValue;
		float _error;

	public:
		LSTMActorCritic()
			: _prevValue(0.0f), _error(0.0f)
		{}

		void createRandom(size_t numInputs, size_t numOutputs, size_t actorHiddenSize, size_t numActorMemoryCells,
			size_t criticHiddenSize, size_t numCriticMemoryCells,
			float minWeight, float maxWeight, std::mt19937 &generator);

		void step(float reward, float actorAlpha, float actorLambdaGamma, float offsetStdDev, float criticAlpha, float criticGamma, float criticLambda, std::mt19937 &generator);

		void setInput(size_t index, float value) {
			_currentInputs[index] = value;
		}

		float getInput(size_t index) const {
			return _currentInputs[index];
		}

		float getOutput(size_t index) const {
			return _actor.getOutput(index);
		}

		// For evolution
		void mutate(float mutationRate, float maxPerturbation, std::mt19937 &generator);
		void crossover(const LSTMActorCritic &parent1, const LSTMActorCritic &parent2, float averageChance, size_t numInputs, size_t numOutputs, size_t actorHiddenSize, size_t numActorMemoryCells,
			size_t criticHiddenSize, size_t numCriticMemoryCells, float minWeight, float maxWeight, std::mt19937 &generator);

		void clearMemory();

		void saveToFile(const std::string &actorFileName, const std::string &criticFileName);
		bool createFromFile(const std::string &actorFileName, const std::string &criticFileName);
	};
}