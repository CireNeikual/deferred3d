void kernel globalIllumination(global const float* treeBuffer, global const float* triangleBuffer, global const float* materialBuffer, read_only image2d_array_t materialTextures,
	read_only image2d_t gBufferPosition, read_only image2d_t gBufferNormal, read_only image2d_t gBufferColor,
	write_only image2d_t target, uint2 targetSize)
{
	int2 pixelPos = (int2)(get_global_id(0), get_global_id(1));

	write_imagef(target, pixelPos, (float4)(0.5f, 0.2f, 0.9f, 1.0f));
}