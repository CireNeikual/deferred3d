
Getting The Latest Sources
========================================

All releases are signed with a :doc:`PGP key <pgpkey>`.

Unsure which release you want? Check the :ref:`FAQ <devel_vs_stable>`.

.. note::

   If you are planning on developing an application using TLS, using
   the latest 1.11 release instead of 1.10 is highly recommended.

Current Development Series (1.11)
----------------------------------------

The latest development release is :doc:`relnotes/1_11_7`:
:tgz:`1.11.7` (:tgz_sig:`sig <1.11.7>`),
:tbz:`1.11.7` (:tbz_sig:`sig <1.11.7>`)

To access the latest unreleased sources, see :doc:`vcs`. A script also
creates regular snapshots of trunk, which are available `here
<https://files.randombit.net/botan/snapshots/>`_.

.. note::

   Versions 1.11.0 and later require a mostly-compliant C++11 compiler
   such as Clang 3.1 or GCC 4.7.

Current Stable Series (1.10)
----------------------------------------

The latest stable branch release is :doc:`relnotes/1_10_7`:
:tgz:`1.10.7` (:tgz_sig:`sig <1.10.7>`),
:tbz:`1.10.7` (:tbz_sig:`sig <1.10.7>`)

..
  Windows Installer
  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  Windows installers for use with Visual C++ 2010
  :installer_x86_32:`1.10.5` (:installer_sig_x86_32:`sig <1.10.5>`)
  and
  :installer_x86_64:`1.10.5` (:installer_sig_x86_64:`sig <1.10.5>`)
  are also available.
