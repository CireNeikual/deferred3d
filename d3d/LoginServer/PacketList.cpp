#include "PacketList.h"

//pliststatic
pliststatic::pliststatic()
{
	idmap.insert(std::pair<sfn::Int8, std::function<std::shared_ptr<Packet>()>>(1, PacketPlayerLogin::Create));
}

pliststatic::~pliststatic()
{

}

//packetlist
pliststatic PacketList::_stdata;

std::shared_ptr<Packet> PacketList::ConstructPacket(sfn::Int8 id)
{
	if (_stdata.idmap.count(id) == 1)
		return (_stdata.idmap[id])();
	return nullptr;
}