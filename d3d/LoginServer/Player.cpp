#include "Player.h"

sqlite3_stmt* Player::_loginstatement;

Player::Player(const std::shared_ptr<TLSConnection> &connection) : _connection(connection)
{
	//_loginstatement = NULL;
}


Player::~Player()
{
}

//
int callbacklogin(void* parameter, int argc, char** argv, char** azColName)
{

}

/*
LoginAttemptResult Player::LoadFromMySql
Runs when a user attempts to login with a given username/password. Returns an enum that describes if the login was
successful, or why it was unsuccessful.

NOTE: Though this function will return a different result for an incorrect username and incorrect password, for security reasons
this information should never be exposed to the end user. If any combination of username/password is incorrect, simply return a generic failure message.
*/
LoginAttemptResult Player::LoadFromMySql(sqlite3* database, const std::string &username, const std::string &password)
{
	//Verify the connection. C is some bullshit and connection doesn't have like a "connected" member and I'm not about to control f the docs for the inevitable mysql_connection_is_connected_tester_function_alpha(). sue me
	if (database == NULL)
	{
		std::cerr << "Error: Attempting to load a player from database but the database connection is invalid." << std::endl;
		return LoginAttemptResult::LOGIN_FAILED_CONNECTION_ISSUE;
	}

	//check to see if we have done this before. If not, create the sqlite statement that we can use from now on:
	if (_loginstatement == NULL)
	{
		std::string query = "SELECT passhash FROM Users where (username = ?)";
		if (sqlite3_prepare_v2(database, query.c_str(), query.length(), &_loginstatement, NULL) != SQLITE_OK)
		{
			std::cerr << "DB ERROR: Unable to compile prepared login statement. Check formatting." << std::endl;
			return LoginAttemptResult::LOGIN_FAILED_CONNECTION_ISSUE;
		}
	}

	sqlite3_reset(_loginstatement);

	//Check to see if the username has valid characters to assist in preventing SQL Injection.
	if (!VerifyUsernameIsValid(username))
		return LoginAttemptResult::LOGIN_FAILED_INVALID_USERNAME_FORMAT;

	//Ok, now we can check to see if the username is legit.
	sqlite3_bind_text(_loginstatement, 1, username.c_str(), username.length(), SQLITE_TRANSIENT);
	int test;
	if ((test = sqlite3_step(_loginstatement)) != SQLITE_ROW)
	{
			return LoginAttemptResult::LOGIN_FAILED_INCORRECT_USERNAME;
	}

	//this only works assuming that the passhash is ascii. sqlite returns text in utf8 format I believe
	const unsigned char* cpasshash = sqlite3_column_text(_loginstatement, 0); //hash of password stored in db
	std::string passhash(reinterpret_cast<const char*>(cpasshash));
	//free((void*)cpasshash);
	
	if (!Botan::check_bcrypt(password, passhash))
		return LoginAttemptResult::LOGIN_FAILED_INCORRECT_PASSWORD;

	return LoginAttemptResult::LOGIN_SUCCESSFUL;
}

std::string Player::Register(sqlite3* database, const std::string &username, const std::string &password)
{
	//Don't know how this is going to work because it will have to interact with steam. So for right now
	//all this function does is return the bcrypt hash of the password.
	//for now manually add user accounts into the db using the sqlite shell tool
	//Botan::AutoSeeded_RNG rng;
	//return Botan::generate_bcrypt(password, rng, 12);
}

/*
We only allow certain characters in usernames as a tool to mitigate possible SQL Injection attacks.
For now valid characters are lowercase or upper case alphabetic.
*/
bool Player::VerifyUsernameIsValid(const std::string &username)
{
	for (int i = 0; i < username.length(); i++)
	{
		if ((username[i] >= 'a' && username[i] <= 'z') || (username[i] >= 'A' && username[i] <= 'Z'))
			continue;
		else
			return false;
	}
	return true;
}