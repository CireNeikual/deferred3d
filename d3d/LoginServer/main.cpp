/*
LOGIN SERVER PARAMETERS:

The login server will listen for two types of connections:
-users attempting to login will send the following sfn::Message data packet after establishing a TLS connection:
	-sfn::Int8 data containing the number 1
	-std::string containing the user's username
	-std::string containing the user's password

	The user will receive the following sfn::Message in return:
	-Int8 error code, where 0 is success.
	if error code is not 0:
		-std::string containing error message.
	if error code is 0:
		-sfn::Int16 data containing number of other users online.
		-sfn::Int16 data containing their ELO.
		-std::string data containing welcome message.

	Once logged in, users can send the following requests: (tbd)
	
-game servers attempting to register themselves with the login server will send the following sfn::Message data packet after establishing a TLS connection.
	-sfn::Int8 data containing the number 2
	-std::string containing the x509 certificate they wish to use.
	
	In response, the login server will send the following:
	-sfn::Int8 code, 0 if the certificate is on the allowed list, and 1 if it is not.
	-32 bit random integer that has been encrypted with the RSA public key described in the certificate.

	The game server must then send back:
	-the 32 bit random integer that was sent to it, reencrypted with the RSA private key that it has on file for the certificate it wants to use.


*/

#include <iostream>
#include <fstream>
#include <SFNUL.hpp>
#include <SFNUL/TcpListener.hpp>
#include <SFNUL/TlsConnection.hpp>
#include <thread>

#include <sqlite3.h>

#include <botan/blowfish.h>

#include "Player.h"
#include "bcrypt.h"


//lazy globals for certs
std::vector<std::shared_ptr<sfn::TlsCertificate>> certificates;
std::shared_ptr<sfn::TlsCertificate> logincert;
std::shared_ptr<sfn::TlsKey> loginkey;

/*
Loads all of the certificates.
format:
	meta.txt must exist in directory:
		-first line is string of the filename of the login server's public certificate.
		-second line is string of the filename of the login server's private key.
		-third line is int of the number of game server certificates, we'll call this N.
		-line 4 - N are strings containing the filename of a game server certificate.
	each file contains the raw text that will be read in and used in the SSL library.
*/
void LoadCertificatesFromDisk(const std::string &directory)
{
	std::ifstream file(directory + "/meta.txt");
	if (!file.is_open())
	{
		std::cerr << "Could not open file meta.txt in directory " << directory << std::endl;
		return;
	}
	std::string maincertloc;
	std::string mainpkeyloc;
	int gamecertcount;
	file >> maincertloc >> mainpkeyloc >> gamecertcount;

	std::vector<std::string> certlocs(gamecertcount);
	certificates.resize(gamecertcount);

	for (int i = 0; i < gamecertcount; i++)
		file >> certlocs[i];

	file.close();
	file.open(directory + "/" + maincertloc); //Load in the login server's certificate
	if (!file.is_open())
	{
		std::cerr << "Could not open maincertloc file " << maincertloc << " in directory " << directory << std::endl;
		return;
	}

	std::stringstream strstream;
	strstream << file.rdbuf();
	logincert = sfn::TlsCertificate::Create(strstream.str());

	file.close();
	strstream.clear();

	file.open(directory + "/" + mainpkeyloc); //Load in the login server's private key information
	if (!file.is_open())
	{
		std::cerr << "Could not open file " << mainpkeyloc << " in directory " << directory << std::endl;
		return;
	}

	std::stringstream strstream2;

	strstream2 << file.rdbuf();
	std::string keys = strstream2.str();
	std::cout << keys << std::endl;
	loginkey = sfn::TlsKey::Create(keys);
	file.close();
	strstream2.clear();

	for (int i = 0; i < gamecertcount; i++)
	{
		std::stringstream strstream3;
		file.open(certlocs[i]);
		strstream3 << file.rdbuf();
		certificates.push_back(sfn::TlsCertificate::Create(strstream3.str()));
		file.close();
		strstream3.clear();
	}
}

int main()
{
	//using this block of code to test generate some hashes.
	/*std::ofstream passhash("passhash.txt");
	Botan::AutoSeeded_RNG rng;
	passhash << Botan::generate_bcrypt("testpassword", rng, 12);
	passhash.close();*/



	std::cout << "Loading key information..." << std::endl;

	LoadCertificatesFromDisk("certs");

	//setup database
	std::cout << "Connecting to database..." << std::endl;

	sqlite3* database;
	int rc = sqlite3_open("gamedb", &database);
	if (rc)
	{
		std::cerr << "Could not open database: " << sqlite3_errmsg(database);
		sqlite3_close(database);
		return 1;
	}

	std::cout << "Databased loaded." << std::endl;

	std::vector<Player> players;

	std::vector<TLSConnection::Ptr> connections;

	std::shared_ptr<sfn::TcpListener> listener = sfn::TcpListener::Create();
	sfn::Endpoint endpoint(4321);

	listener->Listen(endpoint);

	std::cout << "Login server online! Listening for new connections." << std::endl;

	bool exit = false;
	while (!exit)
	{
		TLSConnection::Ptr socket;
		while ((socket = listener->GetPendingConnection<TLSConnection>())) //Go through all new connections
		{
			socket->SetCertificateKeyPair(logincert, loginkey);
			connections.push_back(socket);
		}

		//Delete connections that no longer need to exist.
		for (auto connection_iter = connections.begin(); connection_iter != connections.end();)
		{
			if ((*connection_iter)->RemoteHasShutdown() && !(*connection_iter)->BytesToSend())
				connection_iter = connections.erase(connection_iter);
			else
				connection_iter++;
		}

		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}

	//system("pause");
	//sfn::TlsConnection<sfn::TcpSocket, sfn::TlsEndpointType::SERVER, sfn::TlsVerificationType::REQUIRED>::Create();

	return 0;
}