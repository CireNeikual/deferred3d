#include "Packet.h"


Packet::Packet()
{
}


Packet::~Packet()
{
}

void Packet::ProcessClient()
{

}

void Packet::ProcessServer()
{

}

void Packet::WriteToMessage(std::shared_ptr<sfn::Message> message)
{
	(*message) << _id;
}

const sfn::Int8 Packet::GetID() const
{
	return _id;
}

void Packet::SetId(sfn::Int8 id)
{
	_id = id;
}
